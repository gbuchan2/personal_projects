//DIVISION.CPP
//Definitions of the Division Class
//Graham Buchanan


/***UPDATE LOG***
 *  17 December 2021 - Creation of File
 *  18 December 2021 - Constructor & Destructor
 *  22 December 2021 - Data Accessors
 *  26 December 2021 - Save and Load functionality
 */

#include "division.hpp"
#include <map>

Division::Division() {
	name = "";
}

Division::~Division() {
}

Division::Division(const std::string &n) {
	name = n;
}

size_t Division::getSize() const {
	return size;
}

void Division::PrintData() const {
	size_t i = 0;
	std::multimap<uint32_t, Team *>::const_iterator mit;
	
	printf("\n   %s:\n", name.c_str());
	for(mit = tmMap.begin(); mit != tmMap.end(); ++mit) {
		mit->second->PrintData();
	}
}
