//Bitmatrix Operations 
//A program to manipulate and store bitmatrices
//Graham Buchanan
//04 October 2021

#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <iostream>
#include "bitmatrix.hpp"
using namespace std;

/*This program corresponds to the given program "bitmatrix_editor" which prompts the Bitmatrix and BM_Hash classes to manipulate and store bitmatrices
 * The Bitmatrix Class contains one central vector of strings identified as the BITMATRIX. 
 * The functions within the class create, delete, edit, or copy that BITMATRIX, following the commands of the user.
 * This program is meant to handle matrix multiplication, summation, and basic row operations
 *
 * The BM_Hash Class contains a two-dimensional vector of HTE or "Hash Table Entries".
 * This vector represents the hash table, ordered by a given string key, that stores a Bitmatrix.
 * This class handles storing, recalling, and printing entries in the hash table.*/

//The basic constructor
//Parameters: rows and columns
//Function: produces an empty matrix (all entries are '0')
Bitmatrix::Bitmatrix(int rows, int cols)
{
	string s;
	int i;

	//Error checking
	if(rows <= 0) throw((string) "Bad rows");
	if(cols <= 0) throw((string) "Bad cols");

	//Clear the matrix and push_back strings of '0'
	M.clear();
	s.resize(cols, '0');
	for(i = 0; i < rows; ++i) M.push_back(s);
}

//Constructor from file
//Parameter: file name
//Function: produces a matrix consisting of '0' or '1' entries from a file
Bitmatrix::Bitmatrix(const string &fn)
{
	ifstream fin;
	string a;
	string s;
	string f;
	istringstream ss;
	size_t c = 0;

	//Open the file and Error Check
	fin.open(fn.c_str());
	if(fin.fail()) throw((string) "Can't open file");

	//Clear the matrix and gather data line by line
	//If line is empty, do nothing
	//If line contains entires not '0' or '1' or whitespace, error
	//If line is not same length as previous, error
	//store the line into the matrix
	M.clear(); 
	while(getline(fin, f)) {
		if((f.length() == 0)) getline(fin,f);
		a = "";
		ss.clear();
		ss.str(f);
		while(ss >> s) {
			a = a + s;
		}
		if(!(a.empty())) {
			if((a.find_first_not_of(" 01", 0) != string::npos)) throw((string) "Bad file format");
			if((c != 0) && (a.length() != c)) throw((string) "Bad file format");
			if((c == 0)) c = a.length();
			M.push_back(a); //once success, save string and continue
		}
	}
	fin.close();
}

//Copy Constructor
//Function: Copies the matrix and returns a new matrix
Bitmatrix *Bitmatrix::Copy() const
{
	int i = 0;
	int j = 0;

	//Create a NEW bitmatrix of same size
	//Store value-by-value entries of the matrix
	Bitmatrix *x = new Bitmatrix(this->Rows(),this->Cols());
	for(i = 0; i < x->Rows(); ++i) {
		for(j = 0; j < x->Cols(); ++j) {
			x->Set(i, j, this->Val(i, j));
		}
	}
	return x;
}

//Write 
//Parameter: file name
//Function: produces a copy of the matrix in a specific format into a file
bool Bitmatrix::Write(const string &fn) const
{
	ofstream file;
	size_t i;

	//Error Checking
	if(Rows() == 0 || Cols() == 0) return false;
	if(fn.empty()) return false;

	//Opening the file
	file.open(fn.c_str());
	if(!(file.is_open())) return false;

	//Copy matrix
	for(i = 0; i < M.size(); ++i) {
		file << M[i] << endl;
	}
	file.close();
	return true;
}

//Print
//Paramter: width of rows and columns
//Function: print the matrix on standard output, spaced out by given width
void Bitmatrix::Print(size_t w) const
{
	int i;
	int j;

	//If not given a width, print normally
	//If given a width, every w rows and w cols, add new line and space, respectively
	if(w == 0) {
		for(i = 0; i < Rows(); ++i) {
			for(j = 0; j < Cols(); ++j) {
				cout << M[i][j];
			}
			cout << endl;
		}
	} else {
		for(i = 0; i < Rows(); ++i) {
			if((i != 0) && ((i % w) == 0)) cout << endl;
			for(j = 0; j < Cols(); ++j) {
				if((j != 0) && ((j % w) == 0)) cout << ' ';
				cout << M[i][j];
			}
			cout << endl;
		}
	}
}

//PGM Creation
//Paramters: file name, width (p), border
//Function: produce a pgm file of the matrix with squares representing each entry to the width of p;
//			Add a border around each square to the width of border
bool Bitmatrix::PGM(const string &fn, int p, int border) const
{
	//zeros are white (255), ones are gray (100).
	//each entry is a pixels x pixels square
	//if border > 0, print a border (0) separating EACH square and the matrix
	ofstream file;
	int i = 0;
	int j = 0;
	int k = 0;
	int b = 0;
	string line;
	string row;
	string end;
	string ones;
	string zeros;

	//Error Checking
	if(Rows() == 0 || Cols() == 0) return false;
	if(fn.empty()) return false;
	file.open(fn.c_str());
	if(!file.is_open()) return false;
	if(p <= 0) return false;
	if(border < 0) return false;

	//Reset each string...could be done at declaration
	//Go throuhg the size of border and add the applicable item to either line (entire line) or row/end (in-row)
	line = "";
	row = "";
	for(b = 0; b < border; ++b) {
		for(j = 0; j < Cols()*p + (border * (Cols() + 1)); ++j) {
			if(j != 0) line += ' ';
			line += '0';
		}
		line += '\n';
		if(b != 0) row += ' ';
		row += '0';
	}

	//Reset each string
	//Go through the size of p and assign the appropriate # of entries
	//ones refers to '1' squares
	//zeros refers to '0' squares
	ones = "";
	zeros = "";
	for(i = 0; i < p; ++i) {
		if(i != 0) {
			ones += ' ';
			zeros += ' ';
		}
		ones += "100";
		zeros += "255";
	}

	//Write the heading
	file << "P2" << endl << Cols()*p+(border * (Cols() + 1)) << ' ' << Rows()*p+(border * (Rows() + 1)) << endl << 255 << endl << line;

	//Print every row p times
	//Print every col p times
	//Add border strings where appropriate
	for(i = 0; i < Rows(); ++i) {
		for(k = 0; k < p; ++k) {
			if((k % 2) == 0) file << endl;
			for(j = 0; j < Cols(); ++j) {
				if(j != 0) file << ' ';
				if(row != "") file << row << ' ';
				if(Val(i, j) == '1') file << ones;
				if(Val(i, j) == '0') file << zeros;
			}
			if(row != "") file << ' ' << row;
			file << endl;
		}
		file << line;
	}
	file.close();
	return true;;
}

//Number of Rows
//Function: Returns size of matrix
int Bitmatrix::Rows() const
{
	if(M.empty()) return 0;
	return M.size();
}

//Number of Columns
//Function: Returns size of each string
int Bitmatrix::Cols() const
{
	if(M.empty() || M[0].empty()) return 0;
	return M[0].length();
}

//Value
//Parameters: row index, column index
//Function: returns the entry at requested index
char Bitmatrix::Val(int row, int col) const
{
	if(M.empty()) return 'x';
	if(M[0].empty()) return 'x';
	if(row < 0 || col < 0 || row > Rows() || col > Cols()) return 'x';	
	return M[row][col];
}

//Set Value
//Parameters: row index, column index, char value
//Function: Find specific index, set given value to that index
bool Bitmatrix::Set(int row, int col, char val)
{
	//Error Checking
	if(Rows() == 0) return false;
	if(Cols() == 0) return false;
	if(row < 0 || col < 0 || (row > Rows()) || (col > Cols())) return false;
	if((val != 0) && (val != 1) && (val != '0') && (val != '1')) return false;
	
	//Functions takes either integer or char values
	if((val == 1)) {
		M[row][col] = '1';
	} else if((val == 0)) {
		M[row][col] = '0';
	} else {
		M[row][col] = val;
	}
	return true;
}

//Swapping Rows
//Paramters: row index, row2 index
//Function: Literally just swap the strings
bool Bitmatrix::Swap_Rows(int r1, int r2)
{
	string r;

	//Error Checking
	if(Rows() == 0 || Cols() == 0) return false;
	if(r1 < 0 || r2 < 0 || r1 > Rows() || r2 > Rows()) return false;
	
	//Clear the string 
	//copy the string
	//replace the string
	r.clear();
	r = M[r1];
	M[r1].clear();
	M[r1] = M[r2];
	M[r2].clear();
	M[r2] = r;
	return true;
}

//Row1 plus Row 2
//Paramters: row index, row2 index
//Function: Set Row1 equal to Row1 plus Row 2
bool Bitmatrix::R1_Plus_Equals_R2(int r1, int r2)
{
	string r = "";
	int i = 0;
	
	//Error Checking
	if((Rows() == 0) || (Cols() == 0)) return false;
	if((r1 > Rows() - 1) || (r2 > Rows() - 1)) return false;
	
	//String resize to correct size
	//Perform operations
	r.resize(Cols());
	for(i = Cols() - 1; i >= 0; --i) {
		r[i] = ((M[r1][i] + M[r2][i]) % 2 + '0'); 
	}
	M[r1] = r;
	return true;
}

//Sum
//Parameters: BIMATRIX a, BITMATRIX b
//Function: Add two matrices and return a third
Bitmatrix *Sum(const Bitmatrix *a1, const Bitmatrix *a2)
{
	int i;
	int j;
	
	//Creates a new matrix, sum of a1 & a2
	//if a1 and a2 are not the same size, NULL
	if((a1 == NULL) || (a2 == NULL)) return NULL;
	if((a1->Rows() != a2->Rows()) ||(a1->Cols() != a2->Cols())) return NULL;

	//New Matrix
	Bitmatrix *sum = new Bitmatrix(a1->Rows(),a1->Cols());

	//Each entry of a is added to b, then moduloed correctly, set to sum's matrix
	for(j = 0; j < sum->Rows(); ++j) {
		for(i = sum->Cols() - 1; i >= 0; --i) {
			sum->Set(j, i, (a1->Val(j, i) + a2->Val(j, i)) % 2 + '0');
		}
	}
	return sum;
}

//Product
//Parameters: BITMATRIX a, BITMATRIX b
//Function: Matrix Multiplication
Bitmatrix *Product(const Bitmatrix *a1, const Bitmatrix *a2)
{
	string r = "";
	int i;
	int j;
	int k;
	int w = 0;
	int sum = 0;

	//Error Checking
	if(a1->Cols() != a2->Rows()) return NULL;

	//New Matrix
	Bitmatrix *f = new Bitmatrix(a1->Rows(), a2->Cols());
	//First, identify the product of the correct entries
	//Sum all the products
	//Modulo the sum and store in f matrix
	for(i = 0; i < f->Rows(); ++i) {
		for(j = 0; j < f->Cols(); ++j) {
			for(k = 0; k < a1->Cols(); ++k) {
				w = ((int)a1->Val(i, k) * (int)a2->Val(k, j));
				sum += w;
			}
			f->Set(i, j, sum % 2);
			sum = 0;
		}
	}
	return f;
}

//Sub
//Parameters: BITMATRIX, vector of rows
//Function: Build a matrix solely of specified row indices
Bitmatrix *Sub_Matrix(const Bitmatrix *a1, const vector <int> &rows)
{
	size_t i = 0;
	int j = 0;

	//Error Checking
	if((a1 == NULL)) return NULL;
	if(rows.empty()) return NULL;
	//Guarantee that each rows entry refers to a real row
	for(i = 0; i < rows.size(); ++i) {
		if((rows[i] < 0) || (rows[i] >= a1->Rows())) return NULL;
	}

	//New matrix
	//Unfortunately, one must use SET
	Bitmatrix *sub = new Bitmatrix(rows.size(), a1->Cols());
	for(i = 0; i < rows.size(); ++i) {
		for(j = 0; j < sub->Cols(); ++j) {
			sub->Set(i, j, a1->Val(rows[i], j));
		}
	}
	return sub;
}

//Inverse
//Parameters: BITMATRIX
//Function: Return the inverse
Bitmatrix *Inverse(const Bitmatrix *m)
{
	int i = 0;
	int j = 0;
	bool flag = false;

	//Error Checking
	if(m->Rows() == 0 || m->Cols() == 0) return NULL;
	if(m->Rows() != m->Cols()) return NULL;
	
	//Create a duplicate and identity matrix
	Bitmatrix *dup = m->Copy();
	Bitmatrix *inv = new Bitmatrix(m->Rows(), m->Cols());
	for(i = 0; i < m->Rows(); ++i) {
		inv->Set(i, i, 1);
	}
	Bitmatrix *id = inv->Copy();

	//Find leading zeros in the appropriate locations
	//Reduce all other entries within the same column to achieve RREf
	for(i = 0; i < m->Rows(); ++i) {
		if(dup->Val(i, i) != '1') {
			for(j = i + 1; j < m->Rows(); ++j) {
				if(dup->Val(j, i) == '1') {
					dup->Swap_Rows(i, j);
					inv->Swap_Rows(i, j);
					flag = true;
					break;
				}
			}
			if(flag == false) {
				return NULL;
			}
		}
		for(j = i + 1; j < m->Rows(); ++j) {
			if(dup->Val(j, i) == '1') {
				dup->R1_Plus_Equals_R2(j, i);
				inv->R1_Plus_Equals_R2(j, i);
			}
		}
	}
	for(i = m->Rows() - 2; i >= 0; --i) {
		for(j = m->Cols() - 1; j > i; --j) {
			if(dup->Val(i, j) == '1') {
				dup->R1_Plus_Equals_R2(i, j);
				inv->R1_Plus_Equals_R2(i, j);
			}
		}
	}

	//Guarantee dup matches identity
	for(i = 0; i < m->Rows(); ++i) {
		for(j = 0; j < m->Cols(); ++j) {
			if(dup->Val(i, j) != id->Val(i, j)) return NULL;
		}
	}

	//return inverse
	delete id;
	delete dup;
	return inv;
}


/*HASH TABLE CLASS*/

//Generate a Hash Table
BM_Hash::BM_Hash(int size)
{
	if(size <= 0) throw((string) "Bad size");
	Table.resize(size);
}

//Store a matrix within the table
bool BM_Hash::Store(const string &key, Bitmatrix *bm)
{
	size_t i = 0;
	unsigned int h = 0;
	bool flag = false;
	h = 5381;

	for(i = 0; i < key.size(); ++i) {
		h = (h << 5) + h + key[i];
	}
	//Go straight to Table[key % size]
	//Check if vector is empty, if so, then just push_back
	//if not, run through vector to find same key, if not,
	//push_back
	HTE a;
	a.key = key;
	a.bm = bm;
	if(Table[h % Table.size()].empty()) {
		Table[h % Table.size()].push_back(a);
		return true;
	} else {
		for(i = 0; i < Table[h % Table.size()].size(); ++i) {
			if(Table[h % Table.size()][i].key == key) flag = true;
		}
		if(!flag) {
			Table[h % Table.size()].push_back(a);
			return true;
		}
	}
	return false;
}

//Recall a value from the table
Bitmatrix *BM_Hash::Recall(const string &key) const
{
	size_t i = 0;
	unsigned int h = 0;
	h = 5381;

	for(i = 0; i < key.size(); ++i) {
		h = (h << 5) + h + key[i];
	}

	if(Table.empty()) return NULL;
	if(Table[h % Table.size()].empty()) return NULL;
	else {
		for(i = 0; i < Table[h % Table.size()].size(); ++i) {
			if(Table[h % Table.size()][i].key == key) {
				return Table[h % Table.size()][i].bm;
			}

		}
		return NULL;
	}
}

//Print all table entries in order
vector <HTE> BM_Hash::All() const
{
	vector <HTE> rv;
	size_t i = 0;
	size_t j = 0;

	if(!Table.empty()) {
		for(i = 0; i < Table.size(); ++i) {
			if(!(Table[i].empty())) {
				for(j = 0; j < Table[i].size(); ++j) {
					rv.push_back(Table[i][j]);
				}
			}
		}
	}
	return rv;
}
