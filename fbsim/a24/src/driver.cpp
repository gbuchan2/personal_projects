//MAIN.CPP
//Driver Program
//Graham Buchanan

/***CHANGELOG***
 * 20 July 2022 - Creation and Entering Team Data
 * 03 August 2022 - Added series of game simulation; 
 */

#include "master.hpp"
int main() {
	printf("WELCOME TO FBSIM VERSION ALPHA_22! ENJOY!\n");
	master m;
	m.EnterTeams();
	m.Play_A_Game();
	return 0;
}
