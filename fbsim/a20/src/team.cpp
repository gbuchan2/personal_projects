//TEAM.CPP
//Team Class Definitions
//Graham Buchanan

/***Update Log***
 * 13 October 2021 - Creation
 * 26 October 2021 - Scrapped and Redone
 * 09 November 2021 - Added EditRating & updated error statements
 * 17 November 2021 - MakeFile functionality ... fixed issues with PrintData
 * 22 December 2021 - Division API and storage...more data accessors
 * 26 December 2021 - More data accessors...save and load functionality
 */

#include "team.hpp"

/*
   public:
   Team(std::string l, std::string n, uint16_t r); //Class Constructor
   bool EditName(std::string l, std::string n); //0 - edit Name and Location, 1 - Rating
   bool EditRating(uint16_t r); //replaces the current rating (indicated by the League Class)
   bool UpdateStats(bool flag, int pf, int po); //0 - new game, 1- edited game
   void PrintData(); //Prints all Data of the Team Class

   protected:
   std::string Name; //Stores the name of the Team (i.e. "Tigers")
   std::string Location; //Stores the Location of the Team (i.e. "Auburn")
   uint32_t tmID = 0;
   uint16_t Rating = 0; //0-7 Offense, 8-15 Defense
   double PPG = 0;; //ppg
   double PAPG = 0;//opponent ppg
   uint32_t record = 0; //0-7 ties, 8-15 losses, 16-23 wins
   };*/

Team::Team(const uint32_t &id, const std::string &l, const std::string &n, const uint16_t &r) {
	if((l.empty()) || (n.empty()) || (r == 0)) throw((std::string) "ERROR: Teams must have a valid Location and Name and a Rating greater than 0.");
	Name = n;
	Location = l;
	Rating = r;
	PPG = 0;
	PAPG = 0;
	record = 0;
	tmID = id;
}

bool Team::EditName(const std::string &l, const std::string &n) {
	if((l.empty()) || (n.empty())) {
		throw((std::string) "ERROR: Teams must have a valid Name and Location.");
	} else {
		Location = l;
		Name = n;
	}
	return true;
}

bool Team::EditRating(const uint16_t &r) {
	if(r == 0) {
		std::cerr << "ERROR: Rating must be greater than 0.";
		return false;
	}
	else {
		Rating = r;
	}
	return true;
}

bool Team::UpdateStats(bool &flag, const unsigned int &pf, const unsigned int &po) {

	double temp = 0;
	int t = 0;

	//0 - new game
	//1 - past game
	if(flag) {
		//do nothing for now
	} else {
		t = (record & 0xff) + (record & 0xff00) + (record & 0xff0000); //t = no games
		temp = PPG * t + pf;
		PPG = (temp / (t + 1));
		temp = PAPG * t + po;
		PAPG = (temp / (t + 1));
		
		if(pf == po) {
			t = (record & 0xff) + 1;
			record |= t;
		} else if(pf > po) {
			t = (record & 0xff0000) + 1;
			record |= (t << 16);
		} else {
			t = (record & 0xff00) + 1;
			record |= (t << 8);
		}
	}
	return true;
}
void Team::PrintData() const {
	printf("#%07x - %s %s (%s):\n\t%d-%d %d-%d-%d %.2f-%.2f\n", 
			tmID, Location.c_str(), Name.c_str(), div->GetName().c_str(), (Rating & 0xff), ((Rating >> 8) & 0xff),((record >> 16) & 0xff), ((record >> 8) & 0xff), (record & 0xff), PPG, PAPG);
}

bool Team::SetDivision(const Division *n) {
	div = n;
	return true;
}
uint32_t Team::getID() const{ 
	return tmID;
}
std::string Team::getName() const {
	return Name;
}
std::string Team::getLocation() const {
	return Location;
}
uint16_t Team::getRating() const {
	return Rating;
}
uint32_t Team::getRecord() const {
	return record;
}
uint16_t Team::getPtsScored() const {
	int total = 0;
	int nogms = ((record >> 16) & 0xff) + ((record >> 8) & 0xff) + (record & 0xff);
	total = PPG * nogms;
	return total;
}
uint16_t Team::getOppScored() const {
	int total = 0;
	int nogms = ((record >> 16) & 0xff) + ((record >> 8) & 0xff) + (record & 0xff);
	total = PAPG * nogms;
	return total;
}
Division* Team::getDivision() const {
	return div;
}
