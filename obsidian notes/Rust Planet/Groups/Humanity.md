At an unknown point in time, the human home world no longer sustained human life in a balanced manner. This could have been due to rapidly rising surface temperatures, rampant levels of pollution, or some unexpected natural disaster. A select group of humans either already lived in space or made it off world in order to avoid the collapse of human civilization and the subsequent apocalypse that befell the rest of mankind. While parts of this tale may be true or untrue, those who escaped the surface claimed to be the "last of humanity", adrift in space. 

Some time later, these humans had pulled together the last remnants of the human spacefaring fleet -- military, research, and pleasure vessels alike -- to form one singular satellite. This bastion of human civilization became known as "The Hub". The original population numbered somewhere in the hundreds, but it quickly ballooned with an over-abundance of supply and little restraint. Lawlessness prevailed, and conflict between man and society dominated. Over time, some singular entities emerged to tie humanity together, peacefully and violently. The final composite of this mission formed "[[#The Grand Treaty]]" that has ruled the Hub for some 800 years. 

A notable feature of living in a tin can is the necessity of efficiency. All unnecessary ship parts are repurposed. Supply caches are centralized. Oxygen consumption and carbon production are tracked individually. Early on, the leaders of the Hub realized their inability to "lose mass" in order to maintain a caloric balance. All organic material must be contained inside the Hub, waste and deceased included. Great effort went into legislating, implementing, and enforcing the concept of a [[#Recycle]]. In the same vein, it became apparent to limit the number of humans on the Hub at any one time. The early days of the Hub featured many expeditions across the solar system, but those days are long gone. For the past century or so, homeostasis has been maintained. 

With a limited space and limited population, the threat of inbreeding and genetic deformity also became apparent. A genetic chart of all persons was quickly formed, with strict anti-incest and trait promotion matching employed. Mandatory aptitude and physical fitness tests became the norm. Only the best of the best could reproduce, thus humanity pushed its physical and mental limits. Slowly, humans became leaner to minimize caloric intake. Immune Systems became stronger to eliminate disease. A positive feedback loop began to take effect, as did caste systems and subtle prejudice along the public test results. In many ways, these new humans would look upon their forefathers like aliens; yet, in many more ways, they would be one and the same. 

# Average Life
- Birth-9: living with family, [[Unemployed]]
- 9-15
	- (employed) apprenticing under appropriate Master
	- (unemployed) working under closest employed
- 15: Mating
	- [[Scoper]]s select mating pairs based on genetic diversity and valued traits
	- Mates are not allowed to impregnate until an appropriate time
- 15-24: Assignment
	- (employed only) Guilds promote apprentices into Journeymen and Masters where needed
- 9, 18, 27, ..., so on: testing
	- random genetic/physical/mental testing performed to grade ability

Demographics
- The Hub's life support and hydroponics labs can safely support up to 1250 people, but this population cap was quickly hit as early as 112 A.W. This led to significant food shortages, conflict, and even oxygen deprivation in portions of the Hub. Since then, the 'Grand Treaty' among the guilds produced the '1000 Man Rule', carefully monitored by the Scopers.
- Of the 1000 souls aboard the Hub, there will be 180 children (0-8) at any one point
- A third of the adult population is made to be unemployed (250)
- A 'golden ratio' of 55% females to 45% males is always maintained amongst the populace to ensure the survival of humanity
	- A blight took much of the crop in Hydroponics Lab 03 in 334 A.W., over a fifth of the male population was lost in order to save as many females as possible. In that rare time, the ratio achieved a 60/40 split. The succeeding populations continue to suffer that genetic consequences of a further 3% decrease in genetic diversity
- The infertility rate due to inbreeding continues to grow; by 800 A.W., as much as 12% of births fail due to genetic malformity, and the number of infertile women continues to climb. The latest Scoper report suggests humanity will become 'too near' to have a successful birth as early as 968 A.W.

![[Pasted image 20250107104112.png]]

| adults     | 820 |
| ---------- | --- |
| women      | 451 |
| men        | 369 |
| children   | 180 |
| employed   | 574 |
| unemployed | 246 |
# Recycle

# The Grand Treaty
"The Treaty" operates as the central government of the Hub. It is a partnership between the five guilds to maintain some measure of compromise and cooperation. A select group of elders from each guild join the Treaty, and all move into Sector [[4]], the central axis of the Hub. The council consists of 21 members, not including a Chairman who conducts meetings as a non-voting manner. Each guild is attributed a certain number of seats on the council, listed below:
- 5 seats to the [[Agra]] Union
- 6 seats to the [[Arator]]ship
- 5 seats to the [[Engr|Collective]]
- 4 seats to the [[Proctor|United Proctors' Guild]]
- 1 seat to the [[Scoper]] 

All legislative matters brought before the council require a supermajority to pass and a majority to fail. All seats must vote independently of each other in a secret ballot, to allow for some ambiguity and personal argument on the floor. There has never once been a seat vote differently than its own guild.

Since the latest amendment to the Treaty in 652 A.W., the Agra Union and Aratorship have voted synchronously on 94% of resolutions. Some arguments have been made to the disproportionate splitting of the seats, particularly when the Collective makes up 40% of the station's population and only 25% of the council. Amending the allocation of seats would require passage of a resolution, which requires a supermajority. There have been 192 separate Treaty amendments proposed on this topic since 652 A.W. The Aratorship and Agra Union have failed each one. 

The Treaty also dictates the drafting order of each cycle. The Scopers will always have the first pick, so long as they choose to select a candidate. They have not done so since 733 A.W. Then, the remaining order is chosen by resolution and vote.

![[Pasted image 20250114120500.png]]

# The Wipe
Nobody knows how long the Hub has existed, only how long it has been since the Wipe. When the Hub was first created, a collection of several independent spacecraft came together for an unknown reason. Their databanks and servers were shared equally to provide a common knowledge of basic spacecraft repair and spacewalk operating procedure. This worked well and allowed for a community to establish across several cultures and languages. Binary and code became intermediates for all discussions until one language and leadership could be established. This went on for some time, at least a generation or so, as best as anyone can guess.

Eventually, one of the tethered probes that was monitoring internal and external temperatures and pressures filled its own data storage. The original programming then called for a shutdown period during which all data of a certain age be deleted. This would provide the probe the ability to overwrite years' old measurements, which presumably would have been saved by whatever researcher was using this probe. When the probe shutdown, it locked the entire server network aboard the interspersed Hub and proceeded to delete everything it considered 'old.' Before humanity could shutdown the process, all of human history and a considerable amount of scientific writings had been purged. 
 
This was only the beginning. Conflict immediately rose. As best as the records reflect, a collection of modules declared a war upon the 'Americans' who had built the probe. What followed was a short, but devastating conflict which annihilated most of the remaining databanks and a majority of the original Hub, including the central hydroponics farm. 

The following period of famine led to more conflict and confusion. A great effort was made by the first 'Agra Union' to collectivize all of the information and supplies for the interspersed hydroponics farms throughout the Hub. This unionizing led to a full ceasefire and peace negotiation amongst humanity. 

By the time this period came to a close, some effort went into preserving scientific knowledge and mechanical descriptions of all modules on the Hub, but history was seemingly lost. When the next generation sought answers to the origin of humanity, so much data had been lost and the oldest generations had gone that there was no answer to give. 

This period became known as 'The Wipe'. 

The first historical writings use the founding of the Agra Union as the basis for its timeline, with the Peace Accords noted as the first piece of human history, occurring 3 A.W., 3 cycles after the Wipe. 

800 more cycles has continued to fill humanity with doubt and questioning. Handed-down stories tell of a blue planet as our origin, of a great Father bringing forth our hopeless existence, of us searching for a home for all time. The Scopers' Union has long searched for this blue planet, but none has been found suitable to call home and there is no official record of any planet in the databanks. 

As far as we know, there has only ever been the Hub, and the Hub will endure forever. It is Father, it is Mother, it is our Home. 


# The Breaking
In cycle 802 A.W., a severe malfunction occurs in the ion shield of the Hub, which normally protects it from in-orbit debris. This coincidentally is timed with a meteorite shower that rips the Hub apart. Sectors [[13]], [[16]], and [[9]] are sheared clean from the central body and sent crashing towards the atmosphere. Sectors [[20]], [[5]], and [[21]] are shredded entirely. Sectors [[10]], [[11]], and [[3]] pull adrift and eventually crash onto the nearby moon. Sectors [[8]], [[2]], and [[7]] are pushed deeper into space, sent to drift among the outer planets of the solar system. Sector [[4]] is ripped in half due to these two sections pulling it apart. 

Such a catastrophic event may have been the result of sabotage, but the level of decimation -- as much as 60% of humanity is killed in minutes -- could not have been intentional. Three major sections of the Hub are divorced from each other, and almost all contact is lost between them. The second and last sections achieve long-form radio communication before the latter drifts to the other side of the planet. Neither can help the other, and almost all ability to maneuver in space is lost. The first section crashes onto the planet's surface and must survive the toxic environment. 