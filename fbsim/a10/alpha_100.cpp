//game.cpp
//College Football League Simulator

#include <cstdint>
#include <cstdio>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>

//Team data
struct Team {

	std::string name;
	uint8_t wins = 0;
	uint8_t losses = 0;
	//	uint16_t rtg_off; //scale: 0-99; 99 = 42 ppg; 0 = 0 ppg
		//uint16_t rtg_def; //scale: 0-99; 99 = 0 ppg; 0 = 42 ppg
	uint16_t rtg_ovr;

};

int main(int argc, char** argv) {

	Team* teams = new Team[4];
	uint8_t games = 0;
	int8_t home_team = 0;
	int8_t away_team = 1;
	uint16_t numb_teams = 0;

	printf("How many teams are there? >");
	std::cin >> numb_teams;
	for (int i = 0; i < numb_teams; ++i) {

		printf("\nInput the name and rating.\n>");
		std::cin >> teams[i].name >> teams[i].rtg_ovr;
	}

	printf("Teams:\n %s\n %s\n %s\n %s\n", teams[0].name.c_str(), teams[1].name.c_str(), teams[2].name.c_str(), teams[3].name.c_str());
	printf("How many games are we playing? >");
	std::cin >> games;

	for (int i = 0; i < games; ++i) {
		printf("\nGame %d: Who is away? (team ##) >", i);
		std::cin >> away_team;
		printf("\nGame %d: Who is home? (team ##) >", i);
		std::cin >> home_team;

//		printf("Game 1: %s (%d Rating) vs. %s (%d Rating)!", teams[away_team-1].name.c_str(), teams[away_team-1].rtg_ovr, teams[home_team-1].name.c_str(), teams[home_team-1].rtg_ovr);
		printf("What is the Score? (A-H)\n>");
		uint16_t away_score = 0;
		uint16_t home_score = 0;
		std::cin >> away_score;
		std::cin.ignore(50, '-');
		std::cin >> home_score;
		printf("With a score of %d-%d...", away_score, home_score);
		if (home_score > away_score) {
			printf("The Home Team Wins! Go %s!\n", teams[home_team-1].name.c_str());
			teams[home_team].wins++;
			teams[away_team].losses++;
		}
		else {
			printf("The Away Team Wins! Go %s!\n", teams[away_team-1].name.c_str());
			teams[away_team].wins++;
			teams[home_team].losses++;
		}

	}

	//Print Records
	for (int i = 0; i < numb_teams; ++i) {
		printf("Team: %s, Rating: %d, Record: %d-%d\n", teams[i].name.c_str(), teams[i].rtg_ovr, teams[i].wins, teams[i].losses);
	}

	delete[] teams;
	return 0;

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

