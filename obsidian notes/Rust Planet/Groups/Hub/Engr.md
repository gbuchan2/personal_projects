# "Engr's Collective", "The Collective"
Smiths, Leatherworkers, seamsters, ship repair
Handles all maintenance and improvement aboard the Hub
Handles mass production of some items (occupation tools, clothes, etc)
The largest of the occupation unions

The largest and perhaps most important of the guilds is the Engr's Collective, or Collective for short. Engrs cover all manners of ship maintenance and repair, lighting, climate control, life support, clothing, tool making, and more. Each Engr is specialized into one of these roles, but all are required to receive an adequate teaching of each skill. During apprenticeship, masters will shadow their apprentice under several other masters and journeymen to introduce these topics. In this way, should an emergency occur, any of the 230 trained members of the guild will be able to respond. 

### Guild
Name: "Engr's Collective"
Emblem: Hammer and Wrench
Members: 230
Leader: [[Axel]]
[[Humanity#The Grand Treaty|Treaty]] Seats: 5

### Occupations:
- Coreman - Maintains the Fissile Cores
- Power Distro - Manages power delivery across the Hub
- Radioman - Maintains radio communications between sectors
- Lightman - Updates and replaces lighting as needed
- more

### Stations:
- Fissile Core: [[7]], [[11]], [[13]]
- Engr Station: [[3]], [[7]], [[11]], [[13]], [[20]]
- The Port: [[9]]
- Distribution Center: [[2]], [[4]], [[5]]

### Notables:
- [[Curie]]
- [[Cobal]]