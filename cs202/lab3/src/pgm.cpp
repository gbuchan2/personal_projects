#include "pgm.hpp"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <fstream>
using namespace std;

/*
 * PGM.CPP -- Lab 3
 * A Class file containing all the method definitions of the PGM Class
 * This Class manipulates the data found in a pgm file array
 * Its main functions include reading, writing, rotation, cropping, replicating, and padding
 * [META NOTE] This is perhaps the most inefficient program I have ever written, or I have the worst internet connection ever right now
 *	 Either way, I blame this on the Pitt game. 
 * 
 * Graham Buchanan
 * 13 September 2021 */


bool Pgm::Read(const std::string &file)
{
	ifstream fin;
	string s;
	size_t i, j, r, c, v;

	fin.open(file.c_str());
	if (fin.fail()) return false;

	if (!(fin >> s)) return false;
	if (s != "P2") return false;
	if (!(fin >> c) || c <= 0) return false;
	if (!(fin >> r) || r <= 0) return false;
	if (!(fin >> i) || i != 255) return false;

	Pixels.resize(r);
	for (i = 0; i < r; i++) {
		Pixels[i].clear();
		for (j = 0; j < c; j++) {
			if (!(fin >> v) || v > 255) return false;
			Pixels[i].push_back(v);
		}
	}
	if (fin >> s) return false;
	fin.close();
	return true;
}

bool Pgm::Write(const string &file) const {
	ofstream fout;
	string s;
	size_t i = 0;

	//ERROR CHECKING!
	//Make sure there is a vector that you can print
	//Try to open file (or make one), if fail, fail
	if(Pixels.empty())
		return false;
	fout.open(file.c_str());
	if(fout.fail() || !(fout.is_open())) {
		cerr << "Write File could not open. Try Again." << endl;
		return false;
	} else {
		fout << "P2" << endl << Pixels[0].size() << ' ' << Pixels.size() << endl << 255 << endl;

		//PRINTING IN A standard rows * 20 format for some reason
		//simple calculations:
		//Total number of pixels / no cols equals the row that you are on (already in index form)
		//Total number mod no coles equals the column that you are on (already in index form)
		//insert fancy printing conditions to determine next character
		for(i = 0; i < Pixels.size() * Pixels[0].size(); ++i) { 
			fout << Pixels[i / Pixels[0].size()][i % Pixels[0].size()];
			if(((i + 1) % 20) == 0) {
				fout << endl;
			} else if (i + 1 < Pixels.size() * Pixels[0].size()) {
				fout << ' ';
			} else
				fout << endl;
		}
		return true;
	}
}

//Create produces an array of pixel values based on given rows, cols, and pixel value variables
bool Pgm::Create(size_t r, size_t c, size_t pv) {
	size_t i = 0;
	vector <int> v;

	//ERROR CHECKING if pixel value is greater than the maximum value (or for some reason "negative"), fail
	//Clear out the old Pixel data if currently there
	//generate a new vector based upon the given value and then add that vector to the new Pixel data
	if(pv > 255)
		return false;
	else {
		if(!(Pixels.empty())) 
			Pixels.clear();
		for(i = 0; i < r; ++i) {
			v.resize(c, pv);
			for(i = 0; i < r; ++i)
				Pixels.push_back(v);
		}
		return true;
	}
}

//Cclockwise rotates the array 90 degress, counter-clockwise
bool Pgm::Cclockwise() {
	size_t i = 0;
	size_t j = 0;
	vector <vector<int> > v;

	//ERROR CHECKING if there is no Pixel data, fail
	//generate a vector to contain each row of ints
	//resize the old vector to easily destroy all old data and repurpose
	//load in new data based on simple calculation:
	//Flip rows & cols, then build back from the right (of the array)
	if(Pixels.size() == 0)
		return false;

	for(i = 0; i < Pixels.size(); ++i) v.push_back(Pixels[i]);
	Pixels.resize(1);
	Pixels[0].resize(v.size(), 0);
	Pixels.resize(v[0].size(), Pixels[0]);
	for(i = 0; i < v[0].size(); ++i) {
		for(j = 0; j < v.size(); ++j) {
			Pixels[i][j] = v[j][v[0].size()-i-1];
		}
	}
	return true;
}

//Clockwise rotates the array 90 degress, clockwise
bool Pgm::Clockwise() {
	size_t i = 0;
	size_t j = 0;
	vector <vector<int> > v;

	//ERROR CHECKING there must be something to rotate
	//Same exact methods as Cclockwise (see above)
	//simple calculations to determine new pixel data locations:
	//Flips rows and cols, then build from the bottom row
	if(Pixels.size() == 0)
		return false;
	for(i = 0; i < Pixels.size(); ++i) 
		v.push_back(Pixels[i]);

	Pixels.clear();
	Pixels.push_back(v[0]);
	Pixels[0].resize(v.size(), 0);
	Pixels.resize(v[0].size(), Pixels[0]);
	for(i = 0; i < v[0].size(); ++i) {
		for(j = 0; j < v.size(); ++j) {
			Pixels[i][j] = v[v.size()-j-1][i];
		}

	}
	return true;
}

//Pad adds a padding around the entire array based on given width and pixel value variables
bool Pgm::Pad(size_t w, size_t pv) {
	size_t i = 0;
	size_t j = 0;
	vector <int> v;

	//ERROR CHECKING pixel value must actually resemble a pixel unfortunately
	//Generate a vector of ints to store the pad for the top and bottom rows
	//Add that vector to the Pixel data as those top and bottom rows
	//Then, manually insert the padding pixels into the front and back of each row that is not entirely padding
	if(pv > 255)
		return false;
	v.resize(Pixels[0].size() + (2 * w), pv);
	for(j = 0; j < w; ++j) {
		Pixels.insert(Pixels.begin(), v);
		Pixels.push_back(v);
	}
	for(i = w; i < Pixels.size()-w; ++i) {
		for(j = 0; j < w; ++j) {
			Pixels[i].insert(Pixels[i].begin(), pv);
			Pixels[i].push_back(pv);
		}	
	}
	return true;
}

//Panel generates an array of the original array
bool Pgm::Panel(size_t r, size_t c) {
	size_t i = 0;
	size_t j = 0;
	size_t k = 0;
	vector <int> v;

	//ERROR CHECKING if there are "0" rows in the big array, fail
	//Part I, generate a vector to maintain the original row
	//Add each element of that vector back to the original row
	//Repeat until all have been extended
	if((r == 0) || (c == 0))
		return false;
	for(i = 0; i < Pixels.size(); ++i) {
		v = Pixels[i];
		for(j = 1; j < c; ++j) {
			for(k = 0; k < v.size(); ++k) {
				Pixels[i].push_back(v[k]);
			}
		}
		v.clear();
	}

	//Set a constant for the original no cols
	//Casually copy the original rows into the extended array
	k = Pixels.size();
	for(i = 1; i < r; ++i) {
		for(j = 0; j < k; ++j)
			Pixels.push_back(Pixels[j]);
	}
	return true;
}

//Crop shortens the array both in length and width
//It was also the reason one may have been missing half the lab without realizing it
bool Pgm::Crop(size_t r, size_t c, size_t rows, size_t cols) {
	size_t i = 0;
	size_t j = 0;
	vector <int> v;

	//ERROR CHECKING make sure the smaller array is not secretly bigger than the original
	//Generate a vector of ints to store each of the smaller rows data
	//store this data into smaller rows
	//Cut off the excess rows filled with old data
	if((r > Pixels.size()) || (rows + r > Pixels.size()) || (c > Pixels[0].size()) || (cols + c > Pixels[0].size()))
		return false;
	for(i = 0; i < rows; ++i) {
		for(j = 0; j < cols; ++j) {
			v.push_back(Pixels[r + i][c + j]);
		}
		Pixels[i].resize(cols);
		Pixels[i] = v;
		v.clear();
	}
	Pixels.resize(rows);
	return true;
}
