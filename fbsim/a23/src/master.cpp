//MASTER.HPP
//Driver Program
//Graham Buchanan

/***CHANGELOG***
 * 03 August 2022 - Added series of game simulation; 
 */

#include "master.hpp"

//constructor
//Loads Base save
master::master() {
	std::string a;
	a = "txt/BaseSave.txt";
	LoadState(a);
	Print();
}

master::~master() {
	printf("Thank you for playing!\n");
	std::string a;
	a = "txt/ExitSave.txt";
	SaveState(a);
	DeleteData();
}

//SAVE STATE
bool master::LoadState(std::string &filename) {

	FILE *file;
	int i;

	file = fopen(filename.c_str(), "r");
	if(file == NULL) { perror("Cannot open file!\n"); return false; }
	DeleteData();


	


	return true;
}
bool master::SaveState(std::string &filename) {

	FILE *file;
	time_t now = time(0);
	char *date = ctime(& now);
	int i;

	if(TV.empty()) { perror("There is nothing to save!\n"); return false; }
	file = fopen(filename.c_str(), "w");
	if(file == NULL) { perror("Cannot Open File!\n"); return false; }
	fprintf(file, "FBSIM\NDATE SAVED:%s\n%.4zx\n",date,TV.size());
	for(i = 0; i < TV.size(); ++i) {

		fprintf(file, "Team %2d\n", i);
		fprintf(file, "%30s%30s%3s\n",TV[i]->name.c_str(), TV[i]->location.c_str(), TV[i]->tag.c_str());
		fprintf(file, "%02x%02x%02x%02x%02x%02x%02x\n",TV[i]->id,TV[i]->poff,TV[i]->roff,TV[i]->olin,
				TV[i]->pdef,TV[i]->rdef,TV[i]->dlin);
		fprintf(file, "%04x%04x%04x%04x%04x\n",TV[i]->wins,TV[i]->loss,
				TV[i]->ties,TV[i]->ptsc,TV[i]->oppt);
		fprintf(file, "%04x%04x%04x%04x\n",TV[i]->opyd,TV[i]->dpyd,TV[i]->oryd,TV[i]->dryd);
	}
	return true;
}

//LEAGUE MANAGEMENT
bool master::CreateLeagues() {
	return true;
}

//TEAM MANAGEMENT
bool master::EnterTeams() {
	int notm = 0;
	Team *t = NULL;
	std::string s, a, b, c;
	uint8_t tmid = 0;
	uint64_t rtg = 0;
	uint64_t p,r,l;
	char name[65];
	char yn;
	int count = 0;


	while(1) {
		printf("\n");
		if(TV.size() > 0) Print();
		count = 0;
		do {
			if(count > 0) printf("ERROR. It appears the Location,Name,Tag values you have entered are not unique. The combination of the three must be unique to generate a key for the team. Please try again.\n");
			++count;
			printf("To exit, enter 'Q'. Please enter the Location (30 characters max), Name (30 characters max), and Tag (three characters) of your team separated by commas as shown (Location,Name,Tag).\n> ");
			std::cin.clear();
			std::getline(std::cin, s);
			if((s == "Q") || (s == "q")) {
				if(TV.size() > 0) Print();
				return true;
			}
			a = s.substr(0, s.find(',')); //location
			b = s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1); //name
			c = s.substr(1+s.rfind(',')); // tag
			while((a == "") || (a.length() > 30) || (b == "") || (b.length() > 30) || (c == "") || (c.length() != 3)) {
				printf("\nERROR. You have entered the wrong number of characters for at least one field. The Location and Name must be less than 31 characters. The tag must be 3 characters. Enter Location, Name, and Tag separated by commas as shown (Location,Name,Tag). Please try again.\n> ");
				s = "";
				a = "";
				b = "";
				std::cin.clear();
				std::getline(std::cin, s);
				a = s.substr(0, s.find(',')); //location
				b = s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1); //name
				c = s.substr(1+s.rfind(',')); // tag
			}
		} while(TM.find(s) != TM.end());
		printf("\nPlease input the team's offensive ratings in order (pass,run,line) on a scale of 1-251.\n> ");
		std::cin.clear();
		std::getline(std::cin, s);
		p = atoi(s.substr(0,s.find(',')).c_str());
		r = atoi(s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1).c_str());
		l = atoi(s.substr(1+s.rfind(',')).c_str());
		printf("P: %lu R: %lu L: %lu\n", p,r,l);
		while((p == 0) || (p > 251) || (r == 0) || (r > 251) || (l == 0) || (l > 251)) {
			printf("\nERROR. You have entered an incorrect value for one or more fields. All entered values must be on the range 1 <= x <= 251. Please enter the team's ratings separated by commas as shown (pass,run,line).\n> ");
			s = "";
			std::cin.clear();
			std::getline(std::cin, s);
			p = atoi(s.substr(0,s.find(',')).c_str());
			r = atoi(s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1).c_str());
			l = atoi(s.substr(1+s.rfind(',')).c_str());
		}
		rtg = (((p & 0xff) << 56) | ((r & 0xff) << 48) | ((l & 0xff) << 40));
		std::cout << "Rating: " << std::hex << rtg << std::endl;
		s = "";
		printf("\nPlease input the team's defensive ratings in order (pass,run,line) on a scale of 1-251.\n> ");
		std::cin.clear();
		std::getline(std::cin, s);
		p = atoi(s.substr(0,s.find(',')).c_str());
		r = atoi(s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1).c_str());
		l = atoi(s.substr(1+s.rfind(',')).c_str());
		printf("P: %lu R: %lu L: %lu\n", p,r,l);
		while((p == 0) || (p > 251) || (r == 0) || (r > 251) || (l == 0) || (l > 251)) {
			printf("\nERROR. You have entered an incorrect value for one or more fields. All entered values must be on the range 1 <= x <= 251. Please enter the team's ratings separated by commas as shown (pass,run,line).\n> ");
			s = "";
			std::cin.clear();
			std::getline(std::cin, s);
			p = atoi(s.substr(0,s.find(',')).c_str());
			r = atoi(s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1).c_str());
			l = atoi(s.substr(1+s.rfind(',')).c_str());
		}
		rtg |= (((p & 0xff) << 32) | ((r & 0xff) << 24) | ((l & 0xff) << 16));
		std::cout << "Rating: " << std::hex << rtg << std::endl;
		sprintf(name, "%s,%s,%s", a.c_str(), b.c_str(), c.c_str());
		s = name;
		t = new Team(TV.size(), s, rtg);
		TV.push_back(t);
		TM[s] = t;
	}

	printf("\nAll teams you have entered have been save!\n");
	return true;
}

//GAME MANAGEMENT
bool master::Play_A_Game() {
	return true;
}

//BASE
bool master::DeleteData() {
	delete TV[0];
}

void master::Print() {
	size_t i;
	for(i = 0; i < TV.size(); ++i) {
		printf("Team %zd: %s %s\n", i, TV[i]->location.c_str(), TV[i]->name.c_str());
	}
}

