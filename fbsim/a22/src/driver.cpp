//MAIN.CPP
//Driver Program
//Graham Buchanan

/***CHANGELOG***
 * 20 July 2022 - Creation and Entering Team Data
 * 03 August 2022 - Added series of game simulation; 
 */

#include "master.hpp"
int main() {
	master m;
	printf("WELCOME TO FBSIM VERSION ALPHA_22! PLEASE BEGIN BY INPUTTING YOUR TEAMS. ENJOY!\n");
	m.EnterTeams();
	m.Play_A_Game();
	m.DeleteData();
	printf("Thanks for playing!\n");
	return 0;
}
