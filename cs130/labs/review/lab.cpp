//lab.cpp
//Review of C++ -- GAME CODE PART 2
//Graham Buchanan
//25 January 2021
//Co-conspirators: TAs from Lab Section 3 (?)

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

//class declaring our storage container
class Room {

	public:
		//constructor and destructor
		Room();
		~Room();

		//vars necessary variables to store room data
		std::string title;
		std::string description;
		int north, south, east, west;

		//check if there is an exit out of each room
		std::string north_exit() const {
			if(north != -1)
				return " n";
			else
				return "";
		}
		std::string south_exit() const {
			if(south != -1)
				return " s";
			else 
				return "";
		}
		std::string east_exit() const {
			if(east != -1)
				return " e";
			else
				return "";
		}
		std::string west_exit() const {
			if(west != -1)
				return " w";
			else
				return "";
		}

		//print the invisible menu for the user
		char PrintMenu();
};

//class constructor define
Room::Room() {
	north = -1;
	south = -1;
	east = -1;
	west = -1;
};

//class destructor define
Room::~Room() {};

//program to locate the room exit(s) for each room
void extractInt(std::string line, int& loc_index) {
	std::stringstream ss;
	ss << line;
	std::string temp;
	int found = -1;
	while(!ss.eof()) {
		ss >> temp;
		if(std::stringstream(temp) >> found)
			loc_index = found;
	}
}

//class function define...menu
char Room::PrintMenu() {
	char c;
	std::cout << "> ";
	std::cin >> c;
	return c;
}


//program begins...booyah!
int main(int argc, char **argv) {

	//cond check arguments and file data
	if(argc < 2) {
		std::cout << "Incorrect usage, please supply file name." << std::endl;
		std::exit(1);
	}

	//open the file
	std::string fileName = argv[1];
	std::fstream fin(fileName);

	//vars nec variables
	int num_tildes = 0; //counts tildes ie rooms
	std::string line; //getline string

	//file error check
	if(!fin.is_open()) {
		std::cout << "Error opening file." << std::endl;
		std::exit(1);
	}
	
	//loop first loop to count number of rooms
	while(getline(fin, line)) {

		//loop simple loop to locate tildes
		for(unsigned int i = 0; i <line.size(); ++i) {
			if(line[i] == '~')
				++num_tildes;
		}
	}

	//vars creating room index
	int room_index = (num_tildes / 3);
	
	//class creating our class
	Room *r = new Room[room_index];

	//var reset counter
	num_tildes = 0;

	//close the file
	fin.close();

	//reopen file (RAN INTO ERRORS WITH SECOND LOOP)
	fin.open(fileName);
	if(!fin.is_open()) {
		std::cout << "Error opening file." << std::endl;
		std::exit(1);
	}
	
	//loop second loop to store game files
	while(getline(fin, line)) {

		//recalc index each cycle
		room_index = (num_tildes / 3);

		//reset index each cycle
		int loc_index = -1;

		//cond new room == save title
		if( (num_tildes % 3) == 0) {		
			if( !(line[0] == '~') )
				r[room_index].title = line;
		}
		//cond already have a room == save desc
		if( (num_tildes % 3) == 1) {
			if( !(line[0] == '~') )
				r[room_index].description += line + '\n';
		}
		//cond already have title + desc == save exits
		if( (num_tildes % 3) == 2) {
			 if( !(line[0] == '~') ) {
				 //run function to calc exits
				extractInt(line, loc_index);

				//loop check for exit calls
				for(unsigned int j = 0; j < line.size(); ++j) {
					if(line[j] == 'n')
						r[room_index].north = loc_index;
					if(line[j] == 's')
						r[room_index].south = loc_index;
					if(line[j] == 'e')
						r[room_index].east = loc_index;
					if(line[j] == 'w')
						r[room_index].west = loc_index;
				}
			}
		}

		//loop recalc counter 
		for(unsigned int i = 0; i <line.size(); ++i) {
			if(line[i] == '~')
				++num_tildes;
		}
	}

	//close file
	fin.close();

	//GAME CODE

	//reset room index + user select
	room_index = 0;
	char userSelect = '\0';

	//loop as long as no 'q' QUIT
	while( (userSelect = r[room_index].PrintMenu()) != 'q')
	{
		//cond switch case check for user input
		switch(userSelect) {
			case 'n':
				{
					//cond is there an exit?
					if(r[room_index].north > -1) {
						std::cout << "You moved NORTH." << std::endl;
						room_index = r[room_index].north;
					}
					else
						std::cout << "You can't go NORTH!" << std::endl;
					break;
				}
			case 'l':
				{
					//print room title desc and exits
					std::cout << r[room_index].title << std::endl << r[room_index].description << std::endl;
					std::cout << "Exits: " << r[room_index].north_exit() << r[room_index].south_exit() << r[room_index].east_exit() << r[room_index].west_exit() << std::endl;
					break;
				}
			case 's':
				{
					//cond is there an exit?
					if(r[room_index].south > -1) {
						std::cout << "You moved SOUTH." << std::endl;
						room_index = r[room_index].south;
					}
					else
						std::cout << "You can't go SOUTH!" << std::endl;
					break;
				}
			case 'e':
				{
					//cond is there an exit?
					if(r[room_index].east > -1) {
						std::cout << "You moved EAST." << std::endl;
						room_index = r[room_index].east;
					}
					else
						std::cout << "You can't go EAST!" << std::endl;
					break;
				}
			case 'w':
				{
					//cond is there an exit?
					if(r[room_index].west > -1) {
						std::cout << "You moved WEST." << std::endl;
						room_index = r[room_index].west;
					}
					else
						std::cout << "You can't go WEST!" << std::endl;
					break;
				}
			default:
				break;
		}
	}
					

	//delete the array of objects
	delete[] r;

	return 0;
}



