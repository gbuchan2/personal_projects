//MASTERTEAM.CPP
//TEAM MANAGEMENT
//Graham Buchanan

/***CHANGELOG***
 * 03 August 2022 - Creation; 
 */

#include "master.hpp"

bool master::EnterTeams() {

	int notm = 0;
	Team *t = NULL;
	std::string s, a, b, c;
	uint8_t tmid = 0;
	uint64_t rtg = 0;
	uint64_t p,r,l;
	char name[65];
	char yn;
	int count = 0;


	while(1) {
		printf("\n");
		if(TV.size() > 0) Print();
		count = 0;
		do {
			if(count > 0) printf("ERROR. It appears the Location,Name,Tag values you have entered are not unique. The combination of the three must be unique to generate a key for the team. Please try again.\n");
			++count;
			printf("To exit, enter 'Q'. Please enter the Location (30 characters max), Name (30 characters max), and Tag (three characters) of your team separated by commas as shown (Location,Name,Tag).\n> ");
			std::cin.clear();
			std::getline(std::cin, s);
			if((s == "Q") || (s == "q")) {
				if(TV.size() > 0) Print();
				return true;
			}
			a = s.substr(0, s.find(',')); //location
			b = s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1); //name
			c = s.substr(1+s.rfind(',')); // tag
			while((a == "") || (a.length() > 30) || (b == "") || (b.length() > 30) || (c == "") || (c.length() != 3)) {
				printf("\nERROR. You have entered the wrong number of characters for at least one field. The Location and Name must be less than 31 characters. The tag must be 3 characters. Enter Location, Name, and Tag separated by commas as shown (Location,Name,Tag). Please try again.\n> ");
				s = "";
				a = "";
				b = "";
				std::cin.clear();
				std::getline(std::cin, s);
				a = s.substr(0, s.find(',')); //location
				b = s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1); //name
				c = s.substr(1+s.rfind(',')); // tag
			}
		} while(TM.find(s) != TM.end());
		printf("\nPlease input the team's offensive ratings in order (pass,run,line) on a scale of 1-251.\n> ");
		std::cin.clear();
		std::getline(std::cin, s);
		p = atoi(s.substr(0,s.find(',')).c_str());
		r = atoi(s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1).c_str());
		l = atoi(s.substr(1+s.rfind(',')).c_str());
		printf("P: %lu R: %lu L: %lu\n", p,r,l);
		while((p == 0) || (p > 251) || (r == 0) || (r > 251) || (l == 0) || (l > 251)) {
			printf("\nERROR. You have entered an incorrect value for one or more fields. All entered values must be on the range 1 <= x <= 251. Please enter the team's ratings separated by commas as shown (pass,run,line).\n> ");
			s = "";
			std::cin.clear();
			std::getline(std::cin, s);
			p = atoi(s.substr(0,s.find(',')).c_str());
			r = atoi(s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1).c_str());
			l = atoi(s.substr(1+s.rfind(',')).c_str());
		}
		rtg = (((p & 0xff) << 56) | ((r & 0xff) << 48) | ((l & 0xff) << 40));
		std::cout << "Rating: " << std::hex << rtg << std::endl;
		s = "";
		printf("\nPlease input the team's defensive ratings in order (pass,run,line) on a scale of 1-251.\n> ");
		std::cin.clear();
		std::getline(std::cin, s);
		p = atoi(s.substr(0,s.find(',')).c_str());
		r = atoi(s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1).c_str());
		l = atoi(s.substr(1+s.rfind(',')).c_str());
		printf("P: %lu R: %lu L: %lu\n", p,r,l);
		while((p == 0) || (p > 251) || (r == 0) || (r > 251) || (l == 0) || (l > 251)) {
			printf("\nERROR. You have entered an incorrect value for one or more fields. All entered values must be on the range 1 <= x <= 251. Please enter the team's ratings separated by commas as shown (pass,run,line).\n> ");
			s = "";
			std::cin.clear();
			std::getline(std::cin, s);
			p = atoi(s.substr(0,s.find(',')).c_str());
			r = atoi(s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1).c_str());
			l = atoi(s.substr(1+s.rfind(',')).c_str());
		}
		rtg |= (((p & 0xff) << 32) | ((r & 0xff) << 24) | ((l & 0xff) << 16));
		std::cout << "Rating: " << std::hex << rtg << std::endl;
		sprintf(name, "%s,%s,%s", a.c_str(), b.c_str(), c.c_str());
		s = name;
		t = new Team(TV.size(), s, rtg);
		TV.push_back(t);
		TM[s] = t;
	}

	printf("\nAll teams you have entered have been save!\n");
	return true;
}
