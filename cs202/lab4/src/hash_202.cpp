//Hash_202.cpp
//Implementing the Hash_202 class functions
//Graham Buchanan
//18 Sept 2021





/* Welcome to the Hash_202 Class Definitions
 * This program has been amazingly simple, yet frustrating to write...
 * In short, this program generates a hash table filled with keys and values
 * based on the two hash functions XOR and Last7
 * and the two Collision resolution strategies Linear Probing and Double Hashing
 *
 * The user specifies the table size, hash functions, and collision resolutions strategy
 * from the command line arguments.
 * This class handles exclusively the hash table itself, no UI. 
 * The hash table is made of two vectors Vals and Keys, storing their respective strings
 * 
 * I tried to use exceptions for practice and because it makes it a little easier than a bunch of if/else statements
 * Enjoy!
 * */

#include "hash_202.hpp"

std::string Hash_202::Set_Up(size_t table_size, const std::string &fxn, const std::string &collision) {

	//Check for user input errors
	try {
		if (!(Keys.empty()))
			throw((std::string) "Hash table already set up");
		if (table_size == 0)
			throw((std::string) "Bad table size");
		if ((fxn != "Last7") && (fxn != "XOR"))
			throw((std::string) "Bad hash function");
		if ((collision != "Linear") && (collision != "Double"))
			throw((std::string) "Bad collision resolution strategy");
	} catch (std::string s) {
		return s;
	}

	//Here, you resize the vectors ("table") based upon the given size
	//Reset every variable to 0 and grab the orders for collision and hash function
	Keys.resize(table_size, "");
	Vals.resize(table_size, "");
	Nkeys = 0;
	Fxn = fxn[0];
	Coll = collision[0];
	Nprobes = 0;
	return ""; 
}

std::string Hash_202::Add(const std::string &key, const std::string &val) {
	
	//Check for user input errors
	//Check that all of key's characters are hex digits
	//Make sure that val is a real thing
	try {
		if(Keys.empty())
			throw((std::string) "Hash table not set up");
		if(key.length() == 0)
			throw((std::string) "Empty key");
		ss.clear();
		ss.str(key);
		if(key.find_first_not_of("0123456789ABCDEFabcdef") != std::string::npos)
			throw((std::string) "Bad key (not all hex digits)");
		if(val.length() == 0)
			throw((std::string) "Empty val");
		if(Nkeys == Keys.size())
			throw((std::string) "Hash table full");

		//Last7
		//if key is shorter than 7 chars, keep key, else find the last 7 digits
		b = 0;
		ss.clear();
		(key.length() / 7) == 0 ? ss.str(key) : ss.str(key.substr(key.length() - 7, 7));
		ss >> std::hex >> b;

		//XOR
		//divide the key into pieces 7 chars long, xor each piece with the 'total'
		a = 0;
		for(i = 0; i < (key.length() / 7) + 1; ++i) {
			h2 = 0;
			ss.clear();
			ss.str(key.substr(i * 7, 7));
			ss >> std::hex >> h2;
			a ^= h2;
		}
		//a = XOR
		//b = L7
		
		//Depending on hash function, assign the hashes
		h1 = (Fxn == 'L') ? b : a;
		h2 = (Fxn == 'L') ? a : b;
		flag = false;

		//Linear Probing - if the correct index is occupied, step by one and check the next, continue on
		//if the same key is found, throw exception
		if(Coll == 'L') {
			for(i = 0; i < Keys.size(); ++i) {
				a = (h1 + i) % Keys.size();
				if(Keys[a] == "") {
					Keys[a] = key;
					Vals[a] = val;
					flag = true;
					Nkeys++;
					break;
				} else if(Keys[a] == key)
					throw((std::string) "Key already in the table");
			}
		} else { //DOUBLE - Use h2 to calculate both the correct index and the step
			for(i = 0; i < Keys.size(); ++i) {
				((h2 % Keys.size()) == 0) ? h2++ : h2;
				a = (h1 + h2 * i) % Keys.size();
				if(Keys[a] == "") {
					Keys[a] = key;
					Vals[a] = val;
					flag = true;
					Nkeys++;
					break;
				} else if(Keys[a] == key) 
					throw((std::string) "Key already in the table");
			}
		}
		if(!flag)
			throw((std::string) "Cannot insert key");
	} catch(std::string s) {
		return s;
	}
	//if somehow falls out of try/catch, return blank
	return "";
}

std::string Hash_202::Find(const std::string &key) {
	//Check for user input errors
	try {
		if(Keys.empty())
			throw((std::string) "");
		ss.clear();
		ss.str(key);
		if(key.find_first_not_of("0123456789ABCDEFabcdef") != std::string::npos)
			throw((std::string) "");

		//Last7 - defined above
		b = 0;
		ss.clear();
		(key.length() / 7) == 0 ? ss.str(key) : ss.str(key.substr(key.length() - 7, 7));
		ss >> std::hex >> b;

		//XOR - defined above
		a = 0;
		for(i = 0; i < (key.length() / 7) + 1; ++i) {
			h2 = 0;
			ss.clear();
			ss.str(key.substr(i * 7, 7));
			ss >> std::hex >> h2;
			a ^= h2;
		}

		//reset Nprobes and assign h1 and h2
		Nprobes = 0;
		h1 = (Fxn == 'L') ? b : a;
		h2 = (Fxn == 'L') ? a : b;
		flag = false;

		if(Coll == 'L') { //Linear Probing - defined above
			for(i = 0; i < Keys.size(); ++i) {
				a = (h1 + i) % Keys.size();
				if(Keys[a] == key) { //Once the key has been found, return the associated value
					flag  = true;
					return Vals[a];
				} else
					Nprobes++;
			}
		}
		else { //DOUBLE - defined above
			for(i = 0; i < Keys.size(); ++i) {	
				((h2 % Keys.size()) == 0) ? h2++ : h2;
				a = (h1 + h2 * i) % Keys.size();
				if(Keys[a] == key) {
					flag = true;
					return Vals[a];
				} else //If the key is not found, increment Nprobes
					Nprobes++;
			}
		}
		if(!flag)
			throw((std::string) "");
	} catch(std::string s) {
		return s;
	}
	return "";
}

void Hash_202::Print() const {

	//Check that there is a table, then cycle through each index and print if there is a key
	size_t e = 0;
	if(!Keys.empty()) {
		for(e = 0; e < Keys.size(); ++e) {
			if(Keys[e] != "")
				printf("%5zu %s %s\n", e, Keys[e].c_str(), Vals[e].c_str());
		}

	}
}

size_t Hash_202::Total_Probes() {

	//Check that there is a table, then cycle through each index and identify a key
	size_t e = 0;
	try {
		if(Keys.empty())
			throw((std::string) "ERROR");
		//Run Find for each Key index that holds a key, then sum Nprobes
		for(j = 0; j < Keys.size(); ++j) {
			if(Keys[j] != "") {
				Find(Keys[j]);
				e += Nprobes;
			}
		}
	} catch(...) {
		return 0;
	}
	return e;
}
