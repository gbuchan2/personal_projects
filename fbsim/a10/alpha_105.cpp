//ALPHA 1.05 
//Preparing "Building a Schedule"
//College Football League Simulator

#include <iostream>
#include <vector>
#include <string>
#include <cstdio>
#include <set>
#include <map>

class Team {
  public:
		std::string Name;
    int off;
    int def;
		int wins = 0;
		int losses = 0;
    void Print() const;
};

class TMS {
	public:
		bool Read();
		Team *Find(const std::string &name);
		void Print_By_Offense() const;
		void Print_By_Defense() const;
		void Print_By_Name() const;
		void Print_By_Wins() const;
		void Play_Game(const std::string &Away, const std::string &Home);
	protected:
		std::vector <Team *> TV;
		std::map <std::string, Team *> TM;
};

/* THE MAIN FUNCTION/.....
 *
 * STOP LOSING IT!!!!! */

int main() {
	TMS tms;
//	Team *t;
//	const Team *t;

	printf("Welcome to my Football Simulator version alpha_0105.\nThe current simulator will only progress through an 'as-run' schedule, meaning you will specify the teams, ratings, and then the number of games.\nBefore each game, you will be asked who is playing.\nPlease keep in mind there are no tie games in College Football!\nPlease follow all instructions as they come.\n");
	std::string away_team;
	std::string home_team;
	int games = 0;
	tms.Read();
	int i;
	printf("How many games are we playing?\n");
	std::cin >> games;
	for(i = 0; i < games; ++i) {
		printf("Game %d:\n", i + 1);
		printf("Who is Away? > ");
		std::cin >> away_team;
		printf("Who is Home? > ");
		std::cin >> home_team;
		tms.Play_Game(away_team, home_team);
	}

	tms.Print_By_Wins();
  return 0;
}

void Team::Print() const {
  printf("%s (%d-%d): %d off, %d def.\n", Name.c_str(), wins, losses, off, def);
}

bool TMS::Read() {
	Team *t;
	int notms = 0;
	printf("How many teams are there? > ");
	std::cin >> notms;
	if(notms < 2) {
		printf("There are not enough teams to do anything.\n");
		exit(1);
	}

	for(int i = 0; i < notms; ++i) {
		t = new Team;
		printf("Please input the Name (string), Offensive Rating (int), and Defensive Rating (int).\n");
		std::cin >> t->Name >> t->off >> t->def;
		TV.push_back(t);
	}

	for(int i = 0; i < notms; ++i) TV[i]->Print();

	for(unsigned long i = 0; i < TV.size(); ++i) {
		t = TV[i];
		TM[t->Name] = t;
	}

	return true;
}

//Find returns the Team class object by name
Team *TMS::Find(const std::string &name) {
	std::map<std::string, Team *>::iterator tit;
	tit = TM.find(name);
	if(tit == TM.end()) return NULL;
	return tit->second;
}

void TMS::Print_By_Offense() const {

	Team *t;
	size_t i;
	std::multimap <double, Team *> m;
	std::multimap <double, Team *>::const_iterator mit;

	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->off, t));
	}

	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Defense() const {

	Team *t;
	size_t i;
	std::multimap <double, Team *> m;
	std::multimap <double, Team *>::const_iterator mit;

	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->def, t));
	}

	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Name() const {

	std::map <std::string, Team *> m;
	std::map <std::string, Team *>::const_iterator mit;
	size_t i;
	Team *t;

	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m[t->Name] = t;
	}

	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Wins() const {

	std::map <int, Team *> m;
	std::map <int, Team *>::const_iterator mit;
	size_t i;
	Team *t;

	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m[-t->wins] = t;
	}

	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}


void TMS::Play_Game(const std::string &Away, const std::string &Home) {
	
	int home_score;
	int away_score;
	Team *a;
	Team *h;
	a = Find(Away);
	h = Find(Home);
	if(a == NULL)
		printf("The Away Team cannot be found.\n");
	else if(h == NULL) 
		printf("The Home Team cannot be found.\n");
	else {
		printf("%s vs. %s:\n", Away.c_str(), Home.c_str());
		a->Print();
		h->Print();
		printf("What is the final score (Format: Away-Home)? > ");
		std::cin >> away_score;
		std::cin.ignore(50, '-');
		std::cin >> home_score;
		if (away_score > home_score) {
			printf("Final Score: %s %d, %s %d. \nCongratulations %s!\n", Away.c_str(), away_score, Home.c_str(), home_score, Away.c_str());
			a->wins++;
			h->losses++;
		}
		else if (home_score > away_score) {	
			printf("Final Score: %s %d, %s %d. \nCongratulations %s!\n", Home.c_str(), home_score, Away.c_str(), away_score, Home.c_str());
			h->wins++;
			a->losses++;
		}
		else {
			printf("There was a tie!\n");
		}
	}
}
