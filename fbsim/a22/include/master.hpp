//MASTER.HPP
//Driver Program
//Graham Buchanan

/***CHANGELOG***
 * 03 August 2022 - Added series of game simulation; 
 */

#include "game.hpp"
/*
 * #include "league.hpp"
 */

struct master {

	//TEAM MANAGEMENT
	bool EnterTeams();
	

	//GAME MANAGEMENT
	bool Play_A_Game();
	

	//LEAGUE MANAGEMENT
	bool DeleteData();
	void Print();

	std::map<std::string, Team *> TM;
	std::vector<Team *> TV;
	std::vector<Game *> GV;
};
