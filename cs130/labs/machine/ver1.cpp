//ver1.cpp -- building UI
//Machine Code -- Below Assembly
//Graham Buchanan
//12 March 2021

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdio>

struct Machine {
	unsigned int regs[32];
	unsigned int pc;
	unsigned int num_instructions;
	unsigned int *instructions;

	bool reg(const std::string &name, unsigned int &val);
};

const int NUM_REGS = 32;

const std::string reg_names[] = {
	"$zero",
	"$at",
	"$v0",
	"$v1",
	"$a0",
	"$a1",
	"$a2",
	"$a3",
	"$t0",
	"$t1",
	"$t2",
	"$t3",
	"$t4",
	"$t5",
	"$t6",
	"$t7",
	"$s0",
	"$s1",
	"$s2",
	"$s3",
	"$s4",
	"$s5",
	"$s6",
	"$s7",
	"$t8",
	"$t9",
	"$k0",
	"$k1",
	"$gp",
	"$sp",
	"$fp",
	"$ra"
};

void DecodeInstr (Machine m, int instr) {
	int op_mask = 63;
	int op_code = (instr >> 26) & op_mask;
	int sub_op = instr & op_mask;
	int offset = instr & 0xffff; 
	int sa = (instr >> 6) & 31;
	int rs = (instr >> 21) & 31;
	int rt = (instr >> 16) & 31;
	int rd = (instr >> 11) & 31;

	switch(op_code) {
		case 0: {
			switch(sub_op) {
				case 0: {
					//SLL
					printf("SNL\n");
					m.regs[rt] = m.regs[rd] << sa;
					break;
				}

				case 2: {
					//SRL
					printf("SRL\n");
					m.regs[rd] = (m.regs[rt] >> sa);
					break;
				}

				case 3: {
					//SRA
					printf("SRA\n");
					m.regs[rd] = (m.regs[rt] >> sa);
					break;
				}

				case 32: {
					//ADD
					printf("ADD\n");
					m.regs[rd] = m.regs[rs] + m.regs[rt];
					break;
				}

				case 36: {
					 //AND
					 printf("AND\n");	
					 m.regs[rd] = m.regs[rs] & m.regs[rt];
					 break;
				}

				case 37: {
					 //OR
					 printf("OR\n");	
					 m.regs[rd] = m.regs[rs] | m.regs[rt];
					 break;
				}

				case 42: {
					//SLT
					printf("SLT\n"); 
					if(m.regs[rs] < m.regs[rt])
						m.regs[rd] = 1;
					else
						m.regs[rd] = 0;
					break;
				}

				default:
					break;
			}
			break;
		}

		case 4: { 
			//parse for pieces of beq, do function
			printf("BEQ\n");
			if(m.regs[rs] == m.regs[rt]) {
				m.pc += (offset - 4);
			}
			break;
		}
		
		case 5: {
			//BNE
			if( !(m.regs[rs] == m.regs[rt]) ) {
				m.pc += (offset - 4);
			}
			break;
		}
		
		case 8: {
			//ADDI
			m.regs[rt] = m.regs[rs] + offset;
			break;
		}

		default:
			break;
	}
}



int main(int argv, char **argc) {

	/* BUILDING THE USER INTERFACE....STEAL FROM REVIEW LAB */
	std::string user_comm;
	std::string user_mod;
	unsigned int size = 0;
	FILE *fl;
	Machine mach;
	mach.pc = 0;
	for(int i = 0; i < NUM_REGS; ++i) {
		mach.regs[i] = 0;
	}

	while(user_comm != "quit") {
		std::cout << "> ";
		std::cin >> user_comm;
		if(user_comm == "quit") 
			return 0;

		if(user_comm == "regs") {
				//dump all regs in hex
				for(int j = 0; j < NUM_REGS; ++j) {
					std::cout << std::setw(5) << reg_names[j] <<": 0x" << std::hex << std::setfill('0') << std::setw(8) << mach.regs[j] << " (" << std::dec << std::setfill(' ') << std::setw(5) << mach.regs[j] << ") ";
					if( ( (j % 3) == 0) && j > 0)
						std::cout << '\n';
				}
				std::cout << "PC   : " << mach.pc << '\n';
			}
		
		else if(user_comm == "reg") {
				//dump user_mod in hex & dec
				//user_mod MUST be in form "$xx"
			std::cin >> user_mod;
				unsigned int value = 0;
				if(mach.reg(user_mod, value) ==  true)
					std::cout << std::left <<  std::setw(5) << user_mod <<": 0x" << std::hex << std::setfill('0') << std::setw(8) << value << " (" << std::dec << std::setfill(' ') << std::setw(5) << value << ") \n";
			}
			
		else if(user_comm == "next") {
				//run next instruction and stop
				DecodeInstr(mach, mach.instructions[mach.pc / 4]);
				mach.pc += 4;
			}
			
		else if(user_comm == "run") {
				//run file until no instructions (PC > size)
				while( !(mach.pc > size) ) {
					DecodeInstr(mach, mach.instructions[mach.pc / 4]);
					mach.pc += 4;
				}
		}
			
		else if(user_comm == "load") {
				//load a new program from user_mod
			std::cin >> user_mod;
				fl = fopen(user_mod.c_str(), "rb");
				if(NULL == fl) {
					perror("fl");
					return -1;
				}
				fseek(fl, 0, SEEK_END);
				size = ftell(fl);
				fseek(fl, 0, SEEK_SET);
				mach.num_instructions = (size / 4);
				mach.instructions = new unsigned int[mach.num_instructions];
				fread(mach.instructions, 1, mach.num_instructions, fl);
				fclose(fl);
				mach.pc = 0;
				for(int i = 0; i < NUM_REGS; ++i) {
					mach.regs[i] = 0;
				}
			}
		}
	return 0;
}



bool Machine::reg(const std::string &name, unsigned int &val) {
	for (int i = 0; i < NUM_REGS; i++) {
		if (name == reg_names[i]) {
			val = regs[i];
			return true;
		}
	}
	return false;
}
