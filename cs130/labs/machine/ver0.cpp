//lab6.cpp
//Machine Code -- Below Assembly
//Graham Buchanan
//12 March 2021

#include <iostream>
#include <fstream>
#include <cstdio>

struct Machine {
	unsigned int regs[32];
	unsigned int pc;
	unsigned int num_instructions;
	unsigned int *instructions;

	bool reg(const std::string &name, unsigned int &val);
};

const int NUM_REGS = 32;

const std::string reg_names[] = {
	"$zero",
	"$at",
	"$v0",
	"$v1",
	"$a0",
	"$a1",
	"$a2",
	"$a3",
	"$t0",
	"$t1",
	"$t2",
	"$t3",
	"$t4",
	"$t5",
	"$t6",
	"$t7",
	"$s0",
	"$s1",
	"$s2",
	"$s3",
	"$s4",
	"$s5",
	"$s6",
	"$s7",
	"$t8",
	"$t9",
	"$k0",
	"$k1",
	"$gp",
	"$sp",
	"$fp",
	"$ra"
};



int main(int argv, char **argc) {

	if(argv < 2) {
		printf("User Input error.\n");
		return -1;
	}

	FILE *fl;
	fl = fopen(argc[1], "rb");
	if(NULL == fl) {
		perror("fl");
		return -1;
	}


	fseek(fl, 0, SEEK_END);

	int size = ftell(fl);
	Machine m;

	
	for (int r = 0; r < 32; ++r) {
		m.regs[r] = 0;
	}

	for(int p = 0; p < 32; ++p) {
		printf("Value of Register No. %d : %d\n", p, m.regs[p]);
	}
	m.num_instructions = (size / 4);
	m.instructions = new unsigned int[m.num_instructions];
	printf("Size of File: %u\n", size);
	fseek(fl, 0, SEEK_SET);


	for(unsigned int count = 0; count < m.num_instructions; ++count) {
	
	fread(&m.instructions[count], 1, sizeof(int), fl);
		printf("Instruction No. %d : %x\n", count, m.instructions[count]);
	}
	unsigned int op_mask = 0b000000;

	m.regs[4] = 32;
	m.regs[9] = 43;
	m.regs[10] = 52;
	for(unsigned int j = 0; j < m.num_instructions; ++j) {
			int a = (m.instructions[j] >> 26) | op_mask; //grab opcode
			int c = m.instructions[j] & 63; //grab subopcode
			printf("a: %d, c: %d\n", a, c);

			switch(a) {
			case 0: {
				printf("Special Code.\n");
				switch(c) {

					case 0: {
						//SLL
						printf("SNL\n");
						int sa = (m.instructions[j] >> 6) & 31;
						int rd = (m.instructions[j] >> 11) & 31;
						int rt = (m.instructions[j] >> 16) & 31;
						m.regs[rt] = m.regs[rd] << sa;
						break;
					}

					case 2: {
						//SRL
						printf("SRL\n");
						int sa = (m.instructions[j] >> 6) & 31;
						int rd = (m.instructions[j] >> 11) & 31;
						int rt = (m.instructions[j] >> 16) & 31;
						m.regs[rd] = (m.regs[rt] >> sa);
						break;
					}

					case 3: {
						//SRA
						printf("SRA\n");
						int sa = (m.instructions[j] >> 6) & 31;
						int rd = (m.instructions[j] >> 11) & 31;
						int rt = (m.instructions[j] >> 16) & 31;
						m.regs[rd] = (m.regs[rt] >> sa);
						break;
					}

					case 32: {
						 //ADD
						 printf("ADD\n");
						 int rd = (m.instructions[j] >> 11) & 31;
						 int rt = (m.instructions[j] >> 16) & 31;
						 int rs = (m.instructions[j] >> 21) & 31;
						 m.regs[rd] = m.regs[rs] + m.regs[rt];
						 break;
					 }

					case 36: {
						 //AND
						 printf("AND\n");	
						 int rd = (m.instructions[j] >> 11) & 31;
						 int rt = (m.instructions[j] >> 16) & 31;
						 int rs = (m.instructions[j] >> 21) & 31;
						 m.regs[rd] = m.regs[rs] & m.regs[rt];
						 break;
					 }

					case 37: {
						 //OR
						 printf("OR\n");	
						 int rd = (m.instructions[j] >> 11) & 31;
						 int rt = (m.instructions[j] >> 16) & 31;
						 int rs = (m.instructions[j] >> 21) & 31;
						 m.regs[rd] = m.regs[rs] | m.regs[rt];
						 break;
					 }

					case 42: {
						//SLT
						printf("SLT\n"); 
						int rd = (m.instructions[j] >> 11) & 31;
						int rt = (m.instructions[j] >> 16) & 31;
						int rs = (m.instructions[j] >> 21) & 31;
						if(m.regs[rs] < m.regs[rt])
							m.regs[rd] = 1;
						else
							m.regs[rd] = 0;
						break;
					 }

					default:
						break;

				}
				break;
			}
			
			case 4: { 
				//parse for pieces of beq, do function
				printf("BEQ\n");
				int o = m.instructions[j] | 0x0000;
				int rt = (m.instructions[j] >> 16) | 0b00000;
				int rs = (m.instructions[j] >> 21) | 0b00000;
				if(m.regs[rs] == m.regs[rt]) {
					m.pc += o;
				}
				break;
			}
			case 5: {
				//BNE
				printf("BNE\n");
				int o = m.instructions[j] | 0x0000;
				int rt = (m.instructions[j] >> 16) | 0b00000;
				int rs = (m.instructions[j] >> 21) | 0b00000;
				if( !(m.regs[rs] == m.regs[rt]) ) {
					m.pc += o;
				}
				break;
			}
			case 8: {
				//ADDIU
				printf("ADDIU\n");
				int i = m.instructions[j] | 0x0000;
				int rt = (m.instructions[j] >> 16) | 0b00000;
				int rs = (m.instructions[j] >> 21) | 0b00000;
				m.regs[rt] = m.regs[rs] + i;
				break;
			}

			default:
				printf("Nothing done.\n");
				break;
		}
	}
	printf("%x \n", m.instructions[0]);
	for(int p = 0; p < 32; ++p) {
		printf("Value of Register No. %d : %d\n", p, m.regs[p]);
	}
	return 0;
}
