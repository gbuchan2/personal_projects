#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

/* Put your SSID & Password */
const char* ssid = "NodeMCU";  // Enter SSID here
const char* password = "12345678";  //Enter Password here

const int fenA = D1;
const int fin1 = D0;
const int fin2 = D2;
const int fenB = D5;
const int fin3 = D3;
const int fin4 = D4;
const int benA = D6;
const int bin1 = D7;
const int bin2 = D8;
const int benB = 1;
const int bin3 = 3;
const int bin4 = 10;

int motorSpeed;
int mcontrol;

uint8_t server_state; //0-4, left-stop

/* Put IP Address details */
IPAddress local_ip(192,168,1,1);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255,255,255,0);

ESP8266WebServer server(80);

// uint8_t LED1pin = D0; //GPIO13
bool LED1status = LOW;

// uint8_t LED2pin = D1; //GPIO12
bool LED2status = LOW;

bool update_motors() {
  switch(mcontrol) {
    case 0: {
      digitalWrite(fin1, LOW);
      digitalWrite(fin2, HIGH);
      digitalWrite(fin3, LOW);
      digitalWrite(fin4, HIGH);
      digitalWrite(bin1, LOW);
      digitalWrite(bin2, HIGH);
      digitalWrite(bin3, LOW);
      digitalWrite(bin4, HIGH);
      // Serial.println("State 0");
      //Left
      break;
    }
    case 1: {
      digitalWrite(fin1, HIGH);
      digitalWrite(fin2, LOW);
      digitalWrite(fin3, LOW);
      digitalWrite(fin4, HIGH);
      digitalWrite(bin1, HIGH);
      digitalWrite(bin2, LOW);
      digitalWrite(bin3, LOW);
      digitalWrite(bin4, HIGH);
      // Serial.println("State 1");
      //Forward
      break;
    }
    case 2: {
      digitalWrite(fin1, HIGH);
      digitalWrite(fin2, LOW);
      digitalWrite(fin3, HIGH);
      digitalWrite(fin4, LOW);
      digitalWrite(bin1, HIGH);
      digitalWrite(bin2, LOW);
      digitalWrite(bin3, HIGH);
      digitalWrite(bin4, LOW);
      // Serial.println("State 2");
      //Right
      break;
    }
    case 3: {
      digitalWrite(fin1, LOW);
      digitalWrite(fin2, HIGH);
      digitalWrite(fin3, HIGH);
      digitalWrite(fin4, LOW);
      digitalWrite(bin1, LOW);
      digitalWrite(bin2, HIGH);
      digitalWrite(bin3, HIGH);
      digitalWrite(bin4, LOW);
      // Serial.println("State 3");
      //Back
      break;
    }
    case 4: {
      digitalWrite(fin1, LOW);
      digitalWrite(fin2, LOW);
      digitalWrite(fin3, LOW);
      digitalWrite(fin4, LOW);
      digitalWrite(bin1, LOW);
      digitalWrite(bin2, LOW);
      digitalWrite(bin3, LOW);
      digitalWrite(bin4, LOW);
      // Serial.println("State 4");
      //Stop
      break;
    }
  }
  return true;
}

bool update_state(int state) {
  if(state < 0 || state > 3) return false;
  else mcontrol = state;
  update_motors();
  return true;
}

bool update_speed(int speed) {
  // int Lsp = (speed != 0) ? speed - 8 : 0;
  analogWrite(fenA, speed);
  analogWrite(fenB, speed);
  analogWrite(benA, speed);
  analogWrite(benB, speed);
  return true;
}

void setup() {
  Serial.begin(115200);
  // pinMode(LED1pin, OUTPUT);
  // pinMode(LED2pin, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(fenA, OUTPUT);
  pinMode(fin1, OUTPUT);
  pinMode(fin2, OUTPUT);
  pinMode(fenB, OUTPUT);
  pinMode(fin3, OUTPUT);
  pinMode(fin4, OUTPUT);
  pinMode(fenA, OUTPUT);
  pinMode(bin1, OUTPUT);
  pinMode(bin2, OUTPUT);
  pinMode(benB, OUTPUT);
  pinMode(bin3, OUTPUT);
  pinMode(bin4, OUTPUT);
  motorSpeed = 0;
  mcontrol = 4;

  WiFi.softAP(ssid, password);
  WiFi.softAPConfig(local_ip, gateway, subnet);
  delay(100);

  server.on("/", handle_OnConnect); //server_state = 4
  server.on("/left", handle_left); //server_state = 0
  server.on("/forward", handle_forward); //server_state = 1
  server.on("/right", handle_right); //server_state = 2
  server.on("/back", handle_back); //server_state = 3
  server.on("/stop", handle_stop); //server_state = 4
  server.onNotFound(handle_NotFound);

  server.begin();
  Serial.println("HTTP server started");
  update_speed(63);
}
void loop() {
  server.handleClient();
  // if(LED1status) {
  //   digitalWrite(LED1pin, HIGH);
  // }
  // else{
  //   digitalWrite(LED1pin, LOW);
  // }

  // if(LED2status) {
  //   digitalWrite(LED2pin, HIGH);
  // }
  // else {
  //   digitalWrite(LED2pin, LOW);
  // }
}

void handle_OnConnect() {
  server_state = 4;
  Serial.println("STOP");
  server.send(200, "text/html", SendHTML(server_state));
  update_state(server_state);
  Serial.println("Received STOP");
  //stop
}

void handle_stop() {
  server_state = 4;
  Serial.println("STOP");
  server.send(200, "text/html", SendHTML(server_state)); 
  update_state(4);
  Serial.println("Received STOP");
  update_speed(0);
  //left
}

void handle_left() {
  server_state = 0;
  Serial.println("LEFT");
  server.send(200, "text/html", SendHTML(server_state)); 
  update_state(0);
  Serial.println("Received LEFT");
  update_speed(100);
  //left
}

void handle_forward() {
  server_state = 1;
  Serial.println("FORWARD");
  server.send(200, "text/html", SendHTML(server_state)); 
  update_state(1);
  Serial.println("Received FORWARD");
  update_speed(77);
  //forward
}

void handle_right() {
  server_state = 2;
  Serial.println("RIGHT");
  server.send(200, "text/html", SendHTML(server_state)); 
  update_state(2);
  Serial.println("Received RIGHT");
  update_speed(100);
  //right
}

void handle_back() {
  server_state = 3;
  Serial.println("BACK");
  server.send(200, "text/html", SendHTML(server_state)); 
  update_state(3);
  Serial.println("Received BACK");
  update_speed(77);
  //back
}

void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}

String SendHTML(uint8_t server_state){
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr +="<title>LED Control</title>\n";
  ptr +="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr +="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;} h3 {color: #444444;margin-bottom: 50px;}\n";
  ptr +=".button {display: block;width: 80px;background-color: #1abc9c;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n";
  ptr +=".button-on {background-color: #1abc9c;}\n";
  ptr +=".button-on:active {background-color: #16a085;}\n";
  ptr +=".button-off {background-color: #34495e;}\n";
  ptr +=".button-off:active {background-color: #2c3e50;}\n";
  ptr +="p {font-size: 14px;color: #888;margin-bottom: 10px;}\n";
  ptr +="</style>\n";
  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<h1>ESP8266 Web Server</h1>\n";
  ptr +="<h3>Using Access Point(AP) Mode</h3>\n";
  switch(server_state) {
    case 0: {
      ptr +="<p>SERVER: 0 </p>\n";
      break;
    }
    case 1: {
      ptr +="<p>SERVER: 1 </p>\n";
      break;
    }
    case 2: {
      ptr +="<p>SERVER: 2 </p>\n";
      break;
    }
    case 3: {
      ptr +="<p>SERVER: 3 </p>\n";
      break;
    }
    case 4: {
      ptr +="<p>SERVER: 4 </p>\n";
      break;
    }
  }
  // ptr +="<p>SERVER: server_state </p>\n";
  ptr +="<a class=\"button button-on\" href=\"/left\">LEFT</a>\n";
  ptr +="<a class=\"button button-on\" href=\"/forward\">FORWARD</a>\n";
  ptr +="<a class=\"button button-on\" href=\"/right\">RIGHT</a>\n";
  ptr +="<a class=\"button button-on\" href=\"/back\">BACK</a>\n";
  ptr +="<a class=\"button button-on\" href=\"/stop\">STOP</a>\n";
  // if(led2stat)
  // {ptr +="<p>LED2 Status: ON</p><a class=\"button button-off\" href=\"/led2off\">OFF</a>\n";}
  // else
  // {ptr +="<p>LED2 Status: OFF</p><a class=\"button button-on\" href=\"/led2on\">ON</a>\n";}

  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
}