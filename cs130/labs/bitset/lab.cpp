//lab.cpp
//BITSET LAB -- FINAL
//Graham Buchanan
//03 Feb 2021

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

//class of Sets of Bits
class BITSET {
	
	//each index of the vector will store ONE Integer Value. 
	//This value is then manipulated through the bitwise operators in the class deifnes.
	//The Function ToBinary will then print the literal value of the vector[index] in Binary.
	std::vector <int> set;

	//public class declarations. boom.
	public:
	BITSET();
	~BITSET();
	std::string Test(unsigned int) const;
	void Set(unsigned int);
	void Clear(unsigned int);
	int GetNumSets() const;
	std::string GetSet(unsigned int a) const;
};

//integer ----> Binary string. Roasted.
std::string ToBinary(int val, int spa) {
	std::string c = "";
	int a = 1;
	
	//loop to fill the stirng (39 characters total)
	for(unsigned int j = 0; j < 39; ++j) { 
		if( a % (spa + 1) == 0)
			c += ' ';
		else {
			if(val % 2 == 0)
				c += '0';
			else
				c += '1';
			val = val / 2;
		}
		++a;
	}
	val = 0;
	std::string s = std::string(c.rbegin(), c.rend());
	return s;
}

//main function define...who wouldve guessed
int main(int argc, char ** argv) {
	BITSET z;
	//variables
		char in = '\0';
		unsigned int v = 0;

	//loop for the user interface...Have Fun
	while(in != 'q' || std::cin.eof() || !(!std::cin) ) {
		std::cout << '>';
		std::cin >> in;
		//checking for QUIT
		if(in == 'q' || std::cin.eof())
			exit(1);
		while(std::cin.fail()) {
			std::cin.clear();
			std::cin.ignore();
			std::cin >> in;
		}
		if( !(in == 'n') )
			std::cin >> v;

		//switch case for inputs
		switch(in) {
			case 't':
				{
					std::cout << z.Test(v) << std::endl;
					break;
				}
			case 'g':
				{
					std::cout << z.GetSet(v) << std::endl;
					break;
				}
			case 's':
				{
					z.Set(v);
					break;
				}
			case 'c':
				{
					z.Clear(v);
					break;
				}
			case 'n':
				{
					std::cout << z.GetNumSets() << std::endl;
					break;
				}
			default:
				break;
		}
	} 
	return 0;
}

// CLASS FUNCTION DEFINES
BITSET::BITSET() {
	set.resize(1);
}
BITSET::~BITSET() {}

std::string BITSET::Test(unsigned int a) const {
	//goes to INT location and return true (1) or false (0) based on the value of that bit
	int b = set[ (a / 32) ];
	b = ( (b >> (a)) & 1);
	if( (b == 0) || ((a / 32) > set.size() )) 
		return "0";
	else
		return "1";
}
void BITSET::Set(unsigned int a) {
	//goes to INT location and sets bit to 1
	if((a / 32) > (set.size() - 1))
		set.resize( ((a / 32) + 1), 0);
	int c = set[ (a / 32) ];
	c |= (1 << (a) );
	set[ (a / 32) ] = c;
}
void BITSET::Clear(unsigned int a) {
	//goes to INT locaiton and sets bit to 0
	int d = set[ (a / 32) ];
	d = d & ~(1 << a);
	set[ (a / 32) ] = d;
	for(unsigned int j = set.size(); j > 0; --j) {
		if(set[j] == 0)
			set.resize(j);
		else
			break;
	}
}
int BITSET::GetNumSets() const {
	//returns the number of sets in the vector as integer
	return set.size();
}
std::string BITSET::GetSet(unsigned int a) const {
	//returns the 4-byte integer of the particular set...return 0 if set does not exist
	if(a > (set.size() - 1) )
		return "0";
	else
		return ToBinary(set[a], 4);
}
