//TEAM.CPP
//Definitions for Team module
//Graham Buchanan


/*Changelog
 *
 * 19 July 2022 - Creating base code
 * 20 July 2022 - Finishing Constructors, made into struct
 * 23 July 2022 - Added post-game calculations
 *
 */


#include "team.hpp"

/* THESE CLASSES HAVE NOT BEEN DEFINED
//struct League;
//struct Game;
*/

//Set all values to the base
Team::Team() {
	name = "";
	location = "";
	tag = "";
	id = 0;

	poff = 0;
	roff = 0;
	olin = 0;

	pdef = 0;
	rdef = 0;
	dlin = 0;
	rate = 0;

	wins = 0;
	loss = 0;
	ties = 0;
	ptsc = 0;
	oppt = 0;

	opyd = 0;
	oryd = 0;
	dpyd = 0;
	dryd = 0;
}

//Set all values of the team class
Team::Team(uint8_t num, std::string s, uint64_t rtg) {
	
	//Set all strings
	location = s.substr(0,s.find(','));
	name = s.substr(1+s.find(','), s.rfind(',')-s.find(',')-1);
	tag = s.substr(1+s.rfind(','));
	id = num;

	//set all ratings
	//rtg = poff|roff|olin|pdef|rdef|dlin|0|0
	poff = (rtg >> 56) & 0xff;
	roff = (rtg >> 48) & 0xff;
	olin = (rtg >> 40) & 0xff;
	pdef = (rtg >> 32) & 0xff;
	rdef = (rtg >> 24) & 0xff;
	dlin = (rtg >> 16) & 0xff;
	rate = ((poff + roff + olin) / 3 + (pdef + rdef + dlin) / 3) / 2;
	
	//reset all stats
	wins = 0;
	loss = 0;
	ties = 0;
	ptsc = 0;
	oppt = 0;

}
