//MASTERGAME.CPP
//GAME MANAGEMENT
//Graham Buchanan

/***CHANGELOG***
 * 03 August 2022 - Added series of game simulation; 
 */

#include "master.hpp"

bool master::Play_A_Game() {

	size_t i;
	char play;
	int k;
	int s,p,r;
	int home_pts[6];
	int home_pyds[6];
	int home_ryds[6];
	int away_pts[6];
	int away_pyds[6];
	int away_ryds[6];

	Game *g;
	Print();
	printf("Please select the home team based on its Team # (far left column).\n> ");
	std::cin >> i;
	while(!(std::cin) || i >= TV.size()) {
		printf("ERROR. There is no team with the ID %zd. Please try another.\n> ", i);
		std::cin.clear();
		std::cin >> i;
	}
	g = new Game;
	g->home = TV[i];

	Print();
	printf("Please select the away team based on its Team # (far left column).\n> ");
	std::cin >> i;
	while(i >= TV.size() || i == g->home->id) {
		printf("ERROR. You have entered an incorrect number. You must select another team with an accurate Team ID. Please try another.\n> ");
		std::cin.clear();
		std::cin >> i;
	}
	g->away = TV[i];
	GV.push_back(g);

	printf("Would you like to play this game or have the CPU simulate a series of games? Please enter 'P' or 'S'.\n> ");
	std::cin >> play;
	while((play != 'P') && (play != 'S')) {
		printf("ERROR. You have entered an incorrect option. Please enter either 'P' for play or 'S' for simulate. Try again.\n> ");
		std::cin.clear();
		std::cin >> play;
	}
	std::cin.clear();
	if(play == 'P') {
		for(i = 0; i < 6; ++i) { home_pts[i] = 0; home_pyds[i] = 0; home_ryds[i] = 0; away_pts[i] = 0; away_pyds[i] = 0; away_ryds[i] = 0; }
		printf("\nWELCOME LADIES AND GENTLEMEN TO %s, today the %s (%d-%d-%d) are hosting the %s %s (%d-%d-%d)!\n",
				g->home->location.c_str(),g->home->name.c_str(),g->home->wins,g->home->loss,g->home->ties,g->away->location.c_str(),
				g->away->name.c_str(),g->away->wins,g->away->loss,g->away->ties);
		for(i = 0; i < 4; ++i) {
			printf("| Team | Q1 | Q2 | Q3 | Q4 | OT | Final |\n| Away | %2d | %2d | %2d | %2d | %2d | %5d |\n| Home | %2d | %2d | %2d | %2d | %2d | %5d |\n", 
					away_pts[0],away_pts[1],away_pts[2],away_pts[3],away_pts[4],away_pts[5],
					home_pts[0],home_pts[1],home_pts[2],home_pts[3],home_pts[4],home_pts[5]);
			printf("Please enter data for Quarter %zd in the format Points Passing-Yards Rushing-Yards.\n", i+1);
			printf("\tHome Team.> ");
			std::cin >> s >> p >> r;
			while((!std::cin) || (s < 0) || (s > 100) || (p < -100) || (p > 500) || (r < -100) || (r > 500)) {
				printf("ERROR! The points scored must be on a scale of 0-100, and each yardage total must be less than 500. Please try again.\n> ");
				std::cin.clear();
				std::cin >> s >> p >> r;
			}
			home_pts[i] = s;
			home_pyds[i] = p;
			home_ryds[i] = r;
			home_pts[5] += s;
			home_pyds[5] += p;
			home_ryds[5] += r;
			printf("\tAway Team.> ");
			std::cin >> s >> p >> r;
			while((!std::cin) || (s < 0) || (s > 100) || (p < -100) || (p > 500) || (r < -100) || (r > 500)) {
				printf("ERROR! The points scored must be on a scale of 0-100, and each yardage total must be less than 500. Please try again.\n> ");
				std::cin.clear();
				std::cin >> s >> p >> r;
			}
			away_pts[i] = s;
			away_pyds[i] = p;
			away_ryds[i] = r;
			away_pts[5] += s;
			away_pyds[5] += p;
			away_ryds[5] += r;
		}
		if(away_pts[5] == home_pts[5]) {
			printf("\nAt the end of regulation, the game is still tied! Please enter the total points and yards for the overtime period (only one).\n\tHome Team.> ");
			std::cin >> s >> p >> r;
			i = 4;
			while((!std::cin) || (s < 0) || (s > 100) || (p < -100) || (p > 500) || (r < -100) || (r > 500)) {
				printf("ERROR! The points scored must be on a scale of 0-100, and each yardage total must be less than 500. Please try again.\n> ");
				std::cin.clear();
				std::cin >> s >> p >> r;
			}
			home_pts[i] = s;
			home_pyds[i] = p;
			home_ryds[i] = r;
			home_pts[5] += s;
			home_pyds[5] += p;
			home_ryds[5] += r;
			printf("\n\tAway Team.> ");
			std::cin >> s >> p >> r;
			while((!std::cin) || (s < 0) || (s > 100) || (p < -100) || (p > 500) || (r < -100) || (r > 500)) {
				printf("ERROR! The points scored must be on a scale of 0-100, and each yardage total must be less than 500. Please try again.\n> ");
				std::cin.clear();
				std::cin >> s >> p >> r;
			}
			away_pts[i] = s;
			away_pyds[i] = p;
			away_ryds[i] = r;
			away_pts[5] += s;
			away_pyds[5] += p;
			away_ryds[5] += r;
			if(away_pts[5] == home_pts[5]) {
				printf("After a grueling overtime period, the score remains tied! The game is over!\n");
				g->winner = 'T';
			}
		} else if (away_pts[5] > home_pts[5]) {
			printf("After a grueling regulation period, the away team has won!\n");
			g->winner = 'A';
		} else {
			printf("After a grueling regulation period, the home team has won!\n");
			g->winner = 'H';
		}
		g->home_ptsc = home_pts[5];
		g->home_pyds = home_pyds[5];
		g->home_ryds = home_ryds[5];
		g->away_ptsc = away_pts[5];
		g->away_pyds = away_pyds[5];
		g->away_ryds = away_ryds[5];
		g->Game_Played();
		g->Print();
	}
	else {
		std::cin.clear();
		printf("Please input a number between 1 and 100 for the amount of games to simulate.\n> ");
		std::cin >> k;
		while(k == 0) {
			printf("ERROR. Please enter a number between 1 and 100.\n> ");
			std::cin.clear();
			std::cin >> k;
		}
		g->Sim_Multiple_Games(k);
	}
	Print();
	return true;
}
