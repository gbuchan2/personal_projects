// lab1.cpp
// Addressing the User and stealing their address, property value
// Graham Buchanan
// 25 August 2020

#include <iostream>
#include <string>

using namespace std;

int main()
{
	string name; //var user name 
	string street_name; //var user street name
	string street_type; //var user street type
	int street_number; //var user house#
	int last_val; //var last month property value
	int this_val; //var this month property value
	int val_chg; //var value change
	double this_mort; //var estimated mortgage
	
	cout << "What is your name? "; //txt prompts user for name
	cin >> name;
	cout << "Hi there, " << name << ". \n" ; //txt address user

	cout << "Enter the property street number, name, and type: "; //txt prompts user for address
	cin >> street_number >> street_name >> street_type;
	cout << "Last month's property value: $"; //txt prompts user for last property value 
	cin >> last_val;
	cout << "This month's property value: $"; //txt prompts user for current property value
	cin >> this_val;

	val_chg = this_val - last_val; //calc diff between last and current prop values
	this_mort = (this_val * 0.05)/12; //calc monthly mortgage based on current prop value
	cout << " \n"; //enter
	cout << "This property is located at " << street_number << " " << street_name << " " << street_type << ". \n" ; //txt user address
	cout << "Its value has changed by $" << val_chg << " since last month. \n" ; //txt diff in prop value
	cout << "The estimated monthly mortgage is $" << this_mort << ". \n" ; //txt est mortgage payment

	return 0;

}
