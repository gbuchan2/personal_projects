#include <cstdio>

int Multiply(int leftop, int rightop);
int Add(int leftop, int rightop);
int Sub(int leftop, int rightop);
int Twos(int op);
int Div(int leftop, int rightop);
int Mod(int leftop, int rightop);
int main(int argc, char *argv[])
{
   int left, right, result;
   char op;

   if (4 != argc)
   {
       printf("Usage: %s <left> <op> <right>\n", argv[0]);
       return -1;
   }
   sscanf(argv[1], "%d", &left);
   sscanf(argv[2], "%c", &op);
   sscanf(argv[3], "%d", &right);

    switch (op)
   {
   case 'm':
   case 'x':
   case '*':
       result = Multiply(left, right);
       break;
   case 'a':
   case '+':
       result = Add(left, right);
       break;
   case 's':
   case '-':
       result = Sub(left, right);
       break;
   case 'd':
   case '/':
       result = Div(left, right);
        break;
    case '%':
        result = Mod(left, right);
        break;
    default:
        result = -11111111;
        break;
   }
    printf("%d\n", result);
    return 0;
}
//Write your functions here