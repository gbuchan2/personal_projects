//DIVISION.HPP
//Declarations of the Division Class
//Graham Buchanan


/***UPDATE LOG***
 *  17 December 2021 - Creation of File
 *  18 December 2021 - Constructor & Destructor
 *
 */

#include <vector>
#include <map>
#include <list>
#include "team.hpp"

class Division {

	public:
		Division(const std::vector<Team *> &v, const std::string &n);
		~Division();

		void EditDivision(); //Edits the makeup of the current division
		void CreateSub(); //Creates a subdivision (maximum of two for now)
		void BalanceSubs(); //Balances both subdivisions
		int NumSubs();
		int NumTms();

		void PrintData() const;
		std::string GetName() const;

	protected:
		std::map<uint32_t, Team *> tmMap; //Must use League's Find tools to locate teams
		std::list<Division *> subs; //Map of all subdivisions, max of two for now
		std::string name;
};
