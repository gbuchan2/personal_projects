// lab2.cpp
// Determines whether given highway is primary/auxillary; if auxillary, indicate what highway it services.
// Graham Buchanan
// 07 September 2020

#include <iostream>
#include <string>


using namespace std;

int main()
{
	int userRoad; //var user input road number
	string dirRoadTxt; //string direction of road number given

	userRoad = 1;		//var userRoad = 1 so loop can begin

	while(userRoad != 0) {		//while loop...kinda does what it says...

		cout << "Enter an interstate number: ";		//txt prompts user for highway ##
		cin >> userRoad;

		if(userRoad == 0)		//if userRoad = 0... 
			break;		//kill the program

		if( (userRoad % 100) == 0 || (userRoad <0) || (userRoad > 999) ) {		//cond if userRoad is divisible by 100 or less than 0...
			cout << userRoad << " is not a valid interstate number." << endl;		//txt msg to let user know their number is invalid
		}
		else {		//cond it's another ELSE STATEMENT....OHMYGOSH (i.e. 1<=userRoad 


			if( (userRoad % 2) == 0) {		//cond if userRoad is divisible by 2...
				dirRoadTxt = ", going east/west.";		//string dirRoadTxt = east/west
			}
			else {		//cond else! (i.e. not divisible by 2)...
				dirRoadTxt = ", going north/south.";		//string dirRoadTxt = north/south
			}


			if(userRoad < 100) {		//cond if userRoad is less than 100...
				cout << "I-" << userRoad << " is a primary interstate" << dirRoadTxt << endl;		//txt final statement
			}
			else {		//cond else (i.e. greater than 100)...
				cout << "I-" << userRoad << " is an auxillary highway, serving I-" << userRoad % 100 << dirRoadTxt << endl;		//txt final statement
			}
		}
	}

	return 0;

}
