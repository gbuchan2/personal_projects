//CELL.HPP
//Declares the Cell Class
//Graham Buchanan

/***DESCRIPTION***
 * The CELL is the most basic unit of the game.
 * The MAP is composed of W*H CELLs
 * A CELL provides resources, population, and military to a CIV
 */

/***CHANGELOG***
 * 04 Oct 2024 - Creation
 * 07 Oct 2024 - Begin defining interactions
 *
 */

#include <vector>
#include <map>
#include <set>
#include <list>
#include <deque>
#include <stdint.h>
#include <cstdio>
#include <iostream>
#include <fstream>

class Noble; //Governor of CELL
class Civ; //Owner* of CELL

class Cell {

	private:

		//static details
		uint16_t cell; //identification number [0,65535]
		char good[2]; //goods found in cell (F, W, C, S, M)
		uint8_t locx, locy; //coordinates of cell
		
		//Civ details
		Civ* rule; //Which CIV rules this cell?
		Noble* govn; //Who governs this cell?
		uint8_t type; //development level of cell [0-4]

		//statistics
		uint8_t prod; // [0,10] amt of each good[n] produced
		uint8_t stor; // [0,100] amt of total goods that can be stored 
		uint8_t food; // [0,100] amt of food stored
		uint8_t pop; // [0,250] population of the cell (1=1000 people)
		int8_t grow; // [-25, 25] population growth for this turn
		uint8_t happ; // [0,100] average pop happiness
		uint8_t infr; // [0,100] infrastructure level for the cell
		uint8_t garr; // [0, 250] total garrison in teh cell

	public:

		//generic members
		void Print();
		Cell();

		//edit stats
		bool Init(uint16_t &a, char &b[2], uint8_t c, uint8_t d);
		bool New_Ruler(Civ* a, Noble* b);
		bool Update_Stats();
		bool Clear_Stats();

		//cell interactions
};
