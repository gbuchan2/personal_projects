//ver1.cpp -- reading the files
//bitmap...reading, writing bitmap files
//Graham Buchanan
//22 February 2021

#include <cstdio>
#include <cstdint>
#pragma pack(push, 2)

struct BitmapFileHeader {
	uint16_t type; //type of file, Windows bitmap BM
	uint32_t size; //entire size of file, including headers
	uint16_t reserveda; //reserved for future expansion
	uint16_t reservedb; //reserved for future expansion
	uint32_t offset; //tells the offest from top of file to where pixel data is stored
};

struct BitmapInfoHeader {
	uint32_t size; //size of info header [should be 40]
	uint32_t width; //width of the picture
	uint32_t height; //height of the picture
	uint16_t planes; //number of layers, test pics = 1
	uint16_t bitcount; //number of bits per pixel, should be 24
	uint32_t compression; //compression used, there is none, should be zero
	uint32_t imagesize; //size of image, may be zero
	uint32_t x_pixels_per_meter; //number of pixels per meter horizontally
	uint32_t y_pixels_per_meter; //number of pixels per meter vertically
	uint32_t color_used; //color index used, should be 0
	uint32_t color_important; //should be 0
};

#pragma pack(pop)

struct Pixel {
	uint8_t red; //amount of red
	uint8_t blue; //amount of blue
	uint8_t green; //amount of green
};

//invert = 255 - pixel color value

int main(int argc, char **argv) {

	//command checking
	if(argc != 3) {
		printf("User input error.\n");
		return -1;
	}

	//opening structs
	BitmapFileHeader Header;
	BitmapInfoHeader Info;

	//opening the read file
	FILE *fl;
	fl = fopen(argv[1], "rb");
	if(nullptr == fl) {
		perror("fl");
		return -1;
	}

	fseek(fl, 0, SEEK_SET);
	//testing fread   TEST!!!!
	fread(&Header, 1, sizeof(BitmapFileHeader), fl);
	printf("type: %x, size: %d, reserved: %u, reserved: %u, offset: %u\n", Header.type, Header.size, Header.reserveda, Header.reservedb, Header.offset);

	//info header read TEST!!!!
	fread(&Info, 1, sizeof(BitmapInfoHeader), fl);
	printf("size: %u, width: %u, height: %u, planes: %u, bitcount: %u, compression: %u, imagesize: %u, x_pixels_per_meter: %u, y_pixels_per_meter: %u, color_used: %d, color_important: %d\n", 
			Info.size, Info.width, Info.height, Info.planes, Info.bitcount, Info.compression, Info.imagesize, Info.x_pixels_per_meter, Info.y_pixels_per_meter, Info.color_used, Info.color_important);
	
	uint8_t pad = 4 -((Info.width * sizeof(Pixel)) % 4);
	printf("padding: %d\n", pad);
	Pixel *pix = new Pixel[Info.width * Info.height];
	
	//pix data read TEST!!!!
	fseek(fl, Header.offset, SEEK_SET);
	uint32_t pix_num = 0;
	for(uint16_t i = 0; i < Info.height; ++i) {
		for(uint16_t j = 0; j < (Info.width * sizeof(Pixel) - pad); ++j) {
			fread(&pix[j], 1, sizeof(Pixel), fl);			
			++pix_num;
		}
		fseek(fl, pad, SEEK_CUR);
	}
	printf("Pixels saved: %d\n", pix_num);
	fclose(fl);

	fl = fopen(argv[2], "wb");
	if(nullptr == fl) {
		perror("fl");
		return -1;
	}

	fseek(fl, 0, SEEK_SET);
	fwrite(&Header, 1, sizeof(BitmapFileHeader), fl);
	printf("Header is printed\n");

	fwrite(&Info, 1, sizeof(BitmapInfoHeader), fl);
	printf("Info is printed.\n");

	fseek(fl, Header.offset, SEEK_SET);
	pix_num = 0;
	uint8_t padd = 0;
	for(uint16_t i = 0; i < Info.height; ++i) {
		for(uint16_t j = 0 ; j < (Info.width * sizeof(Pixel) - pad); ++j) {
			fwrite(&pix[j], 1, sizeof(Pixel), fl);
			fwrite(&padd, 1, pad, fl);
			++pix_num;
		}
	}
	printf("Pixel data has been printed.\n");
	fclose(fl);
	delete[] pix;
	return 0;
}

