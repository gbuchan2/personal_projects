//Keno.cpp
//This program simulates the Keno! Lottery game
//Graham Buchanan
//06 October 2021

#include <iostream>
#include <cstdio>
#include <vector>
#include "fraction.hpp"
#include <cmath>

int main() {

	Fraction f; 
	double bet = 0; //Amount to bet
	int tot = 80; //Number of balls in lot
	int house = 20; //Number of balls picked by house
	int num = 0; //Number of balls to choose
	unsigned int c = 0; //Number of catches
	double p = 0; //Payout
	double prob = 0; //Probabilities
	std::vector<int> corr; //catches vector
	std::vector<double> pay; //payout vector
	
	//Here, we read all inputs
	//Writeup says there is no need to error check, so there is no error checking
	std::cin >> bet >> num;
	while(std::cin >> c >> p) {
		corr.push_back(c);
		pay.push_back(p);
	}
	//Printing
	//Reset a double to record total returns
	//Clear out the fraction and then multiply/divide said fraction 
	//by the appropriate binomials
	p = 0;
	printf("Bet: %.2f\nBalls Picked: %d\n", bet, num);
	for(c = 0; c < corr.size(); ++c) {
		f.Clear();
		f.Multiply_Binom(tot - num, house - corr[c]);
		f.Multiply_Binom(num, corr[c]);
		f.Divide_Binom(tot, house);
		prob = f.Calculate_Product();
		std::cout << "  Probability of catching " << corr[c] << " of " << num << ": " << prob << " -- Expected return: " << (prob * pay[c]) << std::endl;
		p += (prob * pay[c]);
	}
	//Because there are two doubles, a flat equation may return a 
	//nonzero number when the answer is 0
	//A little math (and tolerance) is required to figure this out
	//If the difference between the two doubles is less than 1/100000, 
	//set everything to 1 (setting to 0 will result in Div by zero)
	if(std::abs(p - bet) <= 0.00001) {
		p = 1;
		bet = 1;
	}
	printf("Your return per bet: %.2f\nNormalized: %.2f\n", (p - bet), ((p - bet)/ bet));
	return 0;
}
