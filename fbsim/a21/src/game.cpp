//GAME.HPP
//All game data
//Graham Buchanan
//11 March 2022





#include "team.hpp"

class Game {
	public:
		Game(); //Base Constructor
		Game(Team *h, Team *a, std::string &l, uint32_t &id); //Constructor with params

		bool Set_Teams(Team *h, Team *a, std::string, &l);
		bool Calc_Score();

		void Print();
	protected:
		Team *home;
		Team *away;
		std::string location;
		uint32_t id; //Game ID (from Schedule class)

		uint16_t score; //MSB-Away, LSB-Home
}
