#include <string>
#include <vector>
#include <list>
#include <cmath>
#include <algorithm>
#include <map>
#include <set>
#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

class MiddleCode {
	public:
		string encode(string s);
};

string MiddleCode::encode(string s)
{
	string t = "";
	char c = '\0';
	size_t p = 0;
	size_t i = 0;


	if((s.length() % 2) == 0) {
		for(i = 0; i < s.length()/2; ++i) {
			t += s[(s.length()-2*i)/2 -1];
			t += s[(s.length())/2 + i];
		}
	} else {
		for(i = 0; i < s.length()/2+1; ++i) {
			if((i == 0)) {
				t += s[(s.length()-i)/2 ];
			} else {
				c = s[(s.length()-2*i)/2 - 1];
				if(c < s[(s.length())/2 + i]) {
					t += c;		
					t += s[(s.length()-i)/2 + i];
				} else {
					t += s[(s.length())/2 + i];
					t += c;
				}

			}
		}
	}
	return t;
}


