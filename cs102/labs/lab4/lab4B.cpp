//lab4.cpp
//Complicated Authoring Assistant
//Graham Buchanan
//20 October 2020

#include <iostream>
#include <sstream>
#include <string>

using namespace std;

//var declare function to count words
int NumWords(const string& input_str) 
{
	//var declare necessary variables
	int wordCount = 0;
	string strTwo;
	stringstream ss(input_str);

	//loop input string into stream
	while(ss >> strTwo) 
	{
		wordCount++;
	}

	//return variable wordCount
	return wordCount;
}

//var declare function to count nonWS characters
int NumNonWSCharacters(const string& input_str) 
{
	//var declare necessary variables
	int nonWS_char = 0;

	//loop for the length of the string 
	for(unsigned int i = 0; i < input_str.size(); ++i) 
	{	
		//cond if there isn't a space
		if( !(isspace(input_str[i])))
		{
			
			nonWS_char++;
		}
	}

	return nonWS_char;
}

//var declare function to replace characters in a string
void CharReplace(string& input_str, char old_char, char new_char) 
{
	//loop for the length of the string
	for(unsigned int i = 0; i < input_str.size(); ++i)
	{
		if(input_str[i] == old_char)
			input_str[i] = new_char;
	}
}

//var declare function to print menu
char PrintMenu() {

	//var necessary variable
	char userSelect;

	//txt print Menu
	cout << "Options" << endl;
	cout << "w - Number of words" << endl;
	cout << "c - Number of non-whitespace characters" << endl;
	cout << "r - Replace a character" << endl;
	cout << "q - Quit" << endl << endl;

	//txt prompt user for input
	cout << "Choose an option: ";
	cin >> userSelect;		

	return userSelect;
}

int main() {

	//var declare necessary input variables
	string user_str;
	char userSelect;

	//prompt user for inputs
	cout << "Enter a line of text: ";
	getline(cin, user_str);
	
	//txt user input
	cout << endl << "You entered: " << user_str << endl << endl;

	//loop as long as no "Quit"
	while((userSelect = PrintMenu()) != 'q') {

		//cond switch case (menu)
		switch(userSelect) {
			
			//cond 'w' leads to Menu option...count words
			case 'w':

				//var declare variable
				int wordsInText;

				//run function
				wordsInText = NumWords(user_str);
				
				//txt print word count
				cout << endl << "Number of words: " << wordsInText  << endl << endl;

				break;
			
			//cond 'c' leads to Menu option...count non-WS
			case 'c':

				//var declare variable
				int nonWS_char;
				
				//run function
				nonWS_char = NumNonWSCharacters(user_str);
				
				//txt print non-WS count
				cout << endl << "Number of non-whitespace characters: " << nonWS_char << endl << endl;

				break;
			
			//cond 'r' leads to Menu option...replace character
			case 'r':

				//var declare variable
				char old_char, new_char;
				
				//prompt user for input
				cout << "Enter a character to find: ";
				cin >> old_char;		//var char to replace
				cout << "Enter a character to replace: ";
				cin >> new_char;		//var char to replace with
				
				//run function
				CharReplace(user_str, old_char, new_char);
				
				//txt print edited user_str
				cout << endl << "New string: " << user_str << endl << endl;

				break;
			
			//cond DEFAULT...empty line
			default:
				cout << endl;
		}
	}

	return 0;
}
