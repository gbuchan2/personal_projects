//Vflip.cpp
//Take PGM file on cin & output a vertical flip
//Graham Buchanan
//03 Sept 2021

#include <iostream>
#include <iomanip>
#include <vector>

int main() {

	std::string s; //P2
	int r = 0; //rows
	int c = 0; //cols
	int d = 0; //pix val
	int i = 0; //it
	int j = 0; //it
	std::vector <int> v;
	std::vector <int> a;

	//Need to check string, cols, and rows independently for fails
	//If there is not P2, cols > 0, rows > 0, then 255 in that order, quit
	if(!(std::cin >> s) || (s != "P2")) {
		std::cerr << "Bad PGM file -- first word is not P2" << std::endl;
		return 1;
	} else if(!(std::cin >> c) || (c < 1)) {
		std::cerr << "Bad PGM file -- No column specification" << std::endl;
		return 1;
	} else if(!(std::cin >> r) || (r < 1)) {
		std::cerr << "Bad PGM file -- No row specification" << std::endl;
		return 1;
	} else if(!(std::cin >> d) || (d != 255)) {
		std::cerr << "Bad PGM file -- No 255 following the rows and columns" << std::endl;
		return 1;
	} else {
		for(i = 0; i < (r * c); ++i) {
				//Read in all pixel values as integers. If value is <0 or >255, fail
				if(!(std::cin >> d) || (d < 0) || (d > 255)) {
					std::cerr << "Bad PGM file -- pixel " << (i) << " is not a number between 0 and 255" << std::endl;
					return 1;
				} else
					v.push_back(d);
					
			std::cin.clear();
		}
		//After we have read *all* pixels, if there are any others, quit out
		if(std::cin >> d) {
			std::cerr << "Bad PGM file -- Extra stuff after the pixels" << std::endl;
			return 1;
		} else {
			//Probably the part you are most interested in: the calculation
			//I just used one vector cause it was easier than a 2d vector (or using new)
			//Essentially, we already have one vector full so we can utilize it and the row count to our advantage
			//I will start at the end of the column (-1 because indexes) and count backwards to fill up our vector "cols"
			//And that's all there is...
			for(i = r - 1; i > -1; --i) {
				for(j = 0; j < c; ++j) {
					a.push_back(v[i * c + j]); 
				}
			}
			//Printing!
			std::cout << s << std::endl << c << ' ' << r << std::endl << 255 << std::endl ;
			for(i = 0; i < (r * c); ++i) {
				std::cout << a[i] << std::endl;
			}

		}
	}
	return 0;	
}
