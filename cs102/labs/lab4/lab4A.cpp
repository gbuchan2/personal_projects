//lab4.cpp
//Simple Authoring Assistant
//Graham Buchanan
//06 October 2020

#include <iostream>
#include <sstream>
#include <string>

using namespace std;

int main()
{
	string text;		//var used to capture user text
	stringstream text_stream;		//var stringstream to manipulate text
	
	cout << "Enter a line of text: ";		//txt prompt user for input
	getline(cin, text);		//grab the entire user input
	
	const int size = text.length();		//var size of text line
	char abc;		//var rando variable
	text_stream << text;		//var convert to string
	
	cout << "Original: " << text << endl;		//txt print orig
	cout << "Characters: " << size << endl;		//txt print character amt
	cout << "Without whitespace: ";		//txt print without white space
	while (text_stream >> abc) {		//loop while stream is printing to variable
		cout << abc;		//txt print variable
	}
	cout << '\n';		//endline

	return 0;
}

