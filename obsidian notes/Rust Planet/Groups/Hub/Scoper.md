"Scopers"
general scientists: astronomers, chemists, biologists
selects mating pairs, healthcare*

The Scopers are a reclusive group, all of whom live on Sector 8. Not much is known about the innerworkings of the organization, but all decisions are unilaterally made on the "logical" premise. The Scopers original purpose was to find a new home for humanity, hence the "tele-scoper" name. They have a small fleet of Scoper-[[Engr]] probes which regularly sample the planets, moons, and several other objects in the Solar System in this effort. So far, no habitable environment has been reported to the public. In addition to their outward focus, the Scopers also provide testing and tracing of the human genome, and -- as concerns grow for interbreeding -- they use these metrics to formulate the best available pairs among eligible persons. In effect, the Scopers are those responsible for the evolutionary trends of humankind. They have bred out disease and violent tendencies, while encouraging intelligence and strength. 

### Guild
Name: Scopers
Emblem: Atom
Members: 29
Leader: [[Pollo]]
[[Humanity#The Grand Treaty|Treaty]] Seats: 1

### Occupations:
- Astronomer
- Chemist
- Biologist

### Stations:
- Sector [[8]]

### Notables: