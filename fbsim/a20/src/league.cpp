//LEAGUE.CPP
//League Class defines
//Graham Buchanan


/***Update Log***
 * 02 November 2021 - Creation
 * 02 November 2021 - (WIP) Step One - Team data
 * 09 November 2021 - Added Read and Edit teams, Find teams, and prints by name and location
 * 17 November 2021 - MakeFile functionality ... Able to read in teams from standard input
 * 22 December 2021 - SaveState operational! Divisions Operational (max of 2 subdivs)
 * 26 December 2021 - Saving and Reading files is functional. Need to figure out saving conference for each team!!!
 */
#include "league.hpp"
#include <fstream> 

/* This program implements a league class to keep a catalogue of teams.*/


/*
   class League {
   public:

   bool Save_State(std::string &f);

   bool Read_Teams(); //Enter Team Data from load file
   bool Input_Teams(); //Enter Team Data
   bool Edit_Teams(); //Edit Team Data

   Team *Find(const std::string &name); //Find a team based on name
   Team *Find(const std::string &location); //Find a team based on location

   void Print_By_Overall() const; //Print teams by overall rating
   void Print_By_Offense() const; //Print teams by offensive rating
   void Print_By_Defense() const; //Print teams by defensive rating
   void Print_By_Name() const; //Print teams by name
   void Print_By_Location() const; //Print teams by location
   void Print_By_Wins() const; //Print teams by wins

   void Print_By_Division() const;

   protected:
   std::vector <Team *> TV; //Team Vector
   std::multimap <std::string, Team *> NM; //Name Team Map
   std::multimap <std::string, Team *> LM; //Location Team Map

   std::map <std::string, Division *> DM; //Divisions Map
   }*/

League::League() {
}

League::~League() {

	size_t i = 0;
	std::map<std::string, Division *>::iterator dit;
	for(i = 0; i < TV.size(); ++i) {
		delete TV[i];
	}

	for(dit = DM.begin(); dit != DM.end(); ++dit) {
		delete dit->second;
	}
}

//Save all current data
bool League::Save_State(std::string f) {

	FILE *file;
	short int code = 0;
	time_t now = time(0);
	char *date = ctime(& now); //use this to print current date
	std::map<std::string, Division *>::const_iterator mit;
	size_t i =0;


	if(TV.empty()) {
		std::cerr << "There is nothing to save!\n";
		return false;
	}

	file = fopen(f.c_str(), "w");
	if(file == NULL) {
		std::cerr << "ERROR: cannot open file. Please try again.\n";
		return false;
	}

	fprintf(file, "FBSIM\nDate Saved:%s\n%.4zx\n%.4zx\n", date, DM.size(), TV.size());
	for(mit = DM.begin(); mit != DM.end(); ++mit) {
		code = 0;
		fprintf(file, "%.4hx%-30s%.4hx%.4hx\n", code, mit->first.c_str(), mit->second->NumSubs() & 0xff, mit->second->NumTms() & 0xff);
	}
	for(i = 0; i < TV.size(); ++i) {
		code = 1;
		fprintf(file, "%.4hx%.8zx%.4hx%.8x%.4hx%.4hx%-30s%-30s%-30s\n", code, i, TV[i]->getRating(), TV[i]->getRecord(), TV[i]->getPtsScored(), TV[i]->getOppScored(), TV[i]->getLocation().c_str(), TV[i]->getName().c_str(), TV[i]->getDivision()->GetName().c_str());
	}
	fclose(file);
	return true;

}

//Enter Team Data from file
bool League::Read_Teams(std::string f) {	

	FILE *file;
	std::string header = "";
	char *c = new char[30];
	char *c1 = new char[30];
	char *c2 = new char[30];
	unsigned int notms = 0;
	unsigned int nodivs = 0;
	size_t i = 0;
	size_t j = 0;

	uint16_t code = 0;

	std::string divname = "";
	uint16_t NumSubs = 0;
	uint16_t NumTms = 0;

	uint32_t tmID = 0;
	uint16_t tmRating = 0;
	uint32_t tmRecord = 0;
	uint16_t tmPts = 0;
	uint16_t tmOpp = 0;
	std::string tmLoc = "";
	std::string tmName = "";
	std::string tmDiv = "";

	
	std::vector<Team *> v;
	std::map<std::string, std::vector<Team *> > map;
	std::map<std::string, std::vector<Team *> >::const_iterator mit;
	
	Team *t;
	Division *d;
	bool subs = false;

	file = fopen(f.c_str(), "r");
	if(file == NULL) {
		std::cerr << "ERROR: cannot open file. Please try again.\n";
		return false;
	}

	//Making room for all this new data
	std::cout << "Deleting old data.\n";
	std::map<std::string, Division *>::iterator dit;
	for(i = 0; i < TV.size(); ++i) {
		delete TV[i];
	}

	for(dit = DM.begin(); dit != DM.end(); ++dit) {
		delete dit->second;
	}

	TV.clear();
	NM.clear();
	LM.clear();
	DM.clear();

	std::cout << "Deletion successful.\n";

	//Checking file format
	if((fgets(c, 256, file) == NULL) || (*(c + 0) != 'F') || (*(c + 1) != 'B') || (*(c + 2) != 'S') || (*(c + 3) != 'I') || (*(c + 4) != 'M')) {
		std::cerr << "ERROR: This file is not of the correct format (Missing format)! Please try again.\n";
		return false;
	}
	fgets(c, 256, file);
	fscanf(file, "%x\n%x\n", &nodivs, &notms);
	printf("Number of Divisions: %u, Number of Teams: %u.\n", nodivs, notms);
	for(i = 0; i < 30; ++i) *(c + i) = '\0';


	//Reading in division data
	for(i = 0; i < nodivs; ++i) {
		printf("Reading in Division %zx\n", i);
		fscanf(file, "%4hx%30s%4hx%4hx\n", &code, c, &NumSubs, &NumTms);
		if(code != 0) {
			std::cerr << "ERROR: file is not of correct format (Wrong code - Division)! Entered code " << code << std::endl;
			return false;
		}
		for(j = 0; j < 30; ++j) {
			divname += *(c + j);
		}
		divname.erase(divname.find_last_not_of(" \t\f\v\n\r"));
		if(NumSubs > 0) subs = true;
		map.insert(std::pair<std::string, std::vector<Team *> >(divname, v));

		divname = "";
		NumSubs = 0;
		NumTms = 0;
		code = 0;
	}

	//Read all teams in
	for(j = 0; j < notms; ++j) {
		printf("Reading in Team %zx\n", j);
		for(i = 0; i < 30; ++i) { *(c + i) = '\0'; *(c1 + i) = '\0'; *(c2 + i) = '\0'; }
		fscanf(file, "%4hx%8x%4hx%8x%4hx%4hx%30s%30s%30s\n", &code, &tmID, &tmRating, &tmRecord, &tmPts, &tmOpp, c, c1, c2);
		if(code != 1) {
			std::cerr << "ERROR: file is not of correct format (Wrong code - Team)! Entered code: " << code << std::endl;
			return false;
		}
		//Read all integer values



		std::cout << "Location Name Division:\n";
		//sort through each string
		for(i = 0; i < 30; ++i) {
			tmLoc += *(c + i);
		}
		std::cout << tmLoc << ' ';
		tmLoc.erase(tmLoc.find_last_not_of(" \t\f\v\n\r"));
		for(i = 0; i < 30; ++i) {
			tmName += *(c1 + i);
		}
		std::cout << tmName << ' ';
		tmName.erase(tmName.find_last_not_of(" \t\f\v\n\r"));
		for(i = 0; i < 30; ++i) {
			tmDiv += *(c2 + i);
		}
		std::cout << tmDiv << std::endl;
		tmDiv.erase(tmDiv.find_last_not_of(" \t\f\v\n\r"));

		//declare new team and store with appropriate vector
		t = new Team(tmID, tmLoc, tmName, tmRating);
		map.find(tmDiv)->second.push_back(t);
		TV.push_back(t);
		NM.insert(std::pair<std::string, Team *>(tmName, t));
		LM.insert(std::pair<std::string, Team *>(tmLoc, t));

		//clear all necessary values
		tmID = 0;
		tmRating = 0;
		tmRecord = 0;
		tmPts = 0;
		tmOpp = 0;
		tmLoc = "";
		tmName = "";
		tmDiv = "";
		code = 0;
	}

	for(mit = map.begin(); mit != map.end(); ++mit) {
		d = new Division(mit->second, mit->first);
		DM.insert(std::pair<std::string, Division *>(mit->first, d));
	}

	fclose(file);
	std::cout << "Read successful.\n";
	return true;
}

//Enter Team Data manually
bool League::Input_Teams() {

	Team *t;
	uint32_t notms = 0; //No. teams to enter
	std::string n = "";
	std::string l = "";
	int o = 0;
	int d = 0;
	uint16_t r = 0;
	int nodiv = 0;
	uint32_t i = 0;
	size_t string_i = 0;
	int k = 0;
	uint32_t tmid = 0;
	std::vector<Team *> v;
	Division *div;
	std::stringstream ss;
	std::string divname = "";





	//Ask user for division count
	std::cout << "How many divisions are there? Specify '1' for a one-division league. > ";
	std::cin >> nodiv;
	while(!(std::cin) || (nodiv < 1)) {
		std::cin.clear();
		std::cin.ignore();
		std::cerr << "ERROR: There must be at least ONE division, and there may not be more divisions than the number of teams. Please try again. > ";
		std::cin >> nodiv;
	}

	//Enter teams for each division
	for(k = 0; k < nodiv; ++k) {

		std::cout << "Please enter the name of this division. > ";
		std::cin.ignore(256, '\n');
		std::getline(std::cin, divname);
		while(!(std::cin) || (divname == "")) {
			std::cin.clear();
			std::cerr << " ERROR: Please enter a string. >";
			std::getline(std::cin, divname);
		}
		string_i = divname.find_last_not_of(" \t\f\v\n\r");
		if(string_i != std::string::npos) divname.erase(string_i + 1);


		std::cout << "\n Enter data for " << divname << std::endl;

		std::cout << "  How many teams are in this division? > ";
		std::cin >> notms;

		//Error Checking no. teams
		while((!std::cin) || (notms < 1)) {
			std::cin.clear();
			std::cin.ignore();
			printf(" There must be at least one team in each division. Please try again. > ");
			std::cin >> notms;
		}

		//Entering the teams individually
		for(i = 0; i < notms; ++i) {

			//Input Location
			string_i = 0;
			std::cin.ignore(256, '\n');
			printf("\n   Team %d:\n   Please input the Location. > ", i + 1);
			std::getline(std::cin, l);
			while((!std::cin) || (l == "") || (l.find_first_of("0123456789") != std::string::npos)) {
				std::cin.clear();
				std::cerr << "   ERROR: Make sure the location is a string of letters. Try again. > ";
				std::getline(std::cin, l);
			}
			string_i = l.find_last_not_of(" \t\f\v\n\r");
			if(string_i != std::string::npos) l.erase(string_i + 1);
			string_i = 0;

			//Input Name
			printf("   Please input the Name. > ");
			std::getline(std::cin, n);
			while((!std::cin) || (n == "") || (n.find_first_of("0123456789") != std::string::npos)) {
				std::cin.clear();
				std::cerr << "   ERROR: Make sure the name is a string of letters. Try again. > ";
				std::getline(std::cin, n);
			}
			string_i = n.find_last_not_of(" \t\f\v\n\r");
			if(string_i != std::string::npos) n.erase(string_i + 1);

			//Input Ratings
			std::cout << "   Please input the offensive and defensive ratings on a scale of 70-99. > ";
			std::cin >> o >> d;
			while( (!std::cin) || (o < 70) || (d < 70) || (o > 99) || (d > 99)) {
				std::cin.clear();
				std::cerr << "   ERROR: Make sure the offensive and defensive ratings are separated by only spaces. Try again. > ";
				std::cin >> o >> d;
			}

			r = ((d << 8) | o);
			t = new Team(tmid, l, n, r); //create Team
			TV.push_back(t); //Store team pointer
			v.push_back(t);
			++tmid;
		}

		//store Teams in maps...print data for division
		for(size_t j = 0; j < v.size(); ++j) {
			t = v[j];
			NM.insert(std::pair<std::string, Team *>((t->getName()), t));
			LM.insert(std::pair<std::string, Team *>((t->getLocation()), t));
		}
		div = new Division(v, divname);
		DM[divname] = div;
		divname = "";
		v.clear();
	}
	Print_By_Name();

	Save_State((std::string)"files/autosave.txt");
	std::cout << "Data saved!\n";
	return true;
}

//Edit Team Data
bool League::Edit_Team(Team *t) {

	std::string s = "";
	std::string b = "";
	uint32_t r = 0;
	uint16_t a = 0;

	printf("The team selected:\n");
	t->PrintData();
	std::cout << "Select Option to edit: Location, Name, Offense, Defense, or None? > ";
	std::cin >> s;
	while( (!std::cin) || ((s != "Location") && (s != "Name") && (s != "Offense") && (s != "Defense") && (s != "None"))) {
		std::cin.clear();
		std::cerr << "ERROR: Please select of the five options. Try again. > ";
		std::cin >> s;
	}
	if(s == "None") return false;

	if(s == "Location") {
		std::cout << "What is the new location? > ";
		std::cin >> s;
		while( (!std::cin) || (s == "") || (s.find_first_of("0123456789") != std::string::npos)) {
			std::cin.clear();
			std::cerr << "ERROR: Please input a string or type 'None'. Try again. > ";
			std::cin >> s;
		}
		if(s == "None") return false;

		b = t->getName();
		if(!(t->EditName(s, b))) return false;
		return true;
	} else if(s == "Name") {
		std::cout << "What is the new name? > ";
		std::cin >> s;
		while( (!std::cin) || (s == "") || (s.find_first_of("0123456789") != std::string::npos)) {
			std::cin.clear();
			std::cerr << "ERROR: Please input a string or type 'None'. Try again. > ";
			std::cin >> s;
		}
		if(s == "None") return false;

		b = t->getLocation();
		if(!(t->EditName(b, s))) return false;
		return true;

	} else if(s == "Offense") {
		std::cout << "What is the new Offensive Rating? > ";
		std::cin >> r;
		while( (!std::cin) || ((r < 70)) || (r > 99)) {
			std::cin.clear();
			std::cerr << "ERROR: Please input an integer value from 70 to 99 or type '0'. Try again. > ";
			std::cin >> r;		
		}
		if(r == 0) return false;
		//clear out current offense by & '0', then OR with new value
		a = ((t->getRating() & 0xff00) | (r));
		if(!(t->EditRating(a))) return false;
		return true;

	} else if(s == "Defense") {
		std::cout << "What is the new Defensive Rating? > ";
		std::cin >> r;
		while( (!std::cin) || ((r < 70)) || (r > 99)) {
			std::cin.clear();
			std::cerr << "ERROR: Please input an integer value from 70 to 99 or type '0'. Try again. > ";
			std::cin >> r;		
		}
		if(r == 0) return false;
		//clear out current defense by & '0', then OR with new value
		a = ((t->getRating() & 0x00ff) | (r << 8));
		if(!(t->EditRating(a))) return false;
		return true;

	}
	return false;
}


//Find a team based on name
Team* League::Find_Name(const std::string &name) {

	std::map<std::string, Team *>::const_iterator mit;

	mit = NM.find(name);

	if(mit == NM.end()) return NULL;
	return mit->second;
}

//Find a team based on location
Team* League::Find_Location(const std::string &location) {
	std::map<std::string, Team *>::const_iterator mit;

	mit = LM.find(location);
	if(mit == LM.end()) return NULL;
	return mit->second;
}

Team* League::Find_Index(const uint32_t &id) {
	return TV[id];
}

//Print teams by Rating
void League::Print_By_Index() const {
	size_t i = 0;

	printf("Team ID - Team Location and Name - Team Ratings - Team Record - Team Stats\n");
	for(i = 0; i < TV.size(); ++i) {
		TV[i]->PrintData();
	}
}

//Print teams by overall
void League::Print_By_Overall() const {
}

//Print teams by Offense
void League::Print_By_Offense() const {
}

//Print teams by defense
void League::Print_By_Defense() const {
}

//Print teams by name
void League::Print_By_Name() const {

	std::multimap<std::string, Team *>::const_iterator mit;

	printf("Team ID - Team Location and Name - Team Ratings - Team Record - Team Stats\n");
	for(mit = NM.begin(); mit != NM.end(); ++mit) {
		mit->second->PrintData();
	}
}

//Print teams by Location
void League::Print_By_Location() const {

	std::map<std::string, Team *>::const_iterator mit;

	printf("Team ID - Team Location and Name - Team Ratings - Team Record - Team Stats\n");
	for(mit = LM.begin(); mit != LM.end(); ++mit) {
		mit->second->PrintData();
	}
}

//Print teams by wins
void League::Print_By_Wins() const {
}

//Print teams by divisions
void League::Print_By_Division() const {

	std::map<std::string, Division *>::const_iterator mit;

	printf("Team ID - Team Location and Name - Team Ratings - Team Record - Team Stats\n");
	for(mit = DM.begin(); mit != DM.end(); ++mit) {
		mit->second->PrintData();
	}
}
