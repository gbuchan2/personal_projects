//MASTER.HPP
//Controls all background interface operations
//Graham Buchanan
//17 March 2022

#include "schedule.hpp"
#include "division.hpp"

class Master {

	public:
		
		//Enter Data
		bool Save_Data(std::string &file);
		bool Load_Data(std::string &file);
		bool Enter_Teams();
		bool Delete_Teams();
		
		//Divisions
		bool Create_Division();
		bool Delete_Division();

		//Schedule
		bool Create_Schedule();
		bool Generate_Schedule();
		bool Generate_RR();
		bool Delete_Schedule();



	protected:
		std::vector<Team *> TV; //Team Vector
		std::map<std::string, Team *> TM; //Team Map, keyed by string (Team Abbreviation)
		std::set<std::string> TS; //Team Set, a list of all team names
		std::map<std::string, Division *> DM; //Division Map, keyed by string (Division Name)
		std::vector<Division *> DV; //Division Vector		
		Schedule *sch; //full schedule
}
