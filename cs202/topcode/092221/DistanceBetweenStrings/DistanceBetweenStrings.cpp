#include <string>
#include <vector>
#include <list>
#include <cmath>
#include <algorithm>
#include <map>
#include <set>
#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

class DistanceBetweenStrings {
	public:
		int getDistance(string a, string b, string letterSet);
};

int DistanceBetweenStrings::getDistance(string a, string b, string l)
{
	size_t i = 0;
	size_t j = 0;
	int sum = 0;
	int ao = 0;
	int bo = 0;

	for(j = 0; j < l.length(); ++j) {
		for(i = 0; i < a.length(); ++i) {
			if(a[i] < 'a') a[i] = (a[i] - 'A' + 'a');
			if(l[j] == a[i]) ao++;
		}
		for(i = 0; i < b.length(); ++i) {
			if(b[i] < 'a') b[i] = (b[i] - 'A' + 'a');
			if(l[j] == a[i]) bo++;
		}
		sum += (ao - bo)*(ao - bo);
		ao = 0;
		bo = 0;
	}
	return sum;
}
