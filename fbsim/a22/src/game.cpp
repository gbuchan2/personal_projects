//GAME.CPP
//Definitions of the Game module
//Graham Buchanan

/***CHANGELOG***
*
* 21 JULY 2022 - Creation and basic implementation
* 22 July 2022 - Print function
*
*
*/

#include "game.hpp"
#include <random>

bool Game::Game_Played() {	

	home->opyd += home_pyds;
	home->oryd += home_ryds;
	home->ptsc += home_ptsc;
	home->dpyd += away_pyds;
	home->dryd += away_ryds;
	home->oppt += away_ptsc;

	away->opyd += away_pyds;
	away->oryd += away_ryds;
	away->ptsc += away_ptsc;
	away->dpyd += home_pyds;
	away->dryd += home_ryds;
	away->oppt += home_ptsc;

	if(winner == 'T') { home->ties += 1; away->ties += 1; }
	else if(winner == 'H') { home->wins += 1; away->loss += 1; }
	else { home->loss += 1; away->wins += 1; }

	return true;
}

bool Game::Reset_Data() {
	home_ptsc = 0;
	home_pyds = 0;
	home_ryds = 0;

	away_ptsc = 0;
	away_pyds = 0;
	away_ryds = 0;
	winner = '\0';
	return true;
}

bool Game::Sim_Multiple_Games(int n) {

	int total_hpts = 0;
	int total_hpyd = 0;
	int total_hryd = 0;
	int total_apts = 0;
	int total_apyd = 0; 
	int total_aryd = 0;
	double home_wins = 0;
	double away_wins = 0;
	double ties = 0;
	
	Reset_Data();
	printf("Game of %d series between %s %s and %s %s\n", n, away->location.c_str(), away->name.c_str(), home->location.c_str(), home->name.c_str());
	for(int i = 0; i < n; ++i) {
		Calc_Game();
		printf("Game #%d: %s %d, %s %d: PY %d RY %d, PR %d RY %d\n", i+1,away->name.c_str(),away_ptsc,home->name.c_str(),home_ptsc,away_pyds,away_ryds,home_pyds,home_ryds);
		
		total_hpts += home_ptsc;
		total_hpyd += home_pyds;
		total_hryd += home_ryds;
		total_apts += away_ptsc;
		total_apyd += away_pyds;
		total_aryd += away_ryds;

		if(winner == 'H') home_wins += 1;
		else if(winner == 'A') away_wins += 1;
		else ties += 1;
	}

	printf("\nFinal tally:\n");
	printf("Home Team: %s %s %d PF, %d PY, %d RY\n\tThey averaged %5.2f PF, %6.2f PY, and %6.2f RY per game!\n", home->location.c_str(), home->name.c_str(),
			total_hpts,total_hpyd,total_hryd,(double)((double)total_hpts / n),(double)((double)total_hpyd / n),(double)((double)total_hryd / n));
	printf("Away Team: %s %s %d PF, %d PY, %d RY\n\tThey averaged %5.2f PF, %6.2f PY, and %6.2f RY per game!\n", away->location.c_str(), away->name.c_str(),
			total_apts,total_apyd,total_aryd,(double)((double)total_apts / n),(double)((double)total_apyd / n),(double)((double)total_aryd / n));
	return true;
}

bool Game::Play_Game() {
	return true;
}

bool Game::Calc_Game() {

	int home_poff;
	int home_roff;
	int home_olin;
	int home_pdef;
	int home_rdef;
	int home_dlin;

	int away_poff;
	int away_roff;
	int away_olin;
	int away_pdef;
	int away_rdef;
	int away_dlin;

	double home_pp;
	double home_rp;
	double home_tp;

	double away_pp;
	double away_rp;
	double away_tp;

	double home_pluc, home_rluc, home_luck;
	double away_pluc, away_rluc, away_luck;

	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<double> prod_luck(1.0, 0.25); //d(mean,stddev)

	//Grab Ratings
	home_poff = home->poff;
	home_roff = home->roff;
	home_olin = home->olin;
	home_pdef = home->pdef;
	home_rdef = home->rdef;
	home_dlin = home->dlin;

	away_poff = away->poff;
	away_roff = away->roff;
	away_olin = away->olin;
	away_pdef = away->pdef;
	away_rdef = away->rdef;
	away_dlin = away->dlin;

	//Generate luck stat
	do { home_pluc = prod_luck(gen); } while(home_pluc < 0.25 || home_pluc > 2.0);
	do { home_rluc = prod_luck(gen); } while(home_rluc < 0.25 || home_rluc > 2.0);
	do { home_luck = prod_luck(gen) + 0.05; } while(home_luck < 0.5 || home_luck > 1.5);
	do { away_pluc = prod_luck(gen); } while(away_pluc < 0.25 || away_pluc > 2.0);
	do { away_rluc = prod_luck(gen); } while(away_rluc < 0.25 || away_rluc > 2.0);
	do { away_luck = prod_luck(gen); } while(away_luck < 0.5 || away_luck > 1.5);

	//Calculate Production
	home_pp = (25*cbrt(home_poff - away_pdef)+20*cbrt(home_olin - away_dlin)+283.482*home_pluc);
	home_rp = (25*cbrt(home_roff - away_rdef)+20*cbrt(home_olin - away_dlin)+245.7*home_pluc);
	home_tp = home_pp + home_rp;
	away_pp = 25*cbrt(away_poff - home_pdef)+20*cbrt(away_olin - home_dlin)+283.482*away_pluc;
	away_rp = 25*cbrt(away_roff - home_rdef)+20*cbrt(away_olin - home_dlin)+245.7*away_rluc;
	away_tp = away_pp + away_rp;

	printf("Home Team's total production = %.2f, PP = %.2f and RP = %.2f\n", home_tp,home_pp,home_rp);
	printf("Away Team's total production = %.2f, PP = %.2f and RP = %.2f\n", away_tp,away_pp,away_rp);

	//Calculate Yardage
	home_pyds = (-65.8+0.931*home_tp-0.000329*pow(home_tp,2))*(home_pp/home_tp+0.15);
	home_ryds = (-65.8+0.931*home_tp-0.000329*pow(home_tp,2))*(home_rp/home_tp-0.15);
	away_pyds = (-65.8+0.931*away_tp-0.000329*pow(away_tp,2))*(away_pp/away_tp+0.15);
	away_ryds = (-65.8+0.931*away_tp-0.000329*pow(away_tp,2))*(away_rp/away_tp-0.15);

	printf("The %s %s threw for %d yards and ran for %d yards.\n", home->location.c_str(), home->name.c_str(), home_pyds, home_ryds);
	printf("The %s %s threw for %d yards and ran for %d yards.\n", away->location.c_str(), away->name.c_str(), away_pyds, away_ryds);

	//Calculate Points
	home_tp *= home_luck;
	home_ptsc = -1.12+0.0496*home_tp+0.00000832*pow(home_tp, 2)-0.0000000123*pow(home_tp, 3);
	away_tp *= away_luck;
	away_ptsc = -1.12+0.0496*away_tp+0.00000832*pow(away_tp, 2)-0.0000000123*pow(away_tp, 3);

	//Decide winner
	if(home_ptsc < 0) home_ptsc = 0;
	if(away_ptsc < 0) away_ptsc = 0;

	if(home_ptsc == away_ptsc) winner = 'T';
	else if(home_ptsc < away_ptsc) winner = 'A';
	else if(home_ptsc > away_ptsc) winner = 'H';

	Game_Played();

	return true;
}

void Game::Print() {
	if(winner == 'H') {
		printf("Congratulations! The Home Team %s %s won against %s %s with a score of %d-%d.\n",
				home->location.c_str(), home->name.c_str(), away->location.c_str(), away->name.c_str(), away_ptsc, home_ptsc);
	} else if(winner == 'A') {
		printf("Congratulations! The Away Team %s %s won against %s %s with a score of %d-%d.\n", 
				away->location.c_str(), away->name.c_str(), home->location.c_str(), home->name.c_str(), away_ptsc, home_ptsc);
	} else if(winner == 'T') {
		printf("This game ended in a tie between %s %s and %s %s with a score of %d-%d.\n", 
				away->location.c_str(), away->name.c_str(), home->location.c_str(), home->name.c_str(), away_ptsc, home_ptsc);
	} else printf("This game has not been played yet.\n");
}
