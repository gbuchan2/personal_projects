//MAIN.CPP
//Generate Conference Schedule opponents
//Graham Buchanan
//9 Nov 2022

#include <vector>
#include <map>
#include <set>
#include <list>
#include <deque>
#include <stdint.h>
#include <cstdio>
#include <iostream>
#include <fstream>



struct Team {

	string n;
	char[16] schedule;
	char id;

}

struct Conference {

	std::vector<Team *> tv;

}

Team* newTeam(char &a, Conference *c, std::string &s) {

	Team *t = new Team;
	t->id = a;
	t->n = s;
	for(int i = 0; i < 16; ++i) t->schedule[i] = 0;
	c->tv.push_back(t);

}

void clearABC(char[16] &a) {
	for(int i = 0; i < 16; ++i) a[i] = 0;
}

int main() {

	Conference *c = new Conference;

	std::map<std::string, Team *> map;
	std::map<std::string, Team *>::iterator mit;
	Team *tm;
	char[16] abc;

	std::vector<std::string> sv = {
		"Tennessee Volunteers",
		"Alabama Crimson Tide",
		"LSU Tigers",
		"Auburn Tigers",
		"Mississippi State Bulldogs",
		"Mississippi Bulldogs",
		"Texas A&M Aggies",
		"Texas Longhorns",
		"Oklahoma Sooners",
		"Vanderbilt Commodores",
		"Missouri Tigers",
		"South Carolina Gamecocks",
		"Kentucky Wildcats",
		"Florida Gators",
		"Georgia Bulldogs",
		"Arkansas Razorbacks"
	};

	for(char i = 0; i < 16; ++i) {
		tm = newTeam(i, c, sv[i]);
		map.insert(sv[i], tm);
	}

	clearABC(abc);

	



	return 0;
}
