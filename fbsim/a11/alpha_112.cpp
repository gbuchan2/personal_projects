//ALPHA 1.12 
//Preparing "Loading a Schedule"
//College Football League Simulator

#include <iostream>
#include <vector>
#include <string>
#include <cstdio>
#include <set>
#include <map>

struct Game {
	std::string Away;
	std::string Home;
	int away_score;
	int home_score;
};

class Team {
  public:
		std::string Name;
    int off;
    int def;
		int wins = 0;
		int losses = 0;
		float ppg = 0;
		float papg = 0;
    void Print() const;
		void Avg_SC(int pts_for, int pts_ag);
};

class TMS {
	public:
		bool Read();
		bool Edit_Sch();
		void Print_Sch();
		Team *Find(const std::string &name);
		
		void Print_By_Offense() const;
		void Print_By_Defense() const;
		void Print_By_Name() const;
		void Print_By_Wins() const;
		
		void Play_Game(const std::string &Away, const std::string &Home);
		void Play_Schedule();
	protected:
		std::vector <Team *> TV;
		std::map <std::string, Team *> TM;
		std::vector <Game> schedule;
};

/* THE MAIN FUNCTION/.....
 *
 * STOP LOSING IT!!!!! */

int main() {
	TMS tms;
	printf("Welcome to my Football Simulator version alpha_0105.\nThe current simulator will only progress through an 'as-run' schedule, meaning you will specify the teams, ratings, and then the number of games.\nBefore each game, you will be asked who is playing.\nPlease keep in mind there are no tie games in College Football!\nPlease follow all instructions as they come.\n");
	tms.Read();
	tms.Edit_Sch();
	tms.Print_Sch();
  return 0;
}


/* TEAM class function:::::
 *
 */
void Team::Print() const {
  printf("%s (%d-%d); %d offense, %d defense; average score (%.2f-%.2f).\n", Name.c_str(), wins, losses, off, def, ppg, papg);
}

void Team::Avg_SC(int pts_for, int pts_ag) {
	float curr_avg = ppg;
	int num = wins + losses;
	curr_avg *= num;
	curr_avg += pts_for;
	curr_avg /= (num + 1);
	ppg = curr_avg;
	curr_avg = papg;
	curr_avg *= num;
	curr_avg += pts_ag;
	curr_avg /= (num + 1);
	papg = curr_avg;
}

/* TMS class functions::::::
 *
 */
bool TMS::Read() { //read in team data
	Team *t;
	int notms = 0;
	printf("\nHow many teams are there? > ");
	std::cin >> notms;
	if(notms < 2) {
		printf("There are not enough teams to do anything.\n");
		exit(1);
	}
	for(int i = 0; i < notms; ++i) {
		t = new Team;
		printf("\nTeam %d:\nPlease input the Name (string), Offensive Rating (int), and Defensive Rating (int).\n", i + 1);
		std::cin >> t->Name >> t->off >> t->def;
		TV.push_back(t);
	}
	printf("\n");
	for(int i = 0; i < notms; ++i) TV[i]->Print();
	for(unsigned long i = 0; i < TV.size(); ++i) {
		t = TV[i];
		TM[t->Name] = t;
	}
	return true;
}

bool TMS::Edit_Sch() {
	//CURRENTLY ONLY LOADS THE SCHEDULE AT PROGRAM START
	//WILL ADD FUNCTIONALITY TO EDIT DURING RUN
	int nogms = 0;
	std::string home_team;
	std::string away_team;
	Game game;
	printf("\nHow many games are we playing? > ");
	std::cin >> nogms;
	for(int i = 0; i < nogms; ++i) {
		printf("Game %d:\n", i + 1);
		printf("Who is Away? > ");
		std::cin >> away_team;
		printf("Who is Home? > ");
		std::cin >> home_team;
		game.Away = away_team;
		game.Home = home_team;
		schedule.push_back(game);
	}
	return true;
}

Team *TMS::Find(const std::string &name) { //Find correct team object based on name
	std::map<std::string, Team *>::iterator tit;
	tit = TM.find(name);
	if(tit == TM.end()) return NULL;
	return tit->second;
}

/* TMS print functions::::
 */

void TMS::Print_Sch() {
	printf("This is our full season schedule!\n");
	for(size_t i = 0; i < schedule.size(); ++i) {
		printf("     Game %zd: %s at %s\n", i + 1, schedule[i].Away.c_str(), schedule[i].Home.c_str());
	}
}

void TMS::Print_By_Offense() const { //Prints team data by Offensive Rating
	Team *t;
	size_t i;
	std::multimap <double, Team *> m;
	std::multimap <double, Team *>::const_iterator mit;
	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->off, t));
	}
	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Defense() const { //Prints team data by Defensive Rating
	Team *t;
	size_t i;
	std::multimap <double, Team *> m;
	std::multimap <double, Team *>::const_iterator mit;
	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->def, t));
	}

	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Name() const { //Prints team data by Name
	std::map <std::string, Team *> m;
	std::map <std::string, Team *>::const_iterator mit;
	size_t i;
	Team *t;
	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m[t->Name] = t;
	}
	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Wins() const { //Prints team data by Wins
	std::multimap <int, Team *> m;
	std::multimap <int, Team *>::const_iterator mit;
	size_t i;
	Team *t;
	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->wins, t));
	}
	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

/* TMS class functions to play games
 */

void TMS::Play_Game(const std::string &Away, const std::string &Home) { //play a game dialogue 
	int home_score;
	int away_score;
	Team *a;
	Team *h;
	a = Find(Away);
	h = Find(Home);
	if(a == NULL)
		printf("The Away Team cannot be found.\n");
	else if(h == NULL) 
		printf("The Home Team cannot be found.\n");
	else {
		printf("%s vs. %s\n", Away.c_str(), Home.c_str());
		a->Print();
		h->Print();
		printf("What is the final score (Format: Away-Home)? > ");
		std::cin >> away_score;
		std::cin.ignore(50, '-');
		std::cin >> home_score;
		if (away_score > home_score) {
			printf("Final Score: %s %d, %s %d. \nCongratulations %s!\n\n", Away.c_str(), away_score, Home.c_str(), home_score, Away.c_str());
			a->Avg_SC(away_score, home_score);
			h->Avg_SC(home_score, away_score);
			a->wins++;
			h->losses++;
		}
		else if (home_score > away_score) {	
			printf("Final Score: %s %d, %s %d. \nCongratulations %s!\n\n", Home.c_str(), home_score, Away.c_str(), away_score, Home.c_str());
			a->Avg_SC(away_score, home_score);
			h->Avg_SC(home_score, away_score);
			h->wins++;
			a->losses++;
		}
		else {
			printf("There can be no ties! The Game has no conclusion!\n");
		}
	}
}

void TMS::Play_Schedule() { //run through the schedule
	for(size_t i = 0; i < schedule.size(); ++i) {
		printf("GAME %zd", i + 1);
		Play_Game(schedule[i].Away, schedule[i].Home);
	}
}

