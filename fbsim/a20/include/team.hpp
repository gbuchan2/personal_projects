//TEAM.HPP
//Team Class Declarations
//Graham Buchanan

/***Update Log***
 * 13 October 2021 - Creation
 * 26 October 2021 - Finished iteration 1 of Team Class
 */

#include <cstdio>
#include <iostream>
#include <stdint.h>

class Division;

class Team {

	public:
		Team(const uint32_t &id, const std::string &l, const std::string &n, const uint16_t &r); //Class Constructor
		bool EditName(const std::string &l, const std::string &n); //0 - edit Name and Location, 1 - Rating
		bool EditRating(const uint16_t &r); //0 - edit Name and Location, 1 - Rating
		bool UpdateStats(bool &flag, const unsigned int &pf, const unsigned int &po); //0 - new game, 1- edited game
		bool SetDivision(const std::string &n);
		void PrintData() const; //Prints all Data of the Team Class
		std::string getName() const;
		std::string getLocation() const;
		uint32_t getID() const;
		uint16_t getRating() const;
		uint32_t getRecord() const;
		uint16_t getPtsScored() const;
		uint16_t getOppScored() const;
		Division *getDivision() const;

	protected:
		uint32_t tmID;
		std::string Name; //Stores the name of the Team (i.e. "Tigers")
		std::string Location; //Stores the Location of the Team (i.e. "Auburn")
		uint16_t Rating; //0-7 Offense, 8-15 Defense
		double PPG; //ppg
		double PAPG;//opponent ppg
		uint32_t record; //0-7 ties, 8-15 losses, 16-23 wins
		Division *div;
		Division *subdivision;
};
