// lab1A.cpp
// Addressing the User
// Graham Buchanan
// 25 August 2020

#include <iostream>
#include <string>

using namespace std;

int main()		// main executable
{
	string name;		//var user name
	
	cout << "What is your name? ";		//txt prompts user for name
	cin >> name;		//var stores user name
	cout << "Hi there, " << name << "." << endl;		//txt addresses user

	return 0;
}
