//ENUM.CPP
//Enumerates strings -- all possible combinations
//Graham Buchanan
//03 Nov 2021


/***This program takes a given string length and specified number of '1's, 
 *  and expresses every possible combination of the number of '1's within the length of string specified*/

#include <iostream>
#include <sstream>

void enumerate(std::string &s, int index, int n_ones);

int main(int argc, char **argv) {

	std::stringstream ss;
	std::string s = "";
	int i = 0;
	int j = 0;

	//Error Checking to guarantee length and number of ones has been specified
	try {
		if(argc != 3) (throw(std::string) "usage enum length nones");
		ss.clear();
		ss.str(argv[1]);
		if(!(ss >> j)) (throw(std::string) "usage enum length nones");
	} catch(std::string &s) {
		std::cerr << s << std::endl;
		return -1;
	}

	//generate a string with the given length
	for(i = 0; i < j; ++i) {
		s.append("-");
	}

	//pull out the specified number of ones
	ss.clear();
	ss.str(argv[2]);
	ss >> j;

	//enumerate
	enumerate(s, 0, j);

	return 0;
}

void enumerate(std::string &s, int index, int n_ones) {

	//returns when index = s.length()
	size_t i = index;

	//save the string to not overwrite the given string
	std::string a = s;
	if(((unsigned int)index < a.length()) && (n_ones < ((int)s.length() - index + 1))) {
		//at each blank space, call enumerate with '0'	
		if(a[i] == '-') {
			a[i] = '0';
			enumerate(a, i + 1, n_ones);
			a[i] = '-';
		}
		//as long as there are available ones, call enumerate with a '1'
		if((n_ones > 0)) {
			a[i] = '1';
			enumerate(a, i + 1, n_ones - 1);
			a[i] = '-';
		}
		//Print solution
	} else {
		if(n_ones == 0)
			std::cout << a << std::endl;
	}
}

