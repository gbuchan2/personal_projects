//ver1.cpp -- building UI
//Machine Code -- Below Assembly
//Graham Buchanan
//12 March 2021

#include <iostream>
#include <cstdint>
#include <fstream>
#include <cstdio>

//MIPS simulation structure
struct Machine {

	uint32_t regs[32]; //register values array
	uint32_t pc; //program counter
	uint32_t num_instructions; //no. instructions
	uint32_t *instructions; //instructions ptr array

	Machine() {
		pc = 0;
		num_instructions = 0;
		instructions[0] = 0;
	};

	bool reg(const std::string &name, uint32_t &val); //function to locate register
};

const std::string reg_names[] = { //register string array names
	"$zero",
	"$at",
	"$v0",
	"$v1",
	"$a0",
	"$a1",
	"$a2",
	"$a3",
	"$t0",
	"$t1",
	"$t2",
	"$t3",
	"$t4",
	"$t5",
	"$t6",
	"$t7",
	"$s0",
	"$s1",
	"$s2",
	"$s3",
	"$s4",
	"$s5",
	"$s6",
	"$s7",
	"$t8",
	"$t9",
	"$k0",
	"$k1",
	"$gp",
	"$sp",
	"$fp",
	"$ra"
};

void DecodeInstr (Machine &m, uint32_t instr); //operational function proto

bool LoadCheck(Machine m); //checks to see if machine has been loaded

int main(int32_t argv, char **argc) {

	std::string user_comm; //user input command ("regs", "reg", "load")
	std::string user_mod; //user secondary commands ("$t0", "file.bin")
	uint32_t size = 0; //var file size
	FILE *fl; //file ptr
	Machine mach; //construct the Machine

	for(uint8_t i = 0; i < 32; ++i) { //for all 32 regs
		mach.regs[i] = 0; //reset values		
	}
	mach.pc = 0;
	mach.num_instructions = 0;
	
	//OPERTATIONS LOOP!!!
	while(user_comm != "quit") {
		std::cout << "> "; //prompt
		std::cin >> user_comm; //user input first comm
		if(user_comm == "quit") //checks first input
			return 0;


		
		//regs -- dumps all registers into hex and dec
		if(user_comm == "regs") {
			
			if(LoadCheck(mach) == false) { //checks for load
				mach.regs[0] = 0; //guarantees $zero is 0

				for(uint8_t j = 0; j < 32; ++j) { //for all 32 regs
					printf("%-5s: 0x%08x (%5d) ", reg_names[j].c_str(), mach.regs[j], mach.regs[j]);
					if(( (j + 1) % 4) == 0) //if end of row
						printf("\n"); //endline
				}
				printf("PC   : %u\n", mach.pc); //print Program Counter
			}
		}

		//reg -- dumps given reg into hex and dec
		else if(user_comm == "reg") {

			//user_mod MUST be in form "$xx"
			std::cin >> user_mod; //read in second user command

			if(LoadCheck(mach) == false) { //checks for load
				uint32_t value = 0; //var for val
				mach.regs[0] = 0; //guarantees $zero is 0
				if(mach.reg(user_mod, value) ==  true) //checks if reg can is found
					printf("%-5s: 0x%08x (%5d)\n", user_mod.c_str(), value, value);
				else
					printf("User Input Error. Given register must be in the form '$xx'.\n");
			}
		}
		
		//next -- move through next instruction
		else if(user_comm == "next") {
			if(LoadCheck(mach) == false) { //checks for load
				if(mach.pc < size) { //checks if finished program
					DecodeInstr(mach, mach.instructions[mach.pc / 4]); //runs instr
					mach.pc += 4; //steps counter
				}
				else
					printf("No more instructions to run.\n");
			}
		}
		
		//run -- move through program
		else if(user_comm == "run") { 
			if(LoadCheck(mach) == false) { //checks for load
				while( !(mach.pc >= size) ) { //runs through program
					DecodeInstr(mach, mach.instructions[mach.pc / 4]); //runs instr
					mach.pc += 4; //steps counter
				}
			}
		}
		
		//load -- load in the machine
		else if(user_comm == "load") {
			std::cin >> user_mod; //read in second command
			mach.pc = 0; //reset counter
			mach.instructions[0] = 0;
			mach.num_instructions = 0;
			for(uint8_t i = 0; i < 32; ++i) { //for all 32 regs
				mach.regs[i] = 0; //reset values		
			}

			fl = fopen(user_mod.c_str(), "rb"); //open the file			
			if(NULL == fl) { //if file is bad
				perror("fl");
				return -1;
			}

			fseek(fl, 0, SEEK_END); //find end of file
			size = ftell(fl); //return file length in bytes
			fseek(fl, 0, SEEK_SET); //find beg of file
			mach.num_instructions = (size / 4); //calculate no. instr
			mach.instructions = new uint32_t[mach.num_instructions]; //dyn mem array
			fread(mach.instructions, 1, size, fl); //read in instr
			fclose(fl); //close file
		}
		else
			std::cout << "User input command unreadable.\n";
	}
	delete[] mach.instructions;
	return 0;
}

//returns value of given reg if reg is found
bool Machine::reg(const std::string &name, uint32_t &val) {
	
	//for all 32 regs
	for (uint8_t i = 0; i < 32; i++) {
		//if reg matches given reg
		if (name == reg_names[i]) { 
			val = regs[i]; //set val to reg val
			return true; 
		}
	}
	return false;
}

//run the instr
void DecodeInstr (Machine &m, uint32_t instr) {
	uint8_t op_mask = 0b111111; //opcode mask
	uint8_t op_code = (instr >> 26) & op_mask; //find opcode
	uint8_t sub_op = instr & op_mask; //find subopcode
	int16_t offset = instr & 0xffff; //find last 16 bits (offsets/immediates)
	int8_t sa = (instr >> 6) & 31; //find shift amount
	uint8_t rs = (instr >> 21) & 31; //find reg
	uint8_t rt = (instr >> 16) & 31; //find reg
	uint8_t rd = (instr >> 11) & 31; //find reg dest

	//check for opcode
	switch(op_code) {
		case 0: { 
			
			//check for sub
			switch(sub_op) {
				case 0: {
					//SLL
					m.regs[rt] = m.regs[rd] << sa; //rt = rd << sa
					break;
				}

				case 2: {
					//SRL
					//32 - offset = number of bits that will not be shoved off. So, perform SRA, AND those bits with the necessary number: 0x(insert no. f's)
					m.regs[rd] = (m.regs[rt] >> sa); //rd = rt >> sa
					break;
				}

				case 3: {
					//SRA
					uint8_t sign = (m.regs[rt] >> 31);
					uint32_t temp = 0;
				
					//make mask
					for(uint8_t f = 0; f < 32; ++f) { 
						temp = (temp << 1) + sign;
					}
					temp |= (m.regs[rt] >> sa); //create temp
					m.regs[rd] = temp; //rd = temp
					break;
				}

				case 32: {
					//ADD
					m.regs[rd] = m.regs[rs] + m.regs[rt]; //rd = rs + rt
					break;
				}

				case 36: {
					 //AND
					 m.regs[rd] = m.regs[rs] & m.regs[rt]; //rd = rs & rt
					 break;
				}

				case 37: {
					 //OR
					 m.regs[rd] = m.regs[rs] | m.regs[rt]; //rd = rs | rt
					 break;
				}

				case 42: {
					//SLT
					if(m.regs[rs] < m.regs[rt]) //if rs < rt
						m.regs[rd] = 1; //set rd to 1
					else
						m.regs[rd] = 0; //set rd to 0
					break;
				}

				default:
					std::cout << "Unknown Instruction...continuing.\n";
					break;
			}
			break;
		}

		case 4: { 
			//BEQ
			if(m.regs[rs] == m.regs[rt]) { //if rs = rt
				m.pc += (offset * 4); //jump
			}
			break;
		}
		
		case 5: {
			//BNE
			if( !(m.regs[rs] == m.regs[rt]) ) { //if rs != rt
				m.pc += (offset * 4); //jump
			}
			break;
		}
		
		case 8: {
			//ADDI
			m.regs[rt] = m.regs[rs] + offset; //rt rs + offset
			break;
		}

		default:
			std::cout << "Unknown Instruction...continuing.\n"; 
			break;
	}
}

//function to see if mach has loaded
bool LoadCheck(Machine m) {
	if(m.instructions[0] == 0) { //if first instr = 0
		std::cout << "No machine loaded.\n";
		return true;
	}
	else
		return false;
}
