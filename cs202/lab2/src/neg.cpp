//PGM Info
//Take PGM file on cin & count rows, cols, no pixs, avg val of pix (3 decimals)
//Graham Buchanan
//01 Sept 2021

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <vector>

int main() {

	std::vector <int> v; //pixel data
	std::string s; //P2
	unsigned int r = 0; //rows
	unsigned int c = 0; //cols
	int d = 0; //pix val
	unsigned int i = 0; //it

	//Need to check string, cols, and rows independently for fails
	//If there is not P2, cols > 0, rows > 0, then 255 in that order
	if(!(std::cin >> s) || (s != "P2")) {
		std::cerr << "Bad PGM file -- first word is not P2" << std::endl;
		return 1;
	} else if(!(std::cin >> c) || (c < 1)) {
		std::cerr << "Bad PGM file -- No column specification" << std::endl;
		return 1;
	} else if(!(std::cin >> r) || (r < 1)) {
		std::cerr << "Bad PGM file -- No row specification" << std::endl;
		return 1;
	} else if(!(std::cin >> d) || (d != 255)) {
		std::cerr << "Bad PGM file -- No 255 following the rows and columns" << std::endl;
		return 1;
	} else {
		for(i = 0; i < (r * c); ++i) {
				//Read in all pixel values as integers. If value is <0 or >255, fail
				if(!(std::cin >> d)) {
					std::cerr << "Bad PGM file -- Not enough pixels" << std::endl;
					return 1;
				} else if ((d < 0) || (d > 255)) {
					std::cerr << "Bad PGM file -- pixel " << (i) << " is not a number between 0 and 255" << std::endl;
					return 1;
				} else {
					//Inversing pixel data - subtract from 255
					d = 255 - d;
					v.push_back(d);
				}
			std::cin.clear();
		}
		//After we have read *all* pixels, if there are any others, quit out
		if(std::cin >> d) {
			std::cerr << "Bad PGM file -- Extra stuff after the pixels" << std::endl;
			return 1;
		} else {
			//Now print every reversed pixel data in cout
			std::cout << "P2" << std::endl << c << ' ' << r << std::endl << 255 << std::endl;
			for(i = 0; i < v.size(); ++i) std::cout << v[i] << ' ';
			std::cout << std::endl;
		}
	}
	return 0;	
}

