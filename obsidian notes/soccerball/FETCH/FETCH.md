Graham Buchanan\ gbuchan2@vols.utk.edu for any questions

SOCCERBALL is a cpp-based application that emulates a single robot playing soccer.  
This current version only implements a 'fetch bot' for eons to train on.  
Given a centroid (x, y) and minEnclosingCircle radius (r), we determine how to move the robot.  
Basic idea is to maximize radius (r) and center (x, y).  
Not super complicated, but it is a starting place.

### HARDWARE

The current hardware setup includes:

1. ESP8266 NMCU1.0 devboard hosting a WebServer. This board will detect HTML requests to the server and update its serial buffer accordingly.
2. Arduino Uno driving L298N motor controller. The Uno is checking its serial buffer for updates from the ESP.
3. Raspberry Pi4b controlling and operating both and camera and neural network. The Pi4b pipes its output to the WebServer hosted by the ESP.

### SOFTWARE

_Using the NEUROMORPHIC-UTK/FRAMEWORK_

**Params**

{ "num": 1,       # legacy of the old 'xor' app. Do not change
 "num_range": 4,   # determines the number of bits for the RNG ==> max observation = 2^n - 1; must be >= 3
 "num_tests": 50,  
 "human_user": 0,  # if using '-a fifo', run this to see the midpoint, lower, upper values
 "octave": 0}     # set to 1 for a tighter lower, upper

**Logic**\ Lower and Upper are effectively quartiles (think box and whiskers).

When 'octave' is set to 1, we now calculate a half-step up/down for the lower/upper quartile, respectively.

midpoint = pow(2, num_range-1) -1;
lower = (octave) ? pow(2, num_range-2) + pow(2, num_range-3)-1 : pow(2, num_range-2)-1;
upper = (octave) ? 2*pow(2, num_range-2)+pow(2, num_range-3)-1 : pow(2, num_range-1)+pow(2, num_range-2)-1;

This breaks down the generic logic of the application:

actions:
0 - forward
--> if r < lower && x { (lower, upper)
1 - left
--> if x < lower
2 - backward
--> if r > midpoint && x { (lower, upper)
3 - right
--> if x > upper
4 - stop
--> if lower < r < upper && x { (lower, upper)

Here is the literal logic of how we determine the right answer inside the application.cpp:

if(last_observation[2] != 0) {
  //we see an object on screen (r != 0)
  //decide which direction to go
  if(last_observation[0] >= upper) {
    // x > upper, the object is on the right half of the screen
    answer = 3;
  } else if(last_observation[0] <= lower) {
  // x < lower, object is on the left half of the screen`
    answer = 1;
  } else if(last_observation[2] <= lower) {
    // r <= lower, the object is too far away
    answer = 0;
  } else if(last_observation[2] >= midpoint) {
    // r > midpoint, the object is too close (realistically, this means the object fills the screen)
    answer = 2;
  } else {
    // lower < x < upper
    // lower < r < midpoint
    // both conditions must be satisfied, so we stop
    answer = 4;
  }
} else answer = 1; //turn left because we do not see anything`

**Compiling** `/framework/cpp-apps> (cd applications/soccerball ; make clean ; make)` `/framework/cpp-apps> make app=soccerball proc=risp app_clean all`

**Running First-In, First-Out** `/framework/cpp-apps> ./bin/soccerball_risp -a fifo --show_observations --show_rewards --extra_app_params '{"num_tests": 5, "octave": 1, "human_user": 1}'`

midpint: 7 lower: 5 upper: 9      # this is due to setting 'human_user' to 1 as above; tells us the midpoint, lower_quartile, upper_quartile
f l b r s = 0 1 2 3 4             # prints a reminder of the acceptable inputs: (in order) forward, left, back, right, stop
Step 0000. Observations: 10 1 15    # first observation [x y r] = [10 1 15]
[3]                                 # x > upper, so we turn right
Step 0000.  Reward: 1
Step 0001. Observations: 13 4 3     # x > upper, so we turn right
[3]
Step 0001.  Reward: 1         `
Step 0002. Observations: 3 7 4
[1]                                 # x < lower, so we turn left
Step 0002.  Reward: 1
Step 0003. Observations: 0 13 4
[1]                                 # x < lower, so we turn left
Step 0003.  Reward: 1
Step 0004. Observations: 6 6 5
[0]                                 # lower < x < upper && lower < r < midpoint, so we stop
Step 0004.  Reward: 1
Fitness 5

**Training** `/framework/cpp-apps> ./bin/soccerball_risp -a train --encoder '{"val": {"flip_flop": 4}}' --extra_app_params '{"num_tests": 100}' --extra_eons_params '{"population_size": 2000}' --episodes 10 --threads 16`

Epoch:   0     Time:    0.5      Best: 47
Epoch:   1     Time:    0.5      Best: 65.8
Epoch:   2     Time:    0.5      Best: 71.2
...
Epoch:  48     Time:    0.6      Best: 91.9
Epoch:  49     Time:    0.6      Best: 91.9

Still working on the encoder.  
Increasing the bins for 'flip_flop' is likely the best; however, increasing size may add unncessary computation time.  
Also, we just changed a lot of the logic, so we still need to identify the best option now.

### BOT

Chassis has yet to be made

1. Twin DC-Motors operated at 12v by L298N motor controller
2. Logitech 720p webcam