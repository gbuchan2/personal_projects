//AVLTREE_LAB.CPP
//Implement the AVLTREE data structure
//Graham Buchanan
//02 December 2021

#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include "avltree.hpp"
using namespace std;
using CS202::AVLTree;
using CS202::AVLNode;

/***This program seeks to fully implement the AVLTREE data structure.
 *  An AVLTREE is a Binary Search Tree that maintains its balance throughout runtime.
 *  It consists of AVLNodes that hold a key, a value, and the appropriate links to other AVLNodes.
 *  This particular file finishes the implementation of Insertion and Deletion.
 *  It further implements non-Class methods to detect and correct imbalances within the Tree.*/

bool imbalance(const AVLNode *n);
void rotate(AVLNode *n);
void fix_height(AVLNode *n);
void fix_imbalance(AVLNode *n);

//Detect imbalances within the tree
bool imbalance(const AVLNode *n) {

	int lh;
	int rh;

	//Run until n is the sentinel and set the appropriate heights
	if(n->height == 0) return false;
	lh = (n->height != 0) ? n->left->height : 0;
	rh = n->right->height;

	//Check for height imbalances
	if((lh > rh) && ((lh - rh) > 1)) return true;
	if((lh < rh) && ((rh - lh) > 1)) return true;
	else return false;
}

//Rotate about a specific node
//Swap with n's parent
void rotate(AVLNode *n) {

	//set the appropriate pointers
	AVLNode *p = n->parent;
	AVLNode *gp = p->parent;
	AVLNode *m;
	AVLNode *l = n->left;
	AVLNode *r = n->right;

	//cannot rotate the sentinel!
	if(p->height == 0) return;
	
	//Default condition is left-side, other is right-side
	//left-side -> right rotation, vice-versa
	//m pointer really does nothing, it just makes this more legible
	if(n == p->left) { 
		m = r;
		n->parent = gp;
		p->left = m;
		n->right = p;
		p->parent = n;
		m->parent = p;
		if(p == gp->left) gp->left = n;
		else gp->right = n;
	} else { 
		m = l;
		n->parent = gp;
		p->right = m;
		n->left = p;
		p->parent = n;
		m->parent = p;
		if(p == gp->left) gp->left = n;
		else gp->right = n;
	}

	//reset the lowest node's height, then fix the height of the tree
	p->height = 1;
	fix_height(p);
}

//Recursively run back to the root and fix every height
void fix_height(AVLNode *n) {

	//change heights of all nodes EXCEPT sentinel.
	//Then, run through the height of the node's parent
	if(n->height == 0) return;
	n->height = (1 + ((n->right->height >= n->left->height) ? n->right->height : n->left->height));
	fix_height(n->parent);
}

//Detect an imbalance (Zig Zig or Zig Zag) and run the appropriate solution.
void fix_imbalance(AVLNode *n) {
	
	//Treat n as the root of a subtree
	//c is the child
	//g is the grandchild
	AVLNode *c;
	AVLNode *g;
	bool dir = false; //true = right
	bool zz = false; //true = zigzag
	size_t h = n->height - 1;

	//Detect the positioning of the child node that causes the imbalance
	//set the dir flag appropriately
	if(n->left->height == h) {
		c = n->left;
		dir = false;
	} else {
		c = n->right;
		dir = true;
	}

	--h; //h = height of the child's greatest child

	//First check left (yes it looks weird)
	//Identify the side of the child the grandchild is on using the same method as above
	//If g is the same as dir, then zigzig, else zigzag
	//default case should be zigzig (zz = false)
	if(!dir) {
		if(c->left->height == h) {
			g = c->left;
			zz = false;
		} else {
			g = c->right;
			zz = true;
		}
	} else {
		if(c->right->height == h) {
			g = c->right;
			zz = false;
		} else {
			g = c->left;
			zz = true;
		}
	}

	//When there is a zigzig, need to rotate only once about the child
	//When there is a zigzag, need to rotate about grandchild twice
	//Run the fix_height method on the lowest node
	if(!zz) {
		rotate(c);
		fix_height(g);
	} else {
		rotate(g);
		rotate(g);
		fix_height(c);
	}
}

//Assignment Overload
AVLTree& AVLTree::operator= (const AVLTree &t)        
{

	//Clear out the tree
	//Set the sentinel's pointers correctly
	//Use the recursive_postorder_copy method defined below
	//set the size (very important!)
	Clear();
	sentinel->right = recursive_postorder_copy(t.sentinel->right);
	sentinel->right->parent = sentinel;
	sentinel->left = NULL;
	size = t.size;
	return *this;
}

/********MOST OF THESE IMPLEMENTATIONS HAVE NOT BEEN CHANGED FROM THE LAB WRITEUP
 * ALL ADDITIONS TO THE BELOW CODE WILL BE NOTATED! */

/* I simply took Insert and Delete from their binary search tree
   implementations.  They aren't correct for AVL trees, but they are
   good starting points.  */

bool AVLTree::Insert(const string &key, void *val)
{
	AVLNode *parent;
	AVLNode *n;
	AVLNode *a;
	size_t h = 0;

	parent = sentinel;
	n = sentinel->right;

	/* Find where the key should go.  If you find the key, return false. */

	while (n != sentinel) {
		if (n->key == key) return false;
		parent = n;
		n = (key < n->key) ? n->left : n->right;
	}

	/* At this point, parent is the node that will be the parent of the new node.
	   Create the new node, and hook it in. */

	n = new AVLNode;
	n->key = key;
	n->val = val;
	n->parent = parent;
	n->height = 1;
	n->left = sentinel;
	n->right = sentinel;

	/* Use the correct pointer in the parent to point to the new node. */

	if (parent == sentinel) {
		sentinel->right = n;
	} else if (key < parent->key) {
		parent->left = n;
	} else {
		parent->right = n;
	}


	//Run fix_height on the lowest node (the newest node)
	fix_height(n);

	//iterate through the tree from the newest node and check for imbalances at each node
	for(a = n; a != sentinel; a = a->parent) {
		if(imbalance(a)) fix_imbalance(a);
	}

	/* Increment the size */
	size++;
	return true;
}

bool AVLTree::Delete(const string &key)
{
	AVLNode *n, *parent, *mlc;
	string tmpkey;
	void *tmpval;
	AVLNode *a;

	/* Try to find the key -- if you can't return false. */

	n = sentinel->right;
	while (n != sentinel && key != n->key) {
		n = (key < n->key) ? n->left : n->right;
	}
	if (n == sentinel) return false;

	/* We go through the three cases for deletion, although it's a little
	   different from the canonical explanation. */

	parent = n->parent;

	/* Case 1 - I have no left child.  Replace me with my right child.
	   Note that this handles the case of having no children, too. */

	if (n->left == sentinel) {
		if (n == parent->left) {
			parent->left = n->right;
		} else {
			parent->right = n->right;
		}
		if (n->right != sentinel) n->right->parent = parent;
		delete n;
		size--;

		/* Case 2 - I have no right child.  Replace me with my left child. */

	} else if (n->right == sentinel) {
		if (n == parent->left) {
			parent->left = n->left;
		} else {
			parent->right = n->left;
		}
		n->left->parent = parent;
		delete n;
		size--;

		/* If I have two children, then find the node "before" me in the tree.
		   That node will have no right child, so I can recursively delete it.
		   When I'm done, I'll replace the key and val of n with the key and
		   val of the deleted node.  You'll note that the recursive call 
		   updates the size, so you don't have to do it here. */

	} else {
		for (mlc = n->left; mlc->right != sentinel; mlc = mlc->right) ;
		tmpkey = mlc->key;
		tmpval = mlc->val;
		Delete(tmpkey);
		n->key = tmpkey;
		n->val = tmpval;
		return true;
	}

	//Since n has been deleted, need to set it to new node
	//start at parent because the subtree of n should not have been changed
	n = parent;

	//iterate through tree until root and fix the height of the node while checking for imbalances
	for(a = n; a != sentinel; a = a->parent) {
		fix_height(a);
		if(imbalance(a)) fix_imbalance(a);
	}
	return true;
}

/* You need to write these two.  You can lift them verbatim from your
   binary search tree lab. */

vector <string> AVLTree::Ordered_Keys() const
{
	//call make_key_vector on the root
	vector <string> rv;
	make_key_vector(sentinel->right, rv);
	return rv;
}

void AVLTree::make_key_vector(const AVLNode *n, vector<string> &v) const
{
	//run through the entire tree, storing all nodes and their respective children
	if(n == sentinel) return;
	make_key_vector(n->left, v);
	v.push_back(n->key);
	make_key_vector(n->right, v);

}

size_t AVLTree::Height() const
{
	return sentinel->right->height;
}

/* You need to write this to help you with the assignment overload.
   It makes a copy of the subtree rooted by n.  That subtree is part
   of a different tree -- the copy will be part of the tree that
   is calling the method. */

AVLNode *AVLTree::recursive_postorder_copy(const AVLNode *n) const
{

	AVLNode *a;
	//make a = n...return a

	if(n->height == 0) return sentinel;

	a = new AVLNode;
	a->val = n->val;
	a->key = n->key;
	a->height = n->height;

	//copy left, copy right, copy parent
	a->left = recursive_postorder_copy(n->left);
	a->left->parent = a;
	a->right = recursive_postorder_copy(n->right);
	a->right->parent = a;
	return a;
}
