#include <cstdio>
#include <cstdint>

struct Header {
	uint16_t version;
	uint16_t offset;
	uint32_t length;
};

struct Value {
	uint32_t key;
	int32_t value;
};

int main() {
	Header h;
	FILE *fl;
	fl = fopen("inclass.bin", "rb");
	if(nullptr == fl) {
		perror("fl");
		return -1;
	}
	int size = fread(&h, 1, sizeof(Header), fl);
	printf("Version: %d, Offest: %u, Length: %u\n", 
			h.version, h.offset, h.length);

	fseek(fl, h.offset, SEEK_SET);

	Value *value;
	value = new Value[h.length/sizeof(Value)];
	fread(value, 1, h.length, fl);
	printf("%d\n", value[4].value);
	fclose(fl);

	return 0;
}
