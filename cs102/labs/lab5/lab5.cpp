//lab5.cpp PART B
//Shopping Cart Simulator by and for broke college students
//Graham Buchanan
//12 November 2020

#include <iostream>
#include <vector>
#include <iomanip>
#include <string>

using namespace std;

//class declare "Item" class
class Item 
{

	//class_var declare necessary member variables
	private:
		string itemName;
		string itemDesc;
		float itemPrice;
		int itemNum;

	public:

		//class_funct declare default constructor--sets vars to "0"
		Item(string = "none", string = "none", float = 0, int = 0);

		//class_funct Item "set name" define
		void SetName(string n)
		{
			itemName = n;
		}

		//class_funct Item "get Name"
		string GetName() const
		{
			return itemName;
		}

		//class_funct Item "set price" define
		void SetPrice(float p)
		{
			itemPrice = p;
		}

		//class_funct Item "get Price"
		float GetPrice() const
		{
			return itemPrice;
		}

		//class_funct Item "set num"
		void SetQuantity(int q)
		{
			itemNum = q;
		}

		//class_funct Item "get num"
		int GetQuantity() const
		{
			return itemNum;
		}

		//class_funct Item "set desc"
		void SetDescription(string d)
		{
			itemDesc = d;
		}

		//class_funct Item "get desc"
		string GetDescription() const
		{
			return itemDesc;
		}

		//class_funct Item Print define
		void Print()
		{
			//txt print out necessary variables
			cout << GetName() << ": " << GetQuantity() << " at $";
			cout << setprecision(2) << fixed << GetPrice() << " = $" << GetPrice() * GetQuantity() << endl;
		}

		//class_funct Item Print Description define
		void PrintDescription()
		{
			cout << GetName() << ": " << GetDescription();
		}

};

//class declare "Shopping Cart" class
class ShoppingCart
{

	//class_var declare necessary member vars
	private:
		string custName;
		string custDate;
		vector <Item> list;

		//class_funct ShoppingCart "find item index" 
		int FindItemIndex(const string& search) const
		{
			//var index integer
			int s = -1;

			//loop through the scope of the Items Vector
			for( unsigned int i = 0; i < list.size(); ++i)
			{
				//cond if search string matches itemName from Item Vector
				if(list[i].GetName() == search)
				{
					//var index integer = index number
					s = i;

					break;
				}
			}

			return s;
		}


	public:

		//class_funct declare default constructor
		ShoppingCart(string = "None", string = "April 1, 2020");

		//class_funct class mutator declare
		void AddItem(const Item& newItem)
		{
			list.push_back(newItem);
		}

		//class_funct ShoppingCart "remove search item"
		void RemoveItem(const string& del)
		{
			//var index integer
			int s = FindItemIndex(del);

			//cond if no item found...
			if(s == -1)
				cout << "Item not found in cart." << endl;

			//cond if item found...
			else
				list.erase(list.begin() + s);
		}

		//class_funct ShoppingCart "change item num"
		void ChangeQuantity(const string& n, int cq)
		{
			//var index integer
			int s = FindItemIndex(n);

			//cond if user wants to change num...
			if(cq > 0)
			{

				//cond if no search yield
				if(s == -1)
					cout << "Item not found in cart." << endl;

				//cond if search yield
				else
					list[s].SetQuantity(cq);
			}

			//cond if user wants to change num to '0' or less...
			else
				RemoveItem(n);
			
		}

		//class_funct Shopping Cart "get name"
		string GetCustomerName() const
		{
			return custName;
		}

		//class_funct Shopping Cart "get date"
		string GetDate() const
		{
			return custDate;
		}

		//class_funct ShoppingCart "total num" of items
		int GetTotalQuantity() const
		{
			//var sum of nums
			int someNum = 0;

			//loop through vector of Items
			for(unsigned int i = 0; i < list.size(); ++i)
			{
				//var sum of Nums = sum of nums
				someNum += list[i].GetQuantity();
			}

			return someNum;
		}

		//class_funct ShoppingCart "total cost" of items
		double GetTotalCost() const
		{
			//var sum of costs
			double someCost = 0;

			//loop through vector of items
			for(unsigned int i = 0; i < list.size(); ++i)
			{
				//var sum of costs = total costs
				someCost += list[i].GetQuantity() * list[i].GetPrice();
			}

			return someCost;
		}

		//class_funct Shopping Cart "Print Total" of items
		void PrintTotal()
		{
			//txt returns user's cart and date
			cout << endl << GetCustomerName() << "'s Shopping Cart - " << GetDate();
			cout << "\nNumber of Items: " << GetTotalQuantity() << endl << endl;

			//cond if there are more than 0 items in cart
			if(list.size() > 0)
			{
				//loop through vector of Items
				for( unsigned int j = 0; j < list.size(); j++)
				{
					list[j].Print();
				}
			}

			//cond if there is nothing in cart
			else 
			{
				cout << "Shopping cart is empty.\n";
			}

			//txt print out total cost of items
			cout << "\nTotal: $" << setprecision(2) << fixed << GetTotalCost() << endl;

		}

		//class_funct ShoppingCart "Print Desc"
		void PrintDescriptions()
		{
			//txt return user's cart and date
			cout << endl << custName << "'s Shopping Cart - " << custDate << endl << "\nItem Descriptions" << endl;

			//cond if there are items in the cart
			if(list.size() > 0)
			{
				//loop through vector of Items
				for(unsigned int i = 0; i < list.size(); i++)
				{
					list[i].PrintDescription();
					cout << endl;
				}
			}

			//cond if there is nothing in the cart
			else
			{
				cout << "Shopping cart is empty." << endl;
			}
		}
};



//class_funct Item default constructor
Item::Item(string a, string b, float p, int q) : itemName(a), itemDesc(b), itemPrice(p), itemNum(q)
{
}

//class_funct Shopping Cart default constructor
ShoppingCart::ShoppingCart(string n, string d) : custName(n), custDate(d) 
{
}

//funct to print out menu
char PrintMenu()
{
	//txt menu print and user prompt
	char menuSelect;
	cout << "\nMENU" << endl;
	cout << "a - Add item to cart" << "\nd - Remove item from cart" << "\nc - Change item quantity";
	cout << "\ni - Output item descriptions" << "\no - Output shopping cart" << "\nq - Quit";
	cout << endl << endl << "Choose an option: ";
	cin >> menuSelect;
	return menuSelect;
}

//wow I did not expect this one to appear :)
int main() 
{

	//var declare necessry vars
	string user_name;
	string user_date;
	string name;
	string description;
	float price;
	int number;
	char userSelect;

	//txt prompt user for their name and date
	cout << "Enter customer name: ";
	getline(cin, user_name);
	cout << "Enter today's date: ";
	getline(cin, user_date);

	//class initialize ShoppingCart class
	ShoppingCart banana(user_name, user_date);

	//txt return user's name and date
	cout << "\nCustomer name: " << user_name << endl;
	cout << "Today's date: " << user_date << endl;

	//var userSelect equals null
	userSelect = '\0';

	//loop as long as no QUIT
	while((userSelect = PrintMenu()) != 'q')
	{
		//cond switch statement
		switch(userSelect)
		{

			//cond case a
			case 'a':
			{
		
				//txt prompt user for inputs
				cout << "\nADD ITEM TO CART" << endl;
				cout << "Enter the item name: ";
				getline(cin>>ws, name);
				cout << "Enter the item description: ";
				getline(cin>>ws, description);
				cout << "Enter the item price: $";
				cin >> price;
				cout << "Enter the item quantity: ";
				cin >> number;

				//class initialize classes and vector of Items
				Item apple(name, description, price, number);
				banana.AddItem(apple);

				break;
			}
		

			//cond case d
			case 'd':
			{
				//txt prompt user for inputs
				cout << "\nREMOVE ITEM FROM CART" << endl;
				cout << "Enter the item name to remove: ";
				getline(cin>>ws, name);

				//class remove item from cart
				banana.RemoveItem(name);
			
				break;
			}

			//cond case c
			case 'c':
			{
				//txt prompt user for inputs
				cout << endl << "CHANGE ITEM QUANTITY";
				cout << endl << "Enter the item name: ";
				getline(cin>>ws, name);
				cout << "Enter the new quantity: ";
				cin >> number;

				//class change item num
				banana.ChangeQuantity(name, number);

				break;
			}

			//cond case i
			case 'i':
			{
				//class print item descs
				banana.PrintDescriptions();		
				
				break;
			}

			//cond case o
			case 'o':
			{	
				//class print item totals
				banana.PrintTotal();
				
				break;
			}

			//cond DEFAULT....BLANK LINE
			default:
			{	
				cout << endl;
			}

			//var reset userSelect
			userSelect ='\0';
		}
		

	}
	return 0;
}



