//DIVISION.HPP
//All Division and Conference Data
//Graham Buchanan 
//11 March 2022

#include "team.hpp"
#include <map>

class Division {
	public:
		Division(); //Base Constructor
		
		bool Add_Team(Team *t);
		bool Add_Division(std::vector<Team *> &v); //Add division (create a conference)

		void Print();
	protected:
		std::string name; //Name of Division/Conference
		
		std::map<std::string, Team *> TM; //Storing all team by Location and Name
		std::vector<Team *> TV; //Storing all Teams by ID

		std::map<std::string, Division *> DM; //Storing all divisions (if this is conf)
}
