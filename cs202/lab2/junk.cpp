//junk.cpp
//Creating a PGM
//Graham Buchanan
//01 Sept 2021

#include <iostream>
#include <iomanip>

int main() {

	unsigned char c; //0-255
	int i = 0;
	int j = 0;

	c = 0;
	std::cout << "P2" << std::endl << "10 10" << std::endl << 255 << std::endl;
	for(i = 0; i < 10; ++i) {
		c = 10 * i;
		for(j = 0; j < 10; ++j) {
			std::cout << c + (j * 10) << ' ';
		}
		std::cout << std::endl;
	}
	return 0;

}
