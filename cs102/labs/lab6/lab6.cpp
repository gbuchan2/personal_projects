//lab6.cpp
//manual vector because normal vectors aren't good enough
//Graham Buchanan
//20 November 2020

#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

using namespace std;

//GIVEN BY INSTRUCTOR
class doublevector {
	static constexpr double DOUBLE_DEFAULT_VALUE = -5.55;
	double *mValues;
	int mNumValues;

	//GIVEN BY INSTRUCTOR
	public:
	doublevector();
	~doublevector();
	void resize(int new_size, double initial_value = DOUBLE_DEFAULT_VALUE);
	void push_back(double value);
	double &at(int index);
	const double &at(int index) const;
	unsigned long size() const;
};

//GIVEN BY INSTRUCTOR
void print_all(const doublevector &v) {
	if (v.size() == 0) {
		cout << "Vector is empty.\n";
	} 
	else {
		unsigned long i;
		for (i = 0; i < v.size(); i++) {
			cout << "[" << setfill('0') << right << setw(3) << i << "] = " << fixed
				<< setprecision(4) << v.at(i) << '\n';
		}
	}
}

//GIVEN BY INSTRUCTOR
int main() {
	doublevector v;
	do {
		string command;
		cout << "Enter a command ('quit' to quit): ";
		if (!(cin >> command) || command == "quit") {
			break;
		} 

		else if (command == "push_back") {
			double value;
			cin >> value;
			v.push_back(value);
			cout << "Pushed back " << fixed << setprecision(4) << value << '\n';
		} 

		else if (command == "resize") {
			string line;
			int new_size;
			double initial;
			cin >> new_size;
			getline(cin, line);
			istringstream sin(line);

			if (sin >> initial) {
				v.resize(new_size, initial);
			}

			else {
				v.resize(new_size);
			}
		} 

		else if (command == "size") {
			cout << v.size() << '\n';
		} 

		else if (command == "print") {
			print_all(v);
		} 

		else if (command == "get") {
			int index;
			cin >> index;

			try {
				cout << "Value at " << index << " = " << setprecision(4) << fixed
					<< v.at(index) << '\n';
			} 

			catch (out_of_range &err) {
				cout << err.what() << '\n';
			}
		} 

		else if (command == "set") {
			double d;
			int index;
			cin >> index >> d;

			try {
				v.at(index) = d;
				cout << "SET: " << index << " = " << setprecision(4) << fixed
					<< v.at(index) << '\n';
			} 

			catch (out_of_range &err) {
				cout << err.what() << '\n';
			}
		} 

		else if (command == "clear") {
			v.resize(0);
		} 

		else {
			cout << "Invalid command '" << command << "'\n";
		}

	} while (true);

	cout << '\n';

	return 0;
}

// Write your class members below here.

//class_funct doublevector base constructor resets array and size
doublevector::doublevector() 
{
	mValues = nullptr;
	mNumValues = 0;
}

//class_funct doublevector base destructor resets array and size
doublevector::~doublevector()
{
	if(mValues != nullptr)
	{
		delete[] mValues;
		mValues = nullptr;
	}
	mNumValues = 0;
	
}

//class_funct doublevector "resize" define
void doublevector::resize(int new_size, double initial_value) 
{
	//var declare counter var
	unsigned int j;

	//cond if new size is less than 0 or equal to curr size...do nothing
	if(new_size < 0 || new_size == mNumValues)
	{
	}

	//cond if new size equals 0...reset mValues
	else if(new_size == 0)
	{
		//cond if mValues is NOT a NULL
		if(mValues != nullptr)
		{
			//var resets mValues
			delete[] mValues;
			mValues = nullptr;
		}
		
		//var resets size
		mNumValues = 0;
	}

	//cond if new size DOES NOT EQUAL 0...
	else
	{
		//var declare NEWvalue array
		double *nValues = new double[new_size];

		//cond if new size is greater than curr size
		if((unsigned int)new_size > size())
		{
			//loop through curr size...set values for new array 
			for(j = 0; j < size(); ++j)
			{
				nValues[j] = mValues[j];
			}

			//loop through new size...set values for new array
			for(j = size(); j < (unsigned int)new_size; ++j)
			{
				nValues[j] = initial_value;
			}
		}

		//cond if new size is LESS than curr size
		else if((unsigned int)new_size < size())
		{

			//loop through new size...set values for new array
			for(j = 0; j < (unsigned int)new_size; ++j)
			{
				nValues[j] = mValues[j];
			}
		}

		//cond if mValues DOES NOT EQUAL NULL
		if(mValues != nullptr)
		{
			//var remove mValues
			delete[] mValues;
		}

		//var set mValues to nValues
		mValues = nValues;
		mNumValues = new_size;
	}
}

//class_funct doublevector "PUSH BACK" define
void doublevector::push_back(double value)
{
	//basically function resize
	resize(mNumValues + 1, value);
}

//class_funct doublevector "AT"... non-read-only version
double &doublevector::at(int index) 
{
	//GIVEN BY INSTRUCTOR
	if(index >= mNumValues) 
	{
		ostringstream sout;
		sout << "Invalid index #" << index;
		throw out_of_range(sout.str());
	}

	//cond if the input is valid...return value at index
	else
		return mValues[index];
}

//class_funct doublevector "AT"...read-only version
const double &doublevector::at(int index) const
{
	//GIVEN BY INSTRUCTOR
	if(index >= mNumValues) 
	{
		ostringstream sout;
		sout << "Invalid index #" << index;
		throw out_of_range(sout.str());
	}

	//cond if the input is valid...return value at index [READ ONLY]
	else
		return mValues[index];
}

//class_funct doublevector "SIZE" define
unsigned long doublevector::size() const
{
	//var declare var
	unsigned long val_size;

	//var defines
	val_size = mNumValues;

	//return var
	return val_size;
}


