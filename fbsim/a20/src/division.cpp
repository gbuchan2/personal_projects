//DIVISION.CPP
//Definitions of the Division Class
//Graham Buchanan


/***UPDATE LOG***
 *  17 December 2021 - Creation of File
 *  18 December 2021 - Constructor & Destructor
 *  22 December 2021 - Data Accessors
 *  26 December 2021 - Save and Load functionality
 */
/*
#include "team.hpp"

class Division {

public:
Division(bool &s, std::vector<Team *> &v, std::string &n);
~Division();

void EditDivision();
Division* CreateSub(); //Creates a subdivision (maximum of two for now)
void BalanceSubs(); //Balances both subdivisions

protected:
std::map<std::string, Team *> tmMap;
std::map<std::string, Division *> subs; 
std::string name;
};*/

#include "division.hpp"
#include <map>

Division::Division(const std::vector<Team *> &v, const std::string &n) {


	size_t i = 0;
	Division *d = NULL;
	name = n;

	for(i = 0; i < v.size(); ++i) {
		tmMap.insert(std::pair<uint32_t, Team *>(v[i]->getID(), v[i]));
		v[i]->SetDivision(name);
	}
}

Division::~Division() {

	std::list<Division *>::reverse_iterator lit;
	// ****DO NOT DELETE tmList! Teams should be deleted from the League file!!!****
	if(!(subs.empty())) {
		for(lit = subs.rbegin(); lit != subs.rend(); ++lit) {
			delete *lit;
		}
		subs.clear();
	}
}

void Division::EditDivision() {

	//To be Added Later

}

void Division::CreateSub() {

	//To Be Added Later
}

void Division::BalanceSubs() {

	//To Be Added Later
}

int Division::NumSubs() {
	return subs.size();
}

int Division::NumTms() {
	return tmMap.size();
}

void Division::PrintData() const {

	size_t i = 0;
	std::map<uint32_t, Team *>::const_iterator mit;


	printf("\n   %s:\n", name.c_str());
		for(mit = tmMap.begin(); mit != tmMap.end(); ++mit) {
			mit->second->PrintData();
		}

}

std::string Division::GetName() const {
	return name;
}
