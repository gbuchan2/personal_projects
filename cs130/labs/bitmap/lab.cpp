//ver3.cpp -- inverting and flipping
//bitmap...reading, writing bitmap files
//Graham Buchanan
//24 February 2021

#include <cstdio>
#include <cstdint>
#pragma pack(push, 2)

struct BitmapFileHeader {
	uint16_t type; //type of file, Windows bitmap BM
	uint32_t size; //entire size of file, including headers
	uint16_t reserveda; //reserved for future expansion
	uint16_t reservedb; //reserved for future expansion
	uint32_t offset; //tells the offest from top of file to where pixel data is stored
};

struct BitmapInfoHeader {
	uint32_t size; //size of info header [should be 40]
	uint32_t width; //width of the picture
	uint32_t height; //height of the picture
	uint16_t planes; //number of layers, test pics = 1
	uint16_t bitcount; //number of bits per pixel, should be 24
	uint32_t compression; //compression used, there is none, should be zero
	uint32_t imagesize; //size of image, may be zero
	uint32_t x_pixels_per_meter; //number of pixels per meter horizontally
	uint32_t y_pixels_per_meter; //number of pixels per meter vertically
	uint32_t color_used; //color index used, should be 0
	uint32_t color_important; //should be 0
};

#pragma pack(pop)

struct Pixel {
	uint8_t red; //amount of red
	uint8_t blue; //amount of blue
	uint8_t green; //amount of green
};

//invert = 255 - pixel color value

int main(int argc, char **argv) {

	//command checking
	if(argc != 3) {
		printf("User input error.\n");
		return -1;
	}

	//opening structs
	BitmapFileHeader Header;
	BitmapInfoHeader Info;

	//opening the read file
	FILE *fl;
	fl = fopen(argv[1], "rb");
	if(nullptr == fl) {
		perror("fl");
		return -1;
	}

	//read in the Headers
	fread(&Header, 1, sizeof(BitmapFileHeader), fl);
	fread(&Info, 1, sizeof(BitmapInfoHeader), fl);

	//calculate our padding and Pixel pointer array
	uint8_t pad = 4 -((Info.width * sizeof(Pixel)) % 4);
	Pixel *pix = new Pixel[Info.height * Info.width];

	//reading pixel data...need to seek TO the data first
	fseek(fl, Header.offset, SEEK_SET);
	for(uint16_t i = 0; i < Info.height; ++i) {
		fread(&pix[i * Info.width], sizeof(Pixel), Info.width, fl);			
		fseek(fl, pad, SEEK_CUR);

	}

	//close the file
	fclose(fl);
	
	//INVERTING PIXEL DATA
	for(uint16_t k = 0; k < Info.height * Info.width; ++k) {
		//255 = ~0...easy subtraction
		pix[k].red = ~pix[k].red;
		pix[k].green = ~pix[k].green;
		pix[k].blue = ~pix[k].blue;
	}

	//FLIPPING ACROSS X=0
	for(uint16_t c = 0; c < Info.height; ++c) {
		for(uint16_t d = 0; d < Info.width / 2; ++d) {
			//use a temporary struct to hold data as we swap the pixel data
			Pixel temp = pix[c * Info.width + d];
			pix[c * Info.width + d] = pix[(c + 1) * Info.width - 1 - d];
			pix[(c + 1) * Info.width - 1 - d] = temp;
		}
	}

	//NOW WE BEGIN WRITING			
	//file opening
	fl = fopen(argv[2], "wb");
	if(nullptr == fl) {
		perror("fl");
		return -1;
	}

	//write the Header and Info verbatim from the reading
	fwrite(&Header, 1, sizeof(BitmapFileHeader), fl);
	fwrite(&Info, 1, sizeof(BitmapInfoHeader), fl);

	//print the pixel data...seek past the header and info
	fseek(fl, Header.offset, SEEK_SET);
	uint32_t padd = 0;

	//iterate through each row
	for(uint16_t i = 0; i < Info.height; ++i) {
		//place entire rows worth of data...then add padding
		fwrite(&pix[i * Info.width], sizeof(Pixel), Info.width, fl);		
		fwrite(&padd, 1, pad, fl);
	}

	//close it all up
	fclose(fl);
	delete[] pix;
	return 0;
}

