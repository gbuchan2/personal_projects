// lab3.cpp
// Developing a slot machine i.e. developing a gambling addiction
// Graham Buchanan
// 30 September 2020

#include <iostream>
#include <random>

using namespace std;

int main() {

	int user_pts;		//var user score
	int user_bet;		//var user wager
	int user_win;		//var user reward
	unsigned int seed;		//var seed
	int i;		//var i --> for loop
	int gamble[3];		//var gamble array 3 int
	int matches;		//var counts how many matches within the randos
	int plays;		//var counts how many iterations of the main while loop
	vector< vector<int> > tracker(2);		//var vector to carry all wagers(tracker0) and winnings(tracker1)
	int min_max[2][2] = { {999999, 0}, {999999, 0} };		//var array to carry max (col1) /min (col0) values


	default_random_engine rng(seed);		//rng Create RNG	
	uniform_int_distribution<int> r(2,7);		//rng Range of ints 2-7

	cout << "Input a seed: ";		//txt Prompts user for seed
	while ( !( cin >> seed) ) {		//loop guarantee seed input is valid
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		cout << "Input a seed: ";
	}

	user_pts = 100;		//var starts user with 100 "coins"
	user_bet = 1;		//var set user_bet to "1"
	user_win = 1;		//var set user_win to "1"
	plays = 0;		//var play count set to 0

	while(user_pts > 0) {		//loop while user does not enter 0
		cout << '\n';
		cout << "You currently possess " << user_pts << " coins. \n";

		cout << "Bet how many coins? ";		//txt prompts user for wager
		while( !( cin >> user_bet) || !(user_bet <= user_pts) || (user_bet < 0) ) {		//loop guarantee user_bet input is valid

			cin.clear();		
			cin.ignore(numeric_limits<streamsize>::max(), '\n');

			cout << "Bet how many coins? ";		
		}		
		if(user_bet == 0)		//cond if user enters 0...
			break;		//end loop
		cout << '\n';		//endline

		tracker.at(0).push_back(user_bet);		//var stores wager into tracker vector
		if(user_bet > min_max[0][1])		//var stores min/max wagers
			min_max[0][1] = user_bet;
		if(user_bet < min_max[0][0])
			min_max[0][0] = user_bet;

		user_pts -= user_bet;		//var subtract the wagered amount from total points (user_bet && user_pts)
		
		matches = 0;		//var set matches to zero
		for(i=0; i<3; ++i){		//loop 3 times
			gamble[i] = r(rng);		//var stores in array
			if(gamble[i] == gamble[i-1]) {		//cond if this gamble value matches the previous...
				matches += 1;		//var count one match
			}
		}

		for (i=0; i<3; ++i){		//loop 3 times
			cout << " " << gamble[i];		//txt prints array
		}
		cout << '\n';		// endline

		if(matches == 2) {		//cond if there are two matches (i.e. 3 same ##)
			if(gamble[0] == 7)		//cond if one of the values = 7
				user_win = 10;		//var reward = 10*
			else
				user_win = 5;		//var reward = 5*
		}
		else if(matches == 1) {		//cond if there is one match
			user_win = 2;		//var reward = 2*
		}
		else		//cond else... 
			user_win = 0;		//var no matches equals no win

		tracker.at(1).push_back(user_win*user_bet);		//var stores winning into row1 of tracker

		if(user_win*user_bet > min_max[1][1])		//var stores min/max winnings
			min_max[1][1] = user_win*user_bet;
		if(user_win*user_bet < min_max[1][0])
			min_max[1][0] = user_win*user_bet;

		if(user_win > 0)		//cond if you won...
			cout << "You won " << user_bet * user_win << " coins!" << '\n';		//txt Winning Congratulations
		else		//cond else...
			cout << "You did not win." << '\n';		//txt LOSER!

		user_pts += user_bet * user_win;		//var add coins based on win
		plays += 1;		//var play count

	}

	if(plays > 0) {		//cond if user played...

		cout << '\n';
		cout << "Play Summary:" << '\n';		//txt Begins play summary
		for( i = 0; i < plays; ++i) {		//cond for statement
			cout << "You wagered " << tracker.at(0).at(i) << " coins and won " << tracker.at(1).at(i) << " coins. \n";		//txt displays each wager and win		
		}
		cout << '\n';		//endline
		cout << "Your minimum wager was " << min_max[0][0] << " coins. \n";		//txt Play Summary.
		cout << "Your maximum wager was " << min_max[0][1] << " coins. \n";
		cout << "Your minimum prize was " << min_max[1][0] << " coins. \n";
		cout << "Your maximum prize was " << min_max[1][1] << " coins. \n";
	}


	return 0;
}
