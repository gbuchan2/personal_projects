//Checkerboard.cpp
//Generating a checkerboard pattern
//Graham Buchanan
//29 Aug 2021

#include <iostream>
#include <iomanip>

/* Welcome to my Chess program errr checkerboard program...
 * I used a lot of iterators to somewhat simplify the math...yes it may look ugly but it works
 * This whole program comes down to my print char 'p'...if chars suddenly stop working, this won't work anymore */

int main() {
	int r = 0; //rows
	int c = 0; //cols
	char sc = 0; //start
	int cs = 0; //cycle
	int w = 0; //width (no tt pt p)

	//vars
	char p = '\0'; //pt char
	int i = 0; //it
	int j = 0; //it
	int f = 0; //it

	if( !(std::cin >> r >> c >> sc >> cs >> w) ) {
		std::cerr << "usage: checkerboard  - stdin should contain R, C, SC, CS and W" << std::endl;
		return 1;
	}

	/* First thing is to check that all inputs are present and of satisfactory value (">0")
	 *	Three NESTED FOR LOOPS:
	 * ->For Loop I: count the rows
	 *	->For Loop F: make sure the rows are repeated enough times (to satisfy the width)
	 *	->For Loop J: count the columns
	 *	Thank you for checking out my program...hope you enjoy*/

	if((r > 0) && (c > 0) && (sc > 0) && (cs > 0) && (w > 0) && ((cs + sc) <= 127)) {
		for(i = 0; i < r; ++i) { //rows
			for(f = 0; f < w; ++f) {
				for(j = 0; j < c; ++j) { //cols
					p = (sc + ((i+j) % cs));
					std::cout << std::setfill(p) << std::setw(w) << p;
				}
				std::cout << std::endl;
			}
		}
	}
	else
		return 1;
	
	return 0;
}
