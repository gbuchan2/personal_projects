The United Proctor's Guild is the youngest among the guilds but continues to gain traction among the populace. The Proctors strive to provide all children a relatively equal chance at achieving employment, a movement supported originally by the [[Engr|Collective]] to increase recruitment. The guild offers caretaking for newborns in one of four nurseries across the station. This has offloaded the strain for communal help to parents, particularly as human reproduction has continued to fall under the control of the [[Arator]]ship and [[Scoper]]s. The Proctors also provide for the general education every child receives during their first 9 cycles, from basic mathematics and speech to physical exercise and nutrition. The Scopers and Proctors work closely in determining the brightest pupils amongst each cycle, in great secrecy from the wider public. Additionally, the Proctors have now taken over sole ownership of the public record in their Library on Sector 21. This holds the current history of mankind, the entire genetic tree, and other materials available to the public. Of course, one must be careful of who is writing the history that everyone studies.

### Guild
Name: "United Proctor's Guild"
Emblem: Book
Members: 101
Leader: [[Jakal]]
[[Humanity#The Grand Treaty|Treaty]] Seats: 4

### Occupations:
- Caretaker
- Professor
- Librarian
- Recorder

### Stations:
- The Library: [[21]]
- Lecture Hall: [[2]], [[9]], [[10]], [[20]], [[21]]
- Nursery: [[4]], [[5]], [[11]], [[21]]

### Notables:
- 