//SCHEDULE.HPP 
//Stores all games within an object
//Graham Buchanan
//11 March 2022

#include "game.hpp"
#include <vector>
#include <map>

class Schedule {
	public:
		Schedule(); //Base Constructor
		Schedule(std::vector<Game *> &v); //Constructor with params

		void Print();
	protected:
		std::vector<Game *> GV; //Game Vector
		std::map<uint32_t, Game *> GM; //Game Map, keyed by Game ID
}

