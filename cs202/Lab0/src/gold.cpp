//Lab0 - gold.cpp
//Finding gold in input
//Graham Buchanan
//25 Aug 2021

#include <iostream>
int main() {

	char c = '\0';
	int t = 0;
	/*While there is an input, read in individual chars.
	 * As long as char does not find dirt or lame rock, there must be gold. 
	 * Simple integer summation follows based on the ascii value of the char input. 
	 * Then, output the total.*/

	while(std::cin >> c) {
		if( (c != '-') && (c != '.')) {
			t += c - 'A' +1;
		}
	}
	std::cout << t << std::endl;
return 0;
}
