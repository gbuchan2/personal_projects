//ALPHA 1.01 
//Preparing initial storage systems
//College Football League Simulator

#include <iostream>
#include <vector>
#include <string>
#include <cstdio>
#include <set>
#include <map>

class Team {
  public:
		std::string Name;
    int off;
    int def;
    void Print() const;
};

class TMS {
	public:
		bool Read();
		const Team *Find(const std::string &name) const;
	protected:
		std::vector <Team *> TV;
		std::map <std::string, Team *> TM;
};

int main() {
	TMS tms;
//	const Team *t;

	tms.Read();
  return 0;
}

void Team::Print() const {
  printf("%s: %d off, %d def.\n", Name.c_str(), off, def);
}

bool TMS::Read() {
	Team *t;
	int notms = 0;
	printf("How many teams are there? >");
	std::cin >> notms;
	for(int i = 0; i < notms; ++i) {
		t = new Team;
		printf("\nPlease input the Name, Offensive Rating, and Defensive Rating.\n");
		std::cin >> t->Name >> t->off >> t->def;
		TV.push_back(t);
	}
	for(int i = 0; i < notms; ++i) TV[i]->Print();

	for(unsigned long i = 0; i < TV.size(); ++i) {
		t = TV[i];
		TM[t->Name] = t;
	}

	return true;
}

const Team *TMS::Find(const std::string &name) const {
	std::map<std::string, Team *>::const_iterator tit;
	tit = TM.find(name);
	if(tit == TM.end()) return NULL;
	return tit->second;
}
