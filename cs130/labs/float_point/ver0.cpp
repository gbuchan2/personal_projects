//float_lab.cpp
//COSC130
//Graham Buchanan
//15 February 2021

#include <cstdio>

struct Real {
	int sign;
	long exponent;
	unsigned long fraction;
};

Real Decode(int float_value);
int Encode(Real real_value);
Real Normalize(Real value);
Real Multiply(Real left, Real right);
Real Add(Real left, Real right);

int main(int argc, char *argv[]) {
	int left, right;
	char op;
	int value;
	Real rLeft, rRight, result;
	if(argc < 4) {
		printf("Usage: %s <left> <op> <right>\n", argv[0]);
		return -1;
	}
	sscanf(argv[1], "%f", (float *)&left);
	sscanf(argv[2], "%c", &op);
	sscanf(argv[3], "%f", (float *)&right);
	rLeft = Decode(left);
	rRight = Decode(right);
	if(op == 'x') {
		result = Multiply(rLeft, rRight);
	}
	else if(op == '+') {
		result = Add(rLeft, rRight);
	}
	else {
		printf("Unknown operator '%c'\n", op);
		return -2;
	}
	value = Encode(result);
	printf("%.3f %c %.3f = %.3f (0x%08x) \n", *((float*)&left), op, *((float*)&right), *((float*)&value), value);
	return 0;
}

Real Decode(int float_value) {
	//take a 32-bit number in IEEE-754 format
	//covert to Real structure
	char e = 0b10000001 
	long a = 0;
	Real d;
	d.exponent = ((float_value >> 23) & 0xff);
	for(unsigned int 
	d.sign = ((float_value >> 31) & 1);
	d.fraction = (float_value & 0x7ffff);
	return d; 

}
int Encode(Real real_value) {
	//take a Real structure
	//convert it to a 32-bit number in IEEE-754
	
	long e = 127;
	long c = 0;
	int d = 0;
	int f = 0;
	for(unsigned int i = 0; i < 8; ++i) {
		int a = (e >> i) & 1;
		int b = (real_value.exponent >> i) & 1;
		d |= (( a ^ b) ^ c) << i;
		c = (a * b) | (b & c) | (c & a);
	}
	f |= (real_value.sign << 31);
	f |= (d << 23);
	f |= real_value.fraction;
	return f;
}
Real Normalize(Real value) {
	//manipulate the exponent and fraction until the number is "1.<fraction"
	

}
Real Multiply(Real left, Real right) {
	//multiply two Real structures together
	//return Real structure
	

}
Real Add(Real left, Real right) {
	//add two Real structures together
	//return result
	unsigned long c = 0;
	long d = 0;
	Real a;
	left.fraction |= (1 << 23);
	right.faction |= (1 << 23);
	right.fraction = (right.fraction << right.exponent);
	left.fraction = (left.fraction << left.exponent);
	for(unsigned int i = 0; i < 23; ++i) {
		long f = (right.fraction >> i) & 1;
		long b = (left.exponent >> i) & 1;
		d |= (a ^ b) ^ c;
		c = (a & b) | (b & c) | (a & c);
	}
	a.fraction = d;
	return Normalize(a);

	 
	

	
	
}

