#include "TreeAndVertex.cpp"

int main(int argc, char **argv)
{
  int i;
  class TreeAndVertex TheClass;
  int retval;
  vector <int> tree;

  if (argc != 2) { fprintf(stderr, "usage: a.out num or - to enter on stdin\n"); exit(1); }

  if ((string) argv[1] == "-") {
    while (cin >> i) tree.push_back(i);
  } else {

    if (atoi(argv[1]) == 0) {
      tree.push_back(0);
      tree.push_back(0);
      tree.push_back(0);
    }
  
    if (atoi(argv[1]) == 1) {
      tree.push_back(0);
      tree.push_back( 1);
      tree.push_back( 2);
      tree.push_back( 3);
    }
  
    if (atoi(argv[1]) == 2) {
      tree.push_back(0);
      tree.push_back( 0);
      tree.push_back( 2);
      tree.push_back( 2);
    }
  
    if (atoi(argv[1]) == 3) {
      tree.push_back(0);
      tree.push_back( 0);
      tree.push_back( 0);
      tree.push_back( 1);
      tree.push_back( 1);
      tree.push_back( 1);
    }
  
    if (atoi(argv[1]) == 4) {
      tree.push_back(0);
      tree.push_back( 1);
      tree.push_back( 2);
      tree.push_back( 0);
      tree.push_back( 1);
      tree.push_back( 5);
      tree.push_back( 6);
      tree.push_back( 1);
      tree.push_back( 7);
      tree.push_back( 4);
      tree.push_back( 2);
      tree.push_back( 5);
      tree.push_back( 5);
      tree.push_back( 8);
      tree.push_back( 6);
      tree.push_back( 2);
      tree.push_back( 14);
      tree.push_back( 12);
      tree.push_back( 18);
      tree.push_back( 10);
      tree.push_back( 0);
      tree.push_back( 6);
      tree.push_back( 18);
      tree.push_back( 2);
      tree.push_back( 20);
      tree.push_back( 11);
      tree.push_back( 0);
      tree.push_back( 11);
      tree.push_back( 7);
      tree.push_back( 12);
      tree.push_back( 17);
      tree.push_back( 3);
      tree.push_back( 18);
      tree.push_back( 31);
      tree.push_back( 14);
      tree.push_back( 34);
      tree.push_back( 30);
      tree.push_back( 11);
      tree.push_back( 9);
    }
  }

  retval = TheClass.get(tree);
  cout << retval << endl;

  exit(0);
}
