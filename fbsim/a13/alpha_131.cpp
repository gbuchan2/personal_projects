//ALPHA 1.31
////Preparing "Editing Schedule"
//College Football League Simulator

#include <iostream>
#include <vector>
#include <string>
#include <cstdio>
#include <limits>
#include <map>

char PrintMenu() {
	char in = '\0';
	printf("\nCommands:\n"
			"     t - Input team data\n"
			"     y - Edit team data\n"
			"     s - Input schedule\n"
			"     a - Generate schedule (one home/away, no weeks)\n"
			"     d - Print schedule\n"
			"     p - Print standings\n"
			"     g - Play the next game in the schedule\n"
			"     h - Simulate the next game\n"
			"     f - Play the full schedule\n"
			"     r - Simulate the full schedule\n"
			"     e - Edit scores and standings\n"
			"     q - Quit\n"
			"What would you like to do? > ");
	std::cin >> in;
	return in;

}
struct Game {
	std::string Away;
	std::string Home;
	int away_score;
	int home_score;
	short week; //What week this game occurs in
	short num;
};

class Team {
  public:
		std::string Name;
    unsigned int off;
    unsigned int def;
		unsigned int wins = 0;
		unsigned int losses = 0;
		unsigned int ties = 0;
		float ppg = 0;
		float papg = 0;
    void Print() const;
		void Avg_SC(int pts_for, int pts_ag);
};

class TMS {
	public:
		bool Read();
		bool Edit_Team();
		bool In_Sch();
		bool Gen_Sch();
		bool Edit_Sch();
		bool Check_In(char op);
		Team *Find(const std::string &name);
		
		void Print_By_Offense() const;
		void Print_By_Defense() const;
		void Print_By_Name() const;
		void Print_By_Wins() const;
		void Print_Sch();
		
		void Play_Game(Game game);
		void Sim_Game(Game game);
		bool Play_Schedule(int flag);
		bool Edit_Game();

	protected:
		std::vector <Team *> TV;
		std::map <std::string, Team *> TM;
		Game *schedule; //Array. Columns = Games
		int games_played = 0; //TOTAL NUMBER OF GAMES PLAYED
		int weeks_played = 0;
		int nogms = 0; //TOTAL NUMBER OF GAMES across entire schedule (weeks * gamesperweek)
		int nowks = 0;
};

/* THE MAIN FUNCTION/.....
 *
 * STOP LOSING IT!!!!! */

int main() {
	TMS tms;
	char userSelect = '\0';
	int flag = false;
	printf("Welcome to my Football Simulator version alpha_0105.\nThe current simulator will only progress through an 'as-run' schedule, meaning you will specify the teams, ratings, and then the number of games.\nBefore each game, you will be asked who is playing.\nPlease keep in mind there are no tie games in College Football!\nPlease follow all instructions as they come.\n");
	while( (userSelect = PrintMenu()) != 'q') {
		if(userSelect == 't') { //input team data
			tms.Read();
			if(tms.Check_In(userSelect) == false) {
				while(tms.Check_In(userSelect) == false) {
					printf("You must input more than one team.\n");
					tms.Read();
				}
			}
			printf("All teams accounted for. Continuing operation.\n");
		}
		if(userSelect == 'y') { //edit team data
			if(tms.Check_In(userSelect) == true) {
				tms.Edit_Team();
			}
			else
				printf("You must input teams before you can edit a team.\n");
		}
		else if(userSelect == 's') {
			if(tms.Check_In(userSelect) == true) { //User-input schedule
				tms.In_Sch();
				tms.Print_Sch();
			}
			else
				printf("There must be teams to create a schedule.\n");
		}
		else if(userSelect == 'a') { //Generate a schedule (currently one home and one away game
			if(tms.Check_In(userSelect) == true) {
				tms.Gen_Sch();
				tms.Print_Sch();
			}
			else
				printf("There must be teams to create a schedule.\n");
		}
		else if(userSelect == 'd') { //Print schedule
			if(tms.Check_In(userSelect) == true) {
				tms.Print_Sch();
			}
			else
				printf("There must be a schedule to print a schedule.\n");
		}		
		else if(userSelect == 'p') { //Print standings
			if(tms.Check_In(userSelect) == true) {
				tms.Print_By_Wins();
			}
			else
				printf("There must be teams to print the standings\n.");
		}
		else if(userSelect == 'g') { //Play One Game
			if(tms.Check_In(userSelect) == true) {
				flag = 0;
				tms.Play_Schedule(flag);
			}
			else
				printf("There must a schedule to play a game.\n");
		}
		else if(userSelect == 'f') { //Play Full Schedule
			if(tms.Check_In(userSelect) == true) {
				flag = 1;
				tms.Play_Schedule(flag);
			}
			else
				printf("There must be a schedule to play a game.\n");
		}
		else if(userSelect == 'h') { //Sim One Game
			if(tms.Check_In(userSelect) == true) {
				flag = 2;
				tms.Play_Schedule(flag);
			}
			else
				printf("There must a schedule to play a game.\n");
		}
		else if(userSelect == 'r') { //Sim the schedule
			if(tms.Check_In(userSelect) == true) {
				flag = 3;
				tms.Play_Schedule(flag);
			}
			else
				printf("There must a schedule to play a game.\n");
		}
		else if(userSelect == 'e') { //Sim the schedule
			if(tms.Check_In(userSelect) == true) {
				//Edit Games
			}
			else
				printf("There must be teams and completed games to change a game's outcome.\n");
		}
		else {
			printf("Process did not gather your command. Please make sure to use one of the listed commands.");
		}
	}
	printf("Thank you for playing!\n");
  return 0;
}


/* TEAM class functions:::::
 *
 */
void Team::Print() const {
  printf("%s (%d-%d-%d); %d offense, %d defense; average score (%.2f-%.2f).\n", Name.c_str(), wins, losses, ties, off, def, ppg, papg);
}

void Team::Avg_SC(int pts_for, int pts_ag) {
	float curr_avg = ppg;
	int num = wins + losses + ties;
	curr_avg *= num;
	curr_avg += pts_for;
	curr_avg /= (num + 1);
	ppg = curr_avg;
	curr_avg = papg;
	curr_avg *= num;
	curr_avg += pts_ag;
	curr_avg /= (num + 1);
	papg = curr_avg;
}

/* TMS class functions::::::
 *
 */
bool TMS::Read() { //read in team data
	Team *t;
	int notms = 0;
	printf("\nHow many teams are there? > ");
	std::cin >> notms;
	if(notms < 2) {
		while( (!std::cin) || (notms < 2) ) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			printf("There must be more than one team. > ");
			std::cin >> notms;
		}
	}
	for(int i = 0; i < notms; ++i) {
		t = new Team;
		printf("\nTeam %d:\nPlease input the Name (string) Offensive Rating (1-99) and Defensive Rating (1-99).\n", i + 1);
		std::cin >> t->Name >> t->off >> t->def;
		while( (!std::cin) || (t->off > 99) || (t->def > 99) || (t->off < 1) || (t->def < 1) ) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			printf("ERROR: Make sure there are ONLY SPACES in between each input and ratings are on a scale of 1-99. > ");
			std::cin >> t->Name >> t->off >> t->def;
		}
		TV.push_back(t);
	}
	printf("\n");
	for(int i = 0; i < notms; ++i) TV[i]->Print();
	for(unsigned long i = 0; i < TV.size(); ++i) {
		t = TV[i];
		TM[t->Name] = t;
	}
	return true;
}

bool TMS::Edit_Team() {
	Team *a;
	std::string n;
	std::string old_name;
	int rtg;
	bool flag = false;
	char in = '\0';
	printf("Please input the NAME of the team you would like to edit. > ");
	std::cin >> n;
	a = Find(n);
	while(a == NULL) {
		printf("That team cannot be found. Please try again. > ");
		std::cin >> n;
		a = Find(n);
	}
	printf("Team has been located\n");
	a->Print();
	while(in != 'q') {
		printf("Team Editing Commands:\n"
			"n - Name\n"
			"o - Offensive Rating\n"
			"d - Defensive Rating\n"
			"q - Stop Editing\n"
			"Please select an option to change about this team. > ");
		std::cin >> in;
		if(in == 'n') {
			printf("Please input the new name for this team. > ");
			old_name = n;
			if(nogms > 0) 
				flag = true;
			std::cin >> n;
			while(!std::cin) {
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				printf("ERROR: Please input a one word Name. > ");
				std::cin >> n;
			}
			a->Name = n;
			if(flag == true) {
				for(int i = 0; i < nogms; ++i) {
					if(schedule[i].Away == old_name)
						schedule[i].Away = n;
					else if (schedule[i].Home == old_name)
						schedule[i].Home = n;
				}
			}
			flag = false;
		}
		else if (in == 'o') {
			printf("Please input the new offensive rating for this team (1-99). > ");
			std::cin >> rtg;
			while( (!std::cin) || (rtg < 1) || (rtg > 99) ) {
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				printf("ERROR: Please make sure ratings are on a scale of 1-99. > ");
				std::cin >> rtg;
			}
			a->off = rtg;
		}
		else if (in == 'd') {
			printf("Please input the new defensive rating for this team (1-99). > ");
			std::cin >> rtg;
			while( (!std::cin) || (rtg < 1) || (rtg > 99) ) {
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				printf("ERROR: Please make sure ratings are on a scale of 1-99. > ");
				std::cin >> rtg;
			}
			a->def = rtg;
		}
		else
			printf("Process did not gather your command. Please try again.");
	}
	return true;
}

bool TMS::In_Sch() {
	//CURRENTLY ONLY LOADS THE SCHEDULE AT PROGRAM START
	//WILL ADD FUNCTIONALITY TO EDIT DURING RUN
	Team *a;
	Game g;
	int gpw = 0;
	char rm = '\0';
	std::string home_team;
	std::string away_team;
	if(nogms > 0) {
		printf("It seems there is already an existing schedule. Would you like to replace this schedule (y/n)? > ");
		std::cin >> rm;
		if (rm == 'y') {
			delete[] schedule;
			printf("Would you like to delete any currently existing standings (y/n)? > ");
			std::cin >> rm;
			if(rm == 'y') {
				for(size_t i = 0; i < TV.size(); ++i) {
					TV[i]->wins = 0;
					TV[i]->losses = 0;
					TV[i]->ties = 0;
					TV[i]->ppg = 0;
					TV[i]->papg = 0;
				}
			}
		}
		else {
			printf("This program does not currently have the ability to add games to an existing schedule.\n");
			return true;
		}
	}
	printf("How many games are we playing per week? > ");
	std::cin >> gpw;
	printf("How many weeks are there? > ");
	std::cin >> nowks;
	while( (!std::cin) || (gpw < 1) || (nowks < 1) ) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			printf("ERROR: There must be atleast one week and one game. Please input the number of games per week. > ");
			std::cin >> gpw;
			printf("Please input the number of weeks. > ");
			std::cin >> nowks;
	}
	nogms = gpw * nowks;
	schedule = new Game[nogms];
	for(int j = 0; j < nowks; ++j) {
		for(int i = 0; i < gpw; ++i) {
			printf("Week %d, Game %d:\n", j + 1, i + 1);
			printf("Who is Away? > ");
			std::cin >> away_team;
			a = Find(away_team);
			while(a == NULL) {
				printf("That team cannot be found. Please try again. > ");
				std::cin >> away_team;
				a = Find(away_team);
			}
			printf("Who is Home? > ");
			std::cin >> home_team;
			a = Find(home_team);
			while(a == NULL) {
				printf("That team cannot be found. Please try again. > ");
				std::cin >> home_team;
				a = Find(home_team);
			}
			g.Away = away_team;
			g.Home = home_team;
			g.week = j;
			g.num = i;
			schedule[gpw * j + i] = g;
		}
	}
	return true;
}

bool TMS::Gen_Sch() { //currently only makes one week
	if(nogms > 0) { 
		delete[] schedule;
		printf("Schedule deleted.\n");
	}
	nogms = TV.size(); //number of games = number of teams
	Game g;
	std::string away_team;
	std::string home_team;
	printf("Generation function start.\n");
	schedule = new Game[nogms];
	for(int i = 0; i < nogms; ++i) { //for loop to build each game
		away_team = TV[i]->Name;
		if(i == (nogms - 1) ) 
			home_team = TV[0]->Name;
		else
			home_team = TV[i + 1]->Name;
		g.Away = away_team;
		g.Home = home_team;
		g.week = 0;
		g.num = i;
		schedule[i] = g;
	}
	return true;
}

bool TMS::Edit_Sch() {
	//Edit Future Games (who plays)
	//Edit Future Week (add/remove games from week)
	//Add/remove weeks and their respective games
	printf("***Schedule Editor***\n");
	Print_Sch();
	printf("***Schedule Edit Commands***\n"
			"a - Add another week to the schedule\n"
			"r - Remove a week from the schedule\n"


}
/*bool TMS::Gen_RR() {
	int nogms = TV

}*/

//TMS minor functions

bool TMS::Check_In(char op) { //Checks if user command is valid
	if( (op == 't') || (op == 's') || (op == 'p') || (op == 'a') ) {
		if(TV.size() > 0) 
			return true;
		else
			return false;
	}
	else if (op == 'e') {
		if(TV.size() == 0)
			return false;
		else if( (TV[0]->wins + TV[0]->losses + TV[0]->ties) <= 0)
			return false;
		else
			return true;
	}
	else {
		if(sizeof(schedule[0]) > 0) 
			return true;
		else
			return false;
	}
}
Team *TMS::Find(const std::string &name) { //Find correct team object based on name
	std::map<std::string, Team *>::iterator tit;
	tit = TM.find(name);
	if(tit == TM.end()) return NULL;
	return tit->second;
}

/* TMS print functions::::
 */

void TMS::Print_Sch() { //Prints the schedule by week
	printf("\nThis is our full season schedule!\n");//Number of games = sizeof(schedule)/sizeof(Game*)
	int wknum = 0;
	bool wkprt = false;
	for(short j = 0; j < nogms; ++j) {
		if(schedule[j].week > wknum) {
			wkprt = false;
			wknum++;
		}
		if(wkprt == false) {
			printf("Week %d\n", wknum + 1);
			wkprt = true;
		}
		printf("Game %d:\t%s at %s\n", schedule[j].num + 1, schedule[j].Away.c_str(), schedule[j].Home.c_str());
	}
}

void TMS::Print_By_Offense() const { //Prints team data by Offensive Rating
	Team *t;
	size_t i;
	std::multimap <double, Team *> m;
	std::multimap <double, Team *>::const_iterator mit;
	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->off, t));
	}
	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Defense() const { //Prints team data by Defensive Rating
	Team *t;
	size_t i;
	std::multimap <double, Team *> m;
	std::multimap <double, Team *>::const_iterator mit;
	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->def, t));
	}

	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Name() const { //Prints team data by Name
	std::map <std::string, Team *> m;
	std::map <std::string, Team *>::const_iterator mit;
	size_t i;
	Team *t;
	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m[t->Name] = t;
	}
	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Wins() const { //Prints team data by Wins
	std::multimap <int, Team *> m;
	std::multimap <int, Team *>::const_iterator mit;
	size_t i;
	Team *t;
	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->wins, t));
	}
	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

/* TMS class functions to play games
 */

void TMS::Play_Game(Game game) { //play a game dialogue 
	Team *a;
	Team *h;
	int home_score = 0;
	int away_score = 0;
	a = Find(game.Away);
	h = Find(game.Home);
	if(a == NULL)
		printf("The Away Team cannot be found.\n");
	else if(h == NULL) 
		printf("The Home Team cannot be found.\n");
	else {
		printf("Game %d: %s at %s\n", game.num, game.Away.c_str(), game.Home.c_str());
		a->Print();
		h->Print();
		printf("What is the final score (Format: Away-Home)? > ");
		std::cin >> away_score;
		std::cin.ignore(50, '-');
		std::cin >> home_score;
		game.home_score = home_score;
		game.away_score = away_score;
		if (away_score > home_score) {
			printf("Final Score: %s %d, %s %d\n", game.Away.c_str(), away_score, game.Home.c_str(), home_score);
			a->Avg_SC(away_score, home_score);
			h->Avg_SC(home_score, away_score);
			a->wins++;
			h->losses++;
		}
		else if (home_score > away_score) {	
			printf("Final Score: %s %d, %s %d\n", game.Home.c_str(), home_score, game.Away.c_str(), away_score);
			a->Avg_SC(away_score, home_score);
			h->Avg_SC(home_score, away_score);
			h->wins++;
			a->losses++;
		}
		else {
			printf("There was a tie!\n");
			h->ties++;
			a->ties++;
		}
	}
}

void TMS::Sim_Game(Game game) {
	int home_luck = rand() % 11;
	int away_luck = rand() & 11;
	Team *a = Find(game.Away);
	Team *h = Find(game.Home);
	int home_diff = h->off + home_luck - a->def - away_luck + 1;
	int away_diff = a->off + away_luck - h->def - home_luck;
	int home_score = 17.731 + 0.65656 * home_diff + 0.0015103 * home_diff * home_diff; //Quadratic Function based on the *difference* between offensive skill and defensive skill
	if(home_score < 1)
		home_score = 0;
	int away_score = 17.731 + 0.65656 * away_diff + 0.0015103 * away_diff * away_diff;
	if(away_score < 1)
		away_score = 0;
	game.home_score = home_score;
	game.away_score = away_score;
	printf("Game %d, ", game.num + 1);
	if (away_score > home_score) {
		printf("Final: %s %d, %s %d\n", game.Away.c_str(), away_score, game.Home.c_str(), home_score);
		a->Avg_SC(away_score, home_score);
		h->Avg_SC(home_score, away_score);
		a->wins++;
		h->losses++;
	}
	else if (home_score > away_score) {	
		printf("Final: %s %d, %s %d\n", game.Home.c_str(), home_score, game.Away.c_str(), away_score);
		a->Avg_SC(away_score, home_score);
		h->Avg_SC(home_score, away_score);
		h->wins++;
		a->losses++;
	}
	else {
		printf("%s and %s tie with a score of %d!\n", game.Away.c_str(), game.Home.c_str(), home_score);
		h->ties++;
		a->ties++;
	}
}

bool TMS::Play_Schedule(int flag) { //run through the schedule
	short wknum = weeks_played;
	bool wkprt = false;
	if(games_played == nogms) {
		printf("There are no more games on the current schedule.\n");
		return false;
	}
	if(flag == 0) { //Play full schedule
		for(int i = games_played; i < nogms; ++i) { 
			if(schedule[i].week > wknum) {
				++wknum;
				++weeks_played;
				wkprt = false;
			}
			if(wkprt == false) {
				wkprt = true;
				printf("Week %d:\n", wknum + 1);
			}
			Play_Game(schedule[i]);
			games_played++;
		}
		Print_By_Wins();
		games_played = 0;
	}

	if(flag == 1) { //Play one Game
		if(schedule[games_played].week > weeks_played) {
			++weeks_played;
			wkprt = false;
		}
		if(wkprt == false) {
			printf("Week %d:\n", wknum + 1);
			wkprt = true;
		}
		Play_Game(schedule[games_played]);
		++games_played;
	}

	if(flag == 2) { //Simulate one game
		if(schedule[games_played].week > weeks_played) {
			++weeks_played;
			wkprt = false;
		}
		if(wkprt == false) {
			printf("Week %d:\n", wknum + 1);
			wkprt = true;
		}
		Sim_Game(schedule[games_played]);
		games_played++;
	}
	
	if(flag == 3) { //Simulate Full schedule
		for(int i = games_played; i < nogms; ++i) { 
			if(schedule[i].week > wknum) {
				++wknum;
				++weeks_played;
				wkprt = false;
			}
			if(wkprt == false) {
				wkprt = true;
				printf("Week %d:\n", wknum + 1);
			}
			Sim_Game(schedule[i]);
			games_played++;
		}
		Print_By_Wins();
		games_played = 0;
	}
	return true;
}

bool TMS::Edit_Game() {
	int gpw = nogms / nowks;
	int wk = 0;
	int gm = 0;
	float tot = 0;
	char in = '\0';
	int flag = 0; //Prior to edit, away wins = 1, home wins = 0, tie = 2;
	Game g;
	Team *a;
	Team *h;
	printf("This Editor only edits one game at a time. Please enter the WEEK # followed by the GAME #. Week > ");
	std::cin >> wk;
	while( (!std::cin) || (wk == 0) || (wk > nowks + 1) ) {
		printf("ERROR: Please make sure to input a WEEK # that does not exceed the total number of weeks. Week > ");
		std::cin >> wk;
	}
	--wk;
	printf("Game > ");
	std::cin >> gm;
	while( (!std::cin) || (gm == 0) || (gm > nogms + 1) ) {
		printf("ERROR: Please make sure to input a GAME # that does not exceed the total number of games for the week. Game > ");
		std::cin >> gm;
	}
	--gm;
	gm = wk * gpw + gm;
	g = schedule[gm];
	if(g.away_score > g.home_score)
		flag = 1;
	else if(g.home_score > g.away_score)
		flag = 0;
	else
		flag = 2;
	a = Find(g.Away);
	h = Find(g.Home);
	printf("Here are the results for the Game you select:\n%s at %s, %d-%d\n", g.Away.c_str(), g.Home.c_str(), g.away_score, g.home_score);
	while(in != 'q') {
		printf("Please enter a command.\n"
			"a - Away Score\n"
			"h - Home Score\n"
			"q - Quit\n > ");
		std::cin >> in;
		if(in == 'a') {
			printf("Please enter the new score for the Away team. > ");
			std::cin >> wk;
			while ( (!std::cin) || (wk < 0) ) {
				printf("ERROR: That is not a valid score. Please try again. > ");
				std::cin >> wk;
			}
			tot = a->ppg * (a->wins + a->losses + a->ties);
			tot += (wk - g.away_score);
			a->ppg = tot / (a->wins + a->losses + a->ties);
			tot = h->papg * (h->wins + h->losses + h->ties);
			tot += (wk - g.away_score);
			h->papg = tot / (h->wins + h->losses + h->ties);
			g.away_score = wk;

		}
		if(in == 'h') {
			printf("Please enter the new score for the Home team. > ");
			std::cin >> gpw;
			while ( (!std::cin) || (gpw < 0) ) {
				printf("ERROR: That is not a valid score. Please try again. > ");
				std::cin >> gpw;
			}
			tot = h->ppg * (h->wins + h->losses + h->ties);
			tot += (gpw - g.home_score);
			h->ppg = tot / (h->wins + h->losses + h->ties);
			tot = a->papg * (a->wins + a->losses + a->ties);
			tot += (gpw - g.home_score);
			a->papg = tot / (a->wins + a->losses + a->ties);
			g.home_score = gpw;
		}
		else
			printf("The Process did not understand your command. Please try again.");
	}
	schedule[gm] = g;
	if(flag == 0) { //home won originally
		if(wk > gpw) {
			a->wins++;
			a->losses--;
			h->losses++;
			h->wins--;
		}
		if(wk == gpw) {
			a->ties++;
			a->losses--;
			h->ties++;
			h->wins--;
		}
	}
	if(flag == 1) { //away won orig
		if(gpw > wk) {
			a->losses++;
			a->wins--;
			h->wins++;
			h->losses--;
		}
		if(gpw == wk) {
			a->wins--;
			h->losses--;
			a->ties++;
			h->ties++;
		}
	}
	if(flag == 2) { //tie orig
		if(gpw > wk) { //home win
			h->wins++;
			h->ties--;
			a->losses++;
			a->ties--;
		}
		if(wk > gpw) {
			h->losses++;
			a->wins++;
			a->ties--;
			h->ties--;
		}
	}
	return true;
}
