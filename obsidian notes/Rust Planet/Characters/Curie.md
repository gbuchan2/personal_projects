Occupation: [[Engr]]
Age: 17 cycles
Height: 226.3 cm
Mass: 94 kg
Mate: Nik+
Father: Maris+
Mother: 
Children: none
Master: Hugo+
Apprentice(s): 
Birth Sector: [[5]]
Home Sector: [[13]]
### Backstory
Curie was born on Sector 5 in the midst of a Hydroponics strike. Her father Maris, the Head Engr for Sector 5 at the time, purposely disabled life support and massacred many Agras in the Sector's Hydroponics Farm. Maris was subsequently barred from the Collective and [[Humanity#Recycle|Recycle]]d, but he had singularly ended the strike and potentially saved many of his neighbors, as several were starving. Her mother fell into a catatonic state and failed to perform her duties as a laborer, resulting in her own recycling. This left Curie alone at the age of 3, joining a nearby family of laborers (Tani and Nichol). She holds great disdain for the Union, blaming them for her father's death.

In the memory of her father, Curie strived to become a Head Engr of her own. Her ranking in the 97th percentile in aptitude allowed her to choose her path.  At the age of 9, the Collective invited her and carefully placed her with Hugo in Sector 5, a loyal yet ineffectual member. At the same time, her adoptive brother Nik received an invitation a year later, when his scores lifted him from simple laborer to Engr. 

When mating came, Curie and Nik made a good fit genetically, and Hugo leveraged the local magistrate to let it be. Curie and Nik responded poorly to this news, but they each accepted their placement in Sector 13 with great anticipation. They have been station Engrs on Sector 13 for a little over two years, Curie specializing in power distribution and Nik in telecommunications. 

### Story
Curie's story opens with Sector 13 crashing on the planet's surface, but her involvement begins during the [[Humanity#The Breaking|Breaking]]. Her and Nik responded to multiple alarms in the star-ward hull. Nik found several ruptures and quickly made the decision to seal off part of Sector 13. In the ensuing chaos, Curie arrived just in time to watch Nik be pulled out into the vacuum. 

On the surface, Curie joins a small scouting team in search of a sustainable water supply. On the adventure, the team are the first to encounter the [[Babalu]]s, local humans living on the planet. They later encounter the [[Sicuno]]s and save many Babalus in the ensuing conflict. They are led back to a Babalu camp and given food and taught what things to eat and not eat. This information is brought back to Sector 13, only for Curie to find the Sicunos assaulting the small fortification. Curie attempts to save her people, by overloading the Fissile Core, but she dies in the attempt. Much of Sector 13 is taken captive. 