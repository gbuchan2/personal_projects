//DLIST.CPP
//DLIST class definitions
//Graham Buchanan
//27 October 2021

#include "dlist.hpp"
#include <iostream>

/*This Program builds a working version of a doubly-linked list or "dlist" as the Class name implies.
 * It is driven by the "dlist_editor" program.
 * This "List" has a working network of pointers and methods on those nodes, including:
 * Push_Back and Push_Front
 * Pop_Front and Pop_Back
 * Insert_Before and Insert_After
 * Erase and Clear
 * This List allows for the quick insertion, deletion, and storage of strings onto a simple data structure.
 * It holds a primary "sentinel" node that acts as a header for the actual storage array.*/







/***DNODE CLASS***/

//return the next node
Dnode* Dnode::Next() {
	return flink;
}

//return the previous node
Dnode* Dnode::Prev() {
	return blink;
}




/***DLIST CLASS***/

/*CONSTRUCTORS*/

//Constructor
//create new node and assign its variables to itself
Dlist::Dlist() {
	size = 0;
	Dnode *a = new Dnode;
	this->sentinel = a;
	this->sentinel->flink = a;
	this->sentinel->blink = a;
}

//Copy Constructor
//Create a new node, assign variables, then use operator=
Dlist::Dlist(const Dlist &d) {

	size = 0;
	Dnode *a = new Dnode;
	this->sentinel = a;
	this->sentinel->flink = a;
	this->sentinel->blink = a;
	*this = d;
}

//Assignment Overload
//declare Dnode, then iterate through given list and push onto this list
Dlist& Dlist::operator= (const Dlist &d) {

	Dnode *t;
	Clear();
	for(t = d.Begin(); t != d.End(); t = t->Next()) Push_Back(t->s);
	return *this;
}

//Destructor
//Call clear, delete sentinel
Dlist::~Dlist() {
	Clear();
	delete this->sentinel;
}




/*Class Methods*/

//Clear
//use iterator
//set a size integer to maintain size value (changes with Erase())
//Erase all nodes
void Dlist::Clear() {

	size_t i = 0;
	size_t s = size; //Need to memorize the size

	if(!Empty()) {
		for(i = 0; i < s; ++i) {
			Erase(sentinel->Next());		
		}
	}
}

//Empty
//If size is 0, true
bool Dlist::Empty() const {
	return (size == 0);
}

//Size
//return size
size_t Dlist::Size() const {

	return size;

}

//Push_Front
//Add to list from front
//Call Insert Before the first node
void Dlist::Push_Front(const std::string &s) {

	Insert_Before(s, Begin());

}

//Push_Back
//Add to list from back
//Call Insert Before on sentinel
void Dlist::Push_Back(const std::string &s) {

	Insert_Before(s, End());

}

//Pop_Front
//Remove from list from the front
//Call Erase on first node
std::string Dlist::Pop_Front() {

	std::string s = "";
	s = Begin()->s;
	Erase(this->Begin());
	return s;

}

//Pop_Back
//Remove from list on the back
//Call Erase on the last node
std::string Dlist::Pop_Back() {

	std::string s = "";
	s = Rbegin()->s;
	Erase(this->Rbegin());

	return s;

}




/*Iterators*/

//Begin
//return the first node
Dnode* Dlist::Begin() const {
	return (sentinel->Next());
}

//End
//return the sentinel
Dnode* Dlist::End() const {
	return sentinel;
}

//Rbegin
//return the last node
Dnode* Dlist::Rbegin() const {
	return sentinel->Prev();
}

//Rend
//return the sentinel
Dnode* Dlist::Rend() const {
	return sentinel;
}




/*Methods with Iterators*/

//Insert_Before
//declare a new node
//assign the relevant pointers for insertion
void Dlist::Insert_Before(const std::string &s, Dnode *n) {

	Dnode *a = new Dnode;
	a->s = s;
	a->flink = n;
	a->blink = (n->Prev());
	n->Prev()->flink = a;
	n->blink = a;
	size++;
}

//Insert_After
//same as Insert_Before
void Dlist::Insert_After(const std::string &s, Dnode *n) {

	Dnode *a = new Dnode;
	a->s = s;
	a->blink = n;
	a->flink = n->Next();
	n->Next()->blink = a;
	n->flink = a;
	size++;

}

//Erase
//ONLY EXECUTE IF NOT ON SENTINEL
//assign relevant pointers
//delete the pointer
void Dlist::Erase(Dnode *n) {

	if(n != sentinel) {
		n->Prev()->flink = n->Next();
		n->Next()->blink = n->Prev();
		delete n;
		size--;
	}

}
