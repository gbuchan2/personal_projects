//ver0.cpp - LAYOUT
//bitmap...reading, writing bitmap files
//Graham Buchanan
//12 February 2021

#include <cstdio>
#include <cstdint>
#pragma pack(push, 2)

struct BitmapFileHeader {
	uint16_t type; //type of file, Windows bitmap BM
	uint32_t size; //entire size of file, including headers
	uint16_t reseved; //reserved for future expansion
	uint16_t reserved; //reserved for future expansion
	uint32_t offest; //tells the offest from top of file to where pixel data is stored
};

struct BitmapInfoHeader {
	uint32_t size; //size of info header [should be 40]
	uint32_t width; //width of the picture
	uint32_t height; //height of the picture
	uint16_t planes; //number of layers, test pics = 1
	uint16_t bitcount; //number of bits per pixel, should be 24
	uint32_t compression; //compression used, there is none, should be zero
	uint32_t imagesize; //size of image, may be zero
	uint32_t x_pixels_per_meter; //number of pixels per meter horizontally
	uint32_t y_pixels_per_meter; //number of pixels per meter vertically
	uint32_t color_used; //color index used, should be 0
	uint32_t color_important; //should be 0
};

#pragma pack(pop)

struct Pixel {
	uint8_t red; //amount of red
	uint8_t blue; //amount of blue
	uint8_t green; //amount of green
};

//invert = 255 - pixel color value

int main() {

	FILE *fl;
	fl = fopen("test.bmp", "wb");
	if(nullptr == fl) {
		printf("Could not open file.\n");
		return -1;
	}
	fclose(fl);
	return 0;
}

