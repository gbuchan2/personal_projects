//ALPHA 1.02
//Preparing "Game Simulations"
//College Football League Simulator

#include <iostream>
#include <vector>
#include <string>
#include <cstdio>
#include <set>
#include <map>

class Team {
  public:
		std::string Name;
    int off;
    int def;
    void Print() const;
};

class TMS {
	public:
		bool Read();
		const Team *Find(const std::string &name) const;
		void Print_By_Offense() const;
		void Print_By_Defense() const;
		void Print_By_Name() const;
	protected:
		std::vector <Team *> TV;
		std::map <std::string, Team *> TM;
};

int main() {
	TMS tms;
	const Team *t;
//	const Team *t;

	std::string team_name;
	tms.Read();
	printf("Enter a team name. >");
	std::cin >> team_name;
	t = tms.Find(team_name);
	if(t == NULL)
		printf("\nThere is no team of that name.\n");
	else
		t->Print();
	tms.Print_By_Name();
  return 0;
}

void Team::Print() const {
  printf("%s: %d off, %d def.\n", Name.c_str(), off, def);
}

bool TMS::Read() {
	Team *t;
	int notms = 0;
	printf("How many teams are there? >");
	std::cin >> notms;
	for(int i = 0; i < notms; ++i) {
		t = new Team;
		printf("\nPlease input the Name, Offensive Rating, and Defensive Rating.\n");
		std::cin >> t->Name >> t->off >> t->def;
		TV.push_back(t);
	}
	for(int i = 0; i < notms; ++i) TV[i]->Print();

	for(unsigned long i = 0; i < TV.size(); ++i) {
		t = TV[i];
		TM[t->Name] = t;
	}

	return true;
}

const Team *TMS::Find(const std::string &name) const {
	std::map<std::string, Team *>::const_iterator tit;
	tit = TM.find(name);
	if(tit == TM.end()) return NULL;
	return tit->second;
}

void TMS::Print_By_Offense() const {

	Team *t;
	size_t i;
	std::multimap <double, Team *> m;
	std::multimap <double, Team *>::const_iterator mit;

	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->off, t));
	}

	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Defense() const {

	Team *t;
	size_t i;
	std::multimap <double, Team *> m;
	std::multimap <double, Team *>::const_iterator mit;

	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->def, t));
	}

	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Name() const {

	std::map <std::string, Team *> m;
	std::map <std::string, Team *>::const_iterator mit;
	size_t i;
	Team *t;

	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m[t->Name] = t;
	}

	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}
