###FBSIM VERISON ALPHA 24###
##GRAHAM BUCHANAN##


# CHANGELOG #
# 09 SEPT 2024 - CREATION#

Major Source Files:
- master.cpp
	- is the 'tie' for the entire program, contains main()
- league.cpp
	- contains defines for league class
	- includes team.hpp
- team.cpp
  - contains defines for team class
	- includes game.hpp
- game.cpp
  - contains defines for game class

Master drives the fbsim game
League contains all data, all actionables
Team contains all team-related data and members
Game contains all game-related data and members
