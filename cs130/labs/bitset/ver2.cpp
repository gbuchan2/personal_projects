//lab.cpp
//BITSET LAB -- USER INPUT
//Graham Buchanan
//30 Jan 2021







/* MAKING A NOTE FOR THE FUTURE>>>>>>>>>>
 *
 *
 *
 *
 * MAKE THE CODE LOOK BETTER AND SHRINK UP THE SPACE
 *
 *
 *
 *
 *
 * FINISH FUNCTIONS IN MAIN!!!!!!!!
 *
 * MEMBER FUNCTIONS WORK......
 *
 * vector of ints >> TOBINARY converts the INT. 
 * SET and CLEAR CHANGE THE VALUE OF THE INT itself.
 *
*/
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

class BITSET {
	private:
	std::vector <int> set;

	public:
	BITSET();
	~BITSET();
	std::string Test(unsigned int) const;
	void Set(unsigned int);
	void Clear(unsigned int);
	int GetNumSets() const;
	std::string GetSet(unsigned int a) const;
};

std::string ToBinary(int val, int spa) {
	std::string c = "";
	int a = 1;
	for(unsigned int j = 0; j < 39; ++j) { 
		if( a % (spa + 1) == 0)
			c += ' ';
		else {
			if(val % 2 == 0)
				c += '0';
			else
				c += '1';
			val = val / 2;
		}
		++a;
	}

	val = 0;

	std::string s = std::string(c.rbegin(), c.rend());
	return s;
}

int main(int argc, char ** argv) {

	std::cout << "Program Run." << std::endl;

	BITSET z;
		char in = '\0';
		unsigned int v = 0;
	
	while(in != 'q' || std::cin.eof()) {
		std::cout << '>';
		std::cin >> in;
		if(in == 'q' || std::cin.eof())
			exit(0);
		if( !(in == 'n') )
			std::cin >> v;

		switch(in) {
			case 't':
				{
					std::cout << z.Test(v) << std::endl;
					break;
				}
			case 'g':
				{
					std::cout << z.GetSet(v) << std::endl;
					break;
				}
			case 's':
				{
					z.Set(v);
					break;
				}
			case 'c':
				{
					z.Clear(v);
					break;
				}
			case 'n':
				{
					std::cout << z.GetNumSets() << std::endl;
					break;
				}
			default:
				std::cout << "Please enter a valid input" << std::endl;
				break;

			std::cin.clear();
			in = '\0';
		}
	} 


	/*	std::cout << ">";
		std::cin >> in >> v;
		std::cout << in << ' ' << v << std::endl;
*/
		
		
	//	b = ( (b >> (n)) & 1); TEST
	//	b |= (1 << (n) ); SET
	//	b = b & ~(1 << n); CLEAR



	return 0;
}

// CLASS FUNCTION DEFINES

BITSET::BITSET() {
	set.resize(1, 0);
}

BITSET::~BITSET() {}

std::string BITSET::Test(unsigned int a) const {
	//goes to INT location and return true (1) or false (0) based on the value of that bit
	int b = set[ (a / 32) ];
	b = ( (b >> (a)) & 1);
	if( !(b == 0) ) 
		return "1";
	else
		return "0";
}


void BITSET::Set(unsigned int a) {
	//goes to INT location and sets bit to 1
	int c = set[ (a / 32) ];
	c |= (1 << (a) );
	if((a / 32) > (set.size() - 1))
		set.resize( ((a / 32) + 1), 0);
	set[ (a / 32) ] = c;
}


void BITSET::Clear(unsigned int a) {
	//goes to INT locaiton and sets bit to 0
	int d = set[ (a / 32) ];
	d = d & ~(1 << a);
	set[ (a / 32) ] = d;

	for(unsigned int j = set.size(); j > 0; --j) {
		if(set[j] == 0)
			set.resize(j);
		else
			break;
	}

}


int BITSET::GetNumSets() const {
	//returns the number of sets in the vector as integer
	return set.size();
}


std::string BITSET::GetSet(unsigned int a) const {
	//returns the 4-byte integer of the particular set...return 0 if set does not exist
	if(a > (set.size() - 1) )
		return "0";
	else
		return ToBinary(set[a], 4);
}
