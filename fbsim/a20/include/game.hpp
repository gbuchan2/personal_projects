//GAME.HPP
//Declaration of the Game Class
//Graham Buchanan

/***Update Log***
 *  
 *  19 November 2021 - Creation and Declaration
 *
 *
 */

#include "team.hpp"
#include <vector>
#include <list>

class Game {
	public:
		Game(Team *away, Team *home, std::string &location const); //Constructor
		void Play_Game();
		void Edit_Game();
		int score_generator();
		void Print_Data() const;
		
	protected:
		uint16_t score = 0; //0-7 away, 8-15 home
		char winner = '\0'; //a - away, h - home, u - undecided, t - tie
		Team *a; //Away Team
		Team *h; //Home Team
		std::string loc = "";
};
