//Lab6 - fraction.cpp
//manages a fraction, where num and denom are pos. ints
//Graham Buchanan
//06 October 2021

#include "fraction.hpp"
#include <iostream>
#include <cstdio>
#include <vector>

//Clear Fraction
//Purpose: Clear numerator & denominator sets
void Fraction::Clear() {

	numerator.clear();
	denominator.clear();
}

//Add to Fraction
//Parameters: integer n
//Purpose: Inserts an integer into the numerator set
bool Fraction::Multiply_Number(int n) {

	//QUESTION:::ADDING numbers to denominator when there is no num?
	if(n <= 0) return false;
	if(n == 1) return true;
	else if((denominator.size() != 0) && (denominator.find(n) != denominator.end())) denominator.erase(denominator.find(n));
	else numerator.insert(n);
	return true;
}

//Add to Fraction
//Parameters: integer n
//Purpose: Inserts an integer into the denominator set
bool Fraction::Divide_Number(int n) {

	if(n <= 0) return false;
	if(n == 1) return true;
	else if((numerator.size() != 0) && (numerator.find(n) != numerator.end())) numerator.erase(numerator.find(n));
	else denominator.insert(n);
	return true;

}

//Multiply by the Factorial
//Parameters: integer n
//Purpose: Inserts the Factorial of an integer into the numerator
bool Fraction::Multiply_Factorial(int n) {

	int i = 0;

	//Errors are already handled by Multply/Divide
	if(n <= 0) return false;
	if(n == 1) return true;
	else {
		for(i = 2; i <= n; ++i) {
			if(!(Multiply_Number(i))) return false;
			}
	}

	return true;
}

//Divide by the Factorial
//Parameters: integer n
//Purpose: Inserts the Factorial of an integer into the denominator
bool Fraction::Divide_Factorial(int n) {


	int i = 0;
	//Errors are already handled by Multply/Divide
	if(n <= 0) return false;
	if(n == 1) return true;
	else {
		for(i = 2; i <= n; ++i) {
			if(!(Divide_Number(i))) return false;
		}
	}
	return true;
}

//Errors are already handled by Multiply/Divide
bool Fraction::Multiply_Binom(int n, int k) {

	if(n <= 0 || k <= 0);
	else if(k > n) return false;
	else if(!Multiply_Factorial(n)) return false;
	else if(!Divide_Factorial(k)) return false;
	else if(!Divide_Factorial(n - k)) return false;

	return true;

}

bool Fraction::Divide_Binom(int n, int k) {

	if(n <= 0 || k <= 0);
	else if(k > n) return false;
	else if(!Multiply_Factorial(k)) return false;
	else if(!Multiply_Factorial(n - k)) return false;
	else if(!Divide_Factorial(n)) return false;
	return true;
}

//Invert Fraction
//Purpose: Swap the numerator and denominator
void Fraction::Invert() {
	std::vector<int> n, d;
	std::multiset<int>::const_iterator it;
	size_t i = 0;

	for(it = numerator.begin(); it != numerator.end(); ++it) n.push_back(*it);
	for(it = denominator.begin(); it != denominator.end(); ++it) d.push_back(*it);
	Clear();
	for(i = 0; i < n.size(); ++i) denominator.insert(n[i]);
	for(i = 0; i < d.size(); ++i) numerator.insert(d[i]);
}

//Print Fractor
//Purpose: Prints the numerator as products and the denominator as quotients
void Fraction::Print() const {
	std::multiset<int>::const_iterator it;

	if(numerator.empty()) std::cout << 1;
	else {
		for(it = numerator.begin(); it != numerator.end(); ++it) {
			if(it != numerator.begin()) std::cout << " * ";
			std::cout << *it;
		}
	}
	for(it = denominator.begin(); it != denominator.end(); ++it) {
		std::cout << " / " << *it;
	}
	std::cout << std::endl;
}

//Calculating the Fraction
//Purpose: Actually perform multiplcation and division
//to calculate the value of the fraction
double Fraction::Calculate_Product() const {

	double r = 1;
	std::multiset<int>::const_iterator it;
	if(denominator.empty());
	else {
		for(it = denominator.begin(); it != denominator.end(); ++it) {
			r /= *it;
		}
	}
	if(numerator.empty());
	else {
		for(it = numerator.begin(); it != numerator.end(); ++it) {
			r *= *it;
		}
	}
	return r;
}
