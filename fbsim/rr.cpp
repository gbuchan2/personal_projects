#include <vector>
#include <cstdio>
#include <algorithm>
#include <iostream>
	const int BYE = -1;
int Odd(int num_teams);
int Even(int num_teams);
void RotateArray(std::vector<int> &teams, bool flag);

int main() {

int num_teams = 0;

printf("How many teams? > ");
std::cin >> num_teams;
Odd(num_teams);




return 0;
}

int Odd(int num_teams) {
	bool flag = false;
	int n2 = (int)((num_teams)/2);
	std::vector<unsigned int> results;
	int tmp = 0;
	std::vector<int> teams;
	for(int i = 0; i < num_teams; ++i) 
	{
		teams.push_back(i+1);
	}
if((num_teams % 2) ==1)
{
	tmp = num_teams;
	flag = true;
}
else
	tmp = num_teams-1;
	for(int r = 0; r < tmp; ++r) {

		for(int i = 0; i < n2; ++i) {
		unsigned char t1 = teams[n2-i-1] & 0xff;
		unsigned char t2 = teams[n2+i] & 0xff;
		results.push_back ((r << 16) | (t1 << 8) | (t2));
	}
		if(flag == true) {

		unsigned char t1 = teams[num_teams-1] & 0xff;
		unsigned char t2 = 0xf;
		results.push_back ((r << 16) | (t1 << 8) | (t2));
		}
	RotateArray(teams, flag);
	}

	for(int i = 0; i < num_teams; ++i) {
		printf("%x\n", teams[i]);
	}

	for(int i = 0; i < results.size(); ++i) {
		if( (results[i] & 0xff) == 0xf)
			printf("Round %x, Team %x has a BYE!\n", (results[i] >> 16), ((results[i] >> 8) & 0xff));
		else
			printf("Round %x, Team 1: %x, Team 2: %x\n", (results[i] >> 16), ((results[i] >> 8) & 0xff), ((results[i]) & 0xff));
	}

return 1;
}

void RotateArray(std::vector< int> &teams, bool flag) {
	if(flag == true)
	std::rotate(teams.begin(),teams.begin()+teams.size()-1, teams.end());
	else
		std::rotate(teams.begin()+1, teams.begin()+teams.size()-1, teams.end());
}
