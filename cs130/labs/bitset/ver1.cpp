//lab.cpp
//BITSET LAB -- USER INPUT and manipulation
//Graham Buchanan
//30 Jan 2021







/* MAKING A NOTE FOR THE FUTURE>>>>>>>>>>
 *
 *
 *
 *
 * MAKE THE CODE LOOK BETTER AND SHRINK UP THE SPACE
 *
 *
 *
 *
 *
 * FINISH FUNCTIONS IN MAIN!!!!!!!!
 *
 * MEMBER FUNCTIONS WORK......
 *
 * vector of ints >> TOBINARY converts the INT. 
 * SET and CLEAR CHANGE THE VALUE OF THE INT itself.
 *
*/
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

class BITSET {
	private:
	std::vector <int> set;

	public:
	BITSET();
	std::string Test(int) const;
	void Set(int);
	void Clear(int);
	int GetNumSets() const;
	std::string GetSet(int a) const;
};

std::string ToBinary(int val, int spa) {
	std::string c = "";
	int a = 1;
	for(unsigned int j = 0; j < 39; ++j) { 
		if( a % (spa + 1) == 0)
			c += ' ';
		else {
			if(val % 2 == 0)
				c += '0';
			else
				c += '1';
			val = val / 2;
		}
		++a;
	}

	val = 0;

	std::string s = std::string(c.rbegin(), c.rend());
	return s;
}

int main(int argc, char ** argv) {

	std::cout << "Program Run." << std::endl;

	BITSET z;

	/*	int v = 0;
		int s = 0;
		std::cout << ">";
		std::cin >> v;
		std::cout << ">";
		std::cin >> s;
		*/
	//	b = ( (b >> (n)) & 1); TEST
	//	b |= (1 << (n) ); SET
	//	b = b & ~(1 << n); CLEAR

	std::cout << z.Test(0) << std::endl;
	z.Set(0);
	std::cout << z.Test(0) << std::endl;
	std::cout << z.GetSet(0) << std::endl;
	return 0;
}



BITSET::BITSET() {
	set.resize(1, 0);
}

std::string BITSET::Test(int a) const {
	//goes to INT location and return true (1) or false (0) based on the value of that bit
	int b = set[ (a / 32) ];
	b = ( (b >> (a)) & 1);
	if( !(b == 0) ) 
		return "1";
	else
		return "0";
}


void BITSET::Set(int a) {
	//goes to INT location and sets bit to 1
	int c = set[ (a / 32) ];
	c |= (1 << (a) );
	set[ (a / 32) ] = c;
}


void BITSET::Clear(int a) {
	//goes to INT locaiton and sets bit to 0
	int d = set[ (a / 32) ];
	d = d & ~(1 << a);
}


int BITSET::GetNumSets() const {
	//returns the number of sets in the vector as integer
	return set.size();
}


std::string BITSET::GetSet(int a) const {
	//returns the 4-byte integer of the particular set...return 0 if set does not exist
	return ToBinary(set[ (a / 32) ], 4);
}
