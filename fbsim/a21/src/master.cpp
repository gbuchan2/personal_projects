//MASTER.HPP
//Controls all background interface operations
//Graham Buchanan
//03 June 2022

#include <vector>
#include <map>
#include <set>
#include <list>
#include <deque>
#include <stdint.h>
#include <cstdio>
#include <iostream>
#include <fstream>

bool Enter_Teams() {

	size_t i, j, k;
	int a, b, c, notms, nodivs;
	uint64_t r;
	std::string s, in, str, loc, name, abr;
	Team *t;

	Delete_Teams(); //Clear out Team Map and Team Vector
	Delete_Division(); //Clear out Division Map and Vector
	
	/* All Team Data:
	 * Abbreviation
	 * Location
	 * Name 
	 * id (uint32)
	 * off_pass (uint8)
	 * off_run (uint8)
	 * off_line (uint8)
	 * def_pass (uint8)
	 * def_rush (uint8)
	 * def_line (uint8)
	 */

	//Enter One Team Manually
	printf("Please enter the Location. > ");
	std::getline(std::cin, in);
	while(!(std::cin) || (in.empty()) || (in.length > 30)) {
		std::cin.clear();
		std::cin.ignore();
		printf("ERROR. The Location must be between 1 and 30 characters long. > ");
		std::getline(std::cin, in);
	}
	loc = in;
	printf("Please enter the Name. > ");
	std::getline(std::cin, in);
	while(!(std::cin) || (in.empty()) || (in.length > 30) || (TS.find(loc + in) != TS.end())) {
		std::cin.clear();
		std::cin.ignore();
		printf("ERROR. The Name must be between 1 and 30 characters long, and the Location-Name Combination must be unique. > ");
		std::getline(std::cin, in);
	}
	name = in;
	TS.insert(loc + name);
	printf("Please enter the Team's Abbreviation. > ");
	std::cin >> in;
	while(!(std::cin) || (in.empty()) || (in.length != 3) || (TM.find(in) != TM.end())) {
		std::cin.clear();
		std::cin.ignore();
		printf("ERROR. The Abbreviation must be 3 characters long and unique. > ");
		std::cin >> in;
	}
	abr = in;

	//Enter Rating
	printf("Please enter the offensive ratings in the following order, separated by spaces and on the scale of 0-255:\n\tPassing\n\tRushing\n\tBlocking\n > ");
	std::cin >> a >> b >> c;
	while(!(std::cin) || (a < 0) || (b < 0) || (c < 0) || (a > 255) || (b > 255) || (c > 255)) {
		std::cin.clear();
		std::cin.ignore();
		printf("ERROR. Please try again. > ");
		std::cin >> a >> b >> c;
	}
	//r = PASS|RUSH|BLOCKING|COVERAGE|TACKLING|SHEDDING
	r = 0;
	r = (a & 0xff) | ((b << 8) & 0xff00) | ((c << 16) & 0xff0000);
	printf("Please enter the defensive ratings in the following order, separated by spaces and on the scale of 0-255:\n\tPass Coverage\n\tRush Defense\n\tBlock Shedding\n > ");
	std::cin >> a >> b >> c;
	while(!(std::cin) || (a < 0) || (b < 0) || (c < 0) || (a > 255) || (b > 255) || (c > 255)) {
		std::cin.clear();
		std::cin.ignore();
		printf("ERROR. Please try again. > ");
		std::cin >> a >> b >> c;
	}
	r |= (((a << 24) & 0xff000000) | ((b << 32) & 0xff00000000) | ((c << 40) & 0xff0000000000));

	//Now, we have all data for Team creation
	t = new Team(name, loc, abr, notms, r);

	TV.push_back(t);
	TM.insert(std::pair<std::string, Team *>(abr, t));

	return false;
}
