//TEAM.HPP
//All Team Data
//Graham Buchanan
//11 March 2022

#include <string>
#include <vector>
#include <stdint.h>
#include <cstdio>

class Division;
class Schedule;

class Team {

	class friend Master;

	public:
		Team(); //Base Constructor
		Team(std::string &n, std::string &l, std::string &a, uint32_t &id, uint64_t &r); //Sets data directly

		bool Set_Data(); //Sets name, location and ratings, and id
		void Print(); //Print all data

	protected:
		
		//Base Data of the Team
		std::string name;
		std::string location;
		std::string abbreviation;
		uint32_t id;

		uint8_t off_pass; //Offensive Passing Rating
		uint8_t off_run; //Offensive Rushing Rating
		uint8_t off_line; //Offensive Line Rating
		uint8_t off_rtg;

		uint8_t def_pass; //Defensive Coverage Rating
		uint8_t def_rush; //Defensive Run-Stopping Rating
		uint8_t def_line; //Defensive Line Rating
		uint8_t deg_rtg;

		uint8_t off_pts; //Total Points Scored
		uint8_t def_pts; //Total Points Against
		uint8_t wins;
		uint8_t losses;
		uint8_t ties;
		
		//Dynamic Data of the Team
		Schedule *sch; //Schedule Pointer
		Division *conf; //Conference Pointer
		Division *div; //Division (of conf) Pointer
}
