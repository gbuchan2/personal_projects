//MoonGlow.cpp
//reading names and grades
//Graham Buchanan
//29 Aug 2021

#include <iostream>

int main() {
	std::string s = ""; //in
	std::string n = ""; //name_reg
	int i = 0; //it
	double a = 0; //av
	double d = 0; //sc
	double t = 0; //total_reg
		
		/* Here comes the annoying part of this program (figuring out the loop and the if statements took a whole 2 hours):
		 * Check for integer first, and assume fails are a string (Which works based on lab inputs)
		 * BIGGEST ISSUE: using cin.clear and checking for EOF; can't put these at end of loop (or in cond)
		 * After assuming input is a string (if fail the first check), find out what the string is and proceed*/

	while(1) {

		if(std::cin >> d) {
			t += d;
		} 
		else {
			if(std::cin.eof()) {
				std::cout << n << ' ' << t << std::endl;
				break;
			}
			std::cin.clear();
			std::cin >> s;
			if(s == "NAME") {
				std::cin >> n;
			}
		/* For the AVERAGE "function", use the tricky technique of auto-counting the first integer (if present)
		 * ^^^This got me from a 95 to a 100 on the gradescript!
		 * ->For every integer read from cin, count with iterator, find the mean, more totaling
		*  Thank you for reading my Program...enjoy*/

			else if(s == "AVERAGE") {
				if(std::cin >> d) {
					++i;
					a += d;
					while(std::cin >> d) {
						++i;
						a += d;
					}
					a /= i;
					i = 0;
					t += a;
					a = 0;
				}
			}
		}
	}
	return 0;
}
