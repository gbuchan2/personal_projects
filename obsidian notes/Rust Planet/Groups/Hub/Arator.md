The Aratorship comprises the bureaucracy and law enforcement aboard the Hub. It consists of: magistrates who oversee each sector specifically to ensure law and order; security officers who provide a tactical asset to arrest individuals and maintain the rebellious nature of each sector; and administrators who work to track and regulate all the daily matters aboard the Hub, from rations to mandatory testing. All three roles work synchronously under the stewardship of the Head Magistrate who coordinates trade and resource allocations for each sector, as well as sits upon the [[Humanity#The Grand Treaty|Grand Treaty]] as an impartial Chairman. To many, the Aratorship represents the worst aspects of hedonism and gluttony in humanity, but in truth, having one extra set of uniforms is a very small advantage compared to those with only one. Arators are trained as leaders first and foremost, intended to save the remnants of humanity both from foreign threats and themselves. 

### Guild
Name: "The Aratorship"
Emblem: Sword and Shield
Members: 72
Leader: [[Linel]]
Treaty Seats: 6

### Occupations:
- Magistrate - Regularly patrols assigned sector and administers justice
- Administrator - Manages sector incidentals and tracks all records
- Security Officer - Enforces laws and justice where needed

### Stations:
- Barracks: [[7]], [[10]], [[16]], [[21]]
- The Complex: [[4]]

### Notables:
- [[Odeicon]]
- [[Gerohl]]
