#include <string>
#include <vector>
#include <list>
#include <cmath>
#include <algorithm>
#include <map>
#include <set>
#include <iostream>
#include <sstream>
#include <cstdio>
#include <cstdlib>
using namespace std;

/* This is an overly convoluted program.  What I do is create a random
   tree by having node i choose its parent randomly from 0 to i-1.
   Then I permute the indicies, and do a DFS from permuted node 0.  The
   reason is that the graph construction favors node 0 as being the one
   that you end up removing, so this randomizes the root.  */

void DFS(int n, const vector < vector <int> > &adj, 
                vector <int> &parents, 
                const vector <int> &permute_f)
{
  int i;
  int perm_from, perm_to, to;
  vector <int> top;

  perm_from = permute_f[n];
  // printf("DFS.  N=%d  perm_from = %d\n", n, perm_from);

  for (i = 0; i < adj[n].size(); i++) {
    to = adj[n][i];
    perm_to = permute_f[to];
    // printf("   To=%d  perm_to=%d\n", to, perm_to);
    if (parents[perm_to] == -1) {
      parents[perm_to] = perm_from;
      top.push_back(to);
    }
  }
  for (i = 0; i < top.size(); i++) DFS(top[i], adj, parents, permute_f);
}

int main()
{
  int nnodes;
  vector <int> parents;
  vector < vector <int> > adj;
  int i, j, tmp;
  int seed;
  vector <int> permute_f, permute_b;
  vector <bool> done;

  cin >> nnodes;
  cin >> seed;
  srand48(seed);

  for (i = 0; i < nnodes; i++) parents.push_back(drand48()*(i+1));
  adj.resize(nnodes+1);
  for (i = 0; i < parents.size(); i++) {
    adj[i+1].push_back(parents[i]);
    adj[parents[i]].push_back(i+1);
  }
  
//  for (i = 0; i < parents.size(); i++) printf(" %d", parents[i]);
//  printf("\n");
//  printf("\n");

//  for (i = 0; i < adj.size(); i++) {
//    printf("%d:", i);
//    for (j = 0; j < adj[i].size(); j++) printf(" %d", adj[i][j]);
//    printf("\n");
//  }

  for (i = 0; i < adj.size(); i++) permute_f.push_back(i);
  for (i = 0; i < permute_f.size(); i++) {
    j = drand48() * (permute_f.size() -i);
    tmp = permute_f[i];
    permute_f[i] = permute_f[j];
    permute_f[j] = tmp;
  }

//  printf("\n");
//  for (i = 0; i < permute_f.size(); i++) printf(" %d", permute_f[i]);
//  printf("\n");

  for (i = 0; i < permute_f.size(); i++) if (permute_f[i] == 0) j = i;
//  printf("j is %d\n", j);

  parents.clear();
  parents.resize(nnodes+1, -1);
  parents[0] = 0;

  DFS(j, adj, parents, permute_f);

  printf("echo ");
  for (i = 1; i < parents.size(); i++) printf(" %d", parents[i]);
  printf(" | ./a.out -\n");

  return 0;
}
