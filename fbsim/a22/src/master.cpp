//MASTER.CPP
//Driver Program
//Graham Buchanan

/***CHANGELOG***
 * 03 August 2022 - Added series of game simulation; 
 */

#include "master.hpp"

void master::Print() {
	std::map<std::string, Team *>::const_iterator mit;
	std::string a,b;
	a = "Location";
	b = "Name";
	printf("\n|Team #|%30s|%30s|Wins|Loss|Ties| off | def |  PF  |  PA  |  OPYD  |  ORYD  |  DPYD  |  DRYD  |\n",a.c_str(), b.c_str());
	for(mit = TM.begin(); mit != TM.end(); ++mit) 
		printf("|  %03u |%30s|%30s| %2u | %2u | %2u | %3u | %3u | %4d | %4d | %6d | %6d | %6d | %6d |\n"
				, mit->second->id, mit->second->location.c_str(), mit->second->name.c_str()
				, mit->second->wins, mit->second->loss, mit->second->ties
				, (mit->second->poff + mit->second->roff + mit->second->olin) / 3
				, (mit->second->pdef + mit->second->rdef + mit->second->dlin) / 3
				, mit->second->ptsc, mit->second->oppt, mit->second->opyd, mit->second->oryd, mit->second->dpyd, mit->second->dryd);

}

bool master::DeleteData() {
	size_t i;
	for(i = 0; i < TV.size(); ++i) delete TV[i];
	for(i = 0; i < GV.size(); ++i) delete GV[i];
	return true;
}
