//SS_SOLVER.CPP
//Solves a given grid of swords and shields with specific shapes
//Graham Buchanan
//06 November 2021

/***This program provides an accurate solution to the "Swords and Shields" Game outlined in ShapeShifter!!
 *  The program takes a grid on the command-line and a set of shapes on standard input.
 *  Recursively, each shape is applied at all possible grid locations until a solution 
 *		where the entire grid is a value of '1' is found.
 *	Once this solution is found, the program then returns a solution of shape placements,
 *		and the final path to solution.*/


#include <iostream>
#include <vector>
#include <sstream>


//Class to keep track of all grids and shapes
class Solver {
	public:
		bool Read_In(unsigned int argc, char **argv); //Reads in the grid and shapes
		bool Apply_Shape(int index, int r, int c); //Applies a given shape to a specific grid location
		bool Find_Solution(unsigned int index); //Recursively searches for the solution path
		void Print_Solution() const; //Print the solution path
		bool Is_Correct() const; //Checks whether the grid is correct
	protected:
		std::vector<std::string> g; //grid vector
		std::vector<std::vector<std::string> > sh; //shape vector
		std::vector<int> sr; //solution rows
		std::vector<int> sc; //solution cols
};

int main(int argc, char **argv) {

	Solver a; //declaring a class object
	if(!(a.Read_In(argc, argv))) return -1; //Read in the data

	if(a.Find_Solution(0)) //Find the solution
		a.Print_Solution(); //Print
	return 0;
}

bool Solver::Read_In(unsigned int argc, char **argv) {

	//declare vars
	std::string s = "";
	size_t i = 0;
	std::vector<std::string> v;
	std::stringstream ss;
	std::string a = "";

	//Throw an exception if there are not enough grid strings or any grid strings are not '0' or '1'
	try{ 
		if(argc < 3) throw((std::string) "Enter the grid strings.");
		for(i = 1; i < argc; ++i) {
			s = argv[i];
			if(s.find_first_not_of("01") != std::string::npos) throw((std::string) "Enter the grid strings.");
			g.push_back(s);
			s.clear();
		}
	} catch(std::string &e) {
		std::cerr << e << std::endl;
		return false;
	}

	//Reading in shapes
	//grab each line and check that each line is correct, then store the shape strings
	while(getline(std::cin, s)) {
		if((s.find_first_not_of(" 01") != std::string::npos)) {
			std::cerr << "Bad Shape\n";
			s = "";
		}else if(s != "") {
			ss.clear();
			ss.str(s);
			a.clear();
			while(ss >> a) {
				v.push_back(a);	
			}
			sh.push_back(v);
			v.clear();
		}
	}

return true;
}

bool Solver::Apply_Shape(int index, int r, int c) {

	size_t i = 0;
	size_t j = 0;

	//Initially, guarantee the selected shape can actually fit into this position.
	//Then, XOR each grid location with the appropriate shape-piece
	if(sh[index].size() > g.size()) return false;
	for(i = 0; i < sh[index].size(); ++i) {
		if(sh[index][i].size() > g[r + i].size()) return false;
		for(j = 0; j < sh[index][i].size(); ++j) {
			g[r + i][c + j] ^= sh[index][i][j];
			g[r + i][c + j] += '0';
		}
	}

	return true;
}

bool Solver::Find_Solution(unsigned int index) {

	size_t i = 0;
	size_t j = 0;

	//Check if the grid is correct before continuing
	if(index == (sh.size())) {
		if(Is_Correct()) {
			return true;
		} else {
			return false;
		}
	}

	//Check to see if the shape can fit!
	if(sh[index].size() > g.size()) {
		std::cout << "Shape too big\n";
		return false;
	}
	for(i = 0; i < sh[index].size(); ++i) {
		if(sh[index][0].length() > g[0].length()) {
			std::cout << "Shape too wide\n";
			return false;
		}
	}

	//Iterate through each grid location acceptable, and apply the given shape at that location
	//Then call Find_Solution() on the next shape until the solution path is found
	for(i = 0; i <= (g.size() - sh[index].size()); ++i) {
		for(j = 0; j <= (g[0].size() - sh[index][0].size()); ++j) {
			Apply_Shape(index, i, j);
			sr.push_back(i);
			sc.push_back(j);
			if(!Find_Solution(index + 1)) {
				Apply_Shape(index, i, j);
				sr.pop_back();
				sc.pop_back();
			} else
				return true;
		}

	}

	return false;
}

void Solver::Print_Solution() const {

	size_t i = 0;
	size_t j = 0;

	//Run through each shape that has been stored
	//Print the appropriate solution row and column
	for(i = 0; i < sh.size(); ++i) {
		for(j = 0; j < sh[i].size(); ++j) {
			if(j != 0) std::cout << ' ';
			std::cout << sh[i][j];
		}
		std::cout << ' ' << sr[i] << ' ' << sc[i] << std::endl;
	}
}

bool Solver::Is_Correct() const {

	size_t i = 0;
	size_t j = 0;

	//Iterate through each grid location and guarantee each item is '1'
	for(i = 0; i < g.size(); ++i) {
		for(j = 0; j < g[i].size(); ++j) {
			if(g[i][j] != '1') return false;
		}
	}

	return true;
}
