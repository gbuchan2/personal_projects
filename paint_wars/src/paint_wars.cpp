//PAINT_WARS.CPP
//Record Country Data
//Graham Buchanan
//18 March 2022

#include <vector>
#include <iostream>
#include <map>
#include <cstdio>
#include <fstream>

void Save_Data();

struct Alliance;
struct Country {

	std::string name;
	Alliance *ally;
	std::string party;
	int id;

	int Military;
	int GDP;
	int Research;
	int Infrastructure;
	double Average;
	int political_support;	

	Country *owner;
	std::map<std::string, Country *> puppets;

	void Calc_Average() {
		double m, g, r, i;
		m = Military;
		g = GDP;
		r = Research;
		i = Infrastructure;
		Average = ((m + g + r + i) / 4);
	}
};

struct Control {

	std::map<std::string, Country *> cm;
	std::vector<Country *> cv;
	std::map<std::string, Country *>::const_iterator mit;

	void Print();
	bool Save_Data(std::string f);
};

struct Alliance {

	std::string name;
	std::map<std::string, Country *> cmap;

	int Military;
	int GDP;
	int Research;
	int Infrastructure;
	double Average;
};

int main() {

	std::string s;
	std::string n, p;
	int m, g, r, i;
	Country *c;
	int j, k;
	char str[20];
	char a[14];

	Control *ctrl;

	ctrl = new Control;


	k = 0;
	printf("Please Enter Country Data\n");
	while(std::getline(std::cin, s)) {
		j = sscanf(s.c_str(), "%s %s %d %d %d %d", str, a, &m, &g, &r, &i);
		if(j != 6) {
			printf("j = %d, k = %d\n", j, k);
			perror("Not correct number of entries!\n");
			return -1;
		}
		n = str;
		for(j = 0; j < n.size(); ++j) if(n[j] == '_') n[j] = ' ';
		p = a;
		c = new Country;
		c->name = n;
		c->party = p;
		if(p[0] == 'M') c->political_support = 5; //Monarchy
		else if(p[0] == 'F') c->political_support = 4; //Fascism
		else if(p[0] == 'L') c->political_support = 6; //Liberalism
		else if(p[2] == 'm') c->political_support = 4; //Communism
		else c->political_support = 6; //Conservative
		c->Military = m;
		c->Research = r;
		c->Infrastructure = i;
		c->GDP = g;
		c->id = k;
		c->Calc_Average();
		ctrl->cv.push_back(c);
		ctrl->cm[n] = c;
		++k;
	}

	ctrl->Save_Data((std::string)"files/autosave.txt");

	ctrl->Print();
	delete ctrl;

	return 0;
}

void Control::Print() {
	int i;

	for(mit = cm.begin(); mit != cm.end(); ++mit) printf("#%d - %s (%s, %d) MIL - %d, GDP - %d, RES - %d, INF - %d\n", 
		mit->second->id, mit->first.c_str(), mit->second->party.c_str(), mit->second->political_support, mit->second->Military,
		mit->second->GDP, mit->second->Research, mit->second->Infrastructure);
	for(i = 0; i < cv.size(); ++i) delete cv[i];
}

bool Control::Save_Data(std::string f) {

	FILE *file;
	file = fopen(f.c_str(), "w");
	if(file == NULL) {
		perror("Cannot open file!\n");
		return false;
	}
	for(mit = cm.begin(); mit != cm.end(); ++mit) {
		 fprintf(file, "%s %s %d %d %d %d\n", mit->first.c_str(), mit->second->party.c_str(), mit->second->Military, mit->second->GDP, mit->second->Research, mit->second->Infrastructure);
	}

	return true;
}
