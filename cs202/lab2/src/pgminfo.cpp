//PGM Info
//Take PGM file on cin & count rows, cols, no pixs, avg val of pix (3 decimals)
//Graham Buchanan
//01 Sept 2021

#include <iostream>
#include <iomanip>
#include <cstdio>

int main() {

	std::string s; //P2
	int r = 0; //rows
	int c = 0; //cols
	int p = 0; //255
	double a = 0; //avg val
	int d = 0; //pix val
	int i = 0; //it
	int j = 0; //it

	//Need to check string, cols, and rows independently for fails
	//If there is not P2, cols > 0, rows > 0, then 255 in that order
	if(!(std::cin >> s) || (s != "P2")) {
		std::cerr << "Bad PGM file -- first word is not P2" << std::endl;
		return 1;
	} else if(!(std::cin >> c) || (c < 1)) {
		std::cerr << "Bad PGM file -- No column specification" << std::endl;
		return 1;
	} else if(!(std::cin >> r) || (r < 1)) {
		std::cerr << "Bad PGM file -- No row specification" << std::endl;
		return 1;
	} else if(!(std::cin >> p) || (p != 255)) {
		std::cerr << "Program must have '255' after intro" << std::endl;
		return 1;
	} else {
		for(i = 0; i < r; ++i) {
			for(j = 0; j < c; ++j) {
				//Read in all pixel values as integers. If value is <0 or >255, fail
				if(!(std::cin >> d)) {
					std::cerr << "Bad PGM file -- Not enough pixels" << std::endl;
					return 1;
				} else if ((d < 0) || (d > 255)) {
					std::cerr << "Bad PGM file -- pixel " << (i * c + j) << " is not a number between 0 and 255" << std::endl;
					return 1;
				} else
					a += d;
			}
			std::cin.clear();
		}
		//After we have read *all* pixels, if there are any others, quit out
		if(std::cin >> d) {
			std::cerr << "Bad PGM file -- Extra stuff after the pixels" << std::endl;
			return 1;
		} else {
			printf("# Rows: %11d\n# Columns: %8d\n# Pixels: %9d\nAvg Pixel: %8.3f\n", r, c, (i * j), (a / (i * j)));
		}
	}
	return 0;	
}
