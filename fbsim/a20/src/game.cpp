//GAME.CPP
//Definitions of the Game Class
//Graham Buchanan

/***Update Log***
 *  
 *  19 November 2021 - Creation and Declaration
 *  16 Decemiber 2021 - Score Generator, Print_Data
 *
 */

#include "team.hpp"

/*class Game {
	public:
		Game(Team *away, Team *home, std::string &location const); //Constructor
		void Play_Game();
		void Edit_Game();
		int score_generator();
		void Print_Data() const;
		
	protected:
		uint16_t score = 0; //0-7 away, 8-15 home
		char winner = '\0'; //a - away, h - home, u - undecided, t - tie
		Team *a = NULL; //Away Team
		Team *h = NULL; //Home Team
		std::string loc = "";
		unsigned int id_num = 0;
};*/
		

Game::Game(unsigned int &id const, Team *away, Team *home, std::string &location const) {

	id_num = id;
	a = away;
	h = home;
	loc = location;
	winner = 'u';
	score = 0;

}

void Play_Game() {

	int home_score = 0;
	int away_score = 0;
	std::string h_name = "";
	std::string a_name = "";

	if(h == NULL) {
		printf("This game data has not been specified.\n");
		return;
	}

	h_name = (h->getLocation() + ' ' + h->getName());
	a_name = (a->getLocation() + ' ' + a->getName());

	std::cout << "Welcome to " << loc << "! Today's game is between the " << a_name << " and the " << h_name << ".\n";
	a->PrintData();
	h->PrintData();

	std::cout << "What is the away team's score? > ";
	std::cin >> away_score;
	while(!(std::cin) || (away_score < 0) || (away_score > 255)) {
		std::cin.clear();
		std::cout << "The score must be between 0 and 255. Please try again. > ";
		std::cin >> home_score;
	}
	std::cout << "What is the home team's score? > ";
	std::cin >> home_score;
	while(!(std::cin) || (home_score < 0) || (home_score > 255)) {
		std::cin.clear();
		std::cout << "The score must be between 0 and 255. Please try again. > ";
		std::cin >> home_score;
	}

	if(home_score > away_score) winner = 'h';
	else if(home_score == away_score) winner = 't';
	else if(home_score < away_score) winner = 'a';

	Print_Data();
}

int Game::score_generator() {
	
	uint16_t sc = 0;

	int h_diff = 0;
	int a_diff = 0;
	int hl = (rand() % 6 + 5);
	int al = (rand() % 6 + 5);
	
	hr = ((h->getRating >> 8) & 0xff + 2);
	ar = (a->getRating & 0xff);
	h_diff = (hl * hr) - (al * ar);
	
	hr = (h->getRating & 0xff + 2);
	ar = ((a->getRating >> 8) & 0xff);
	a_diff = (al * ar) - (hl * hr);

	int home_score = 0;
	int away_score = 0;

	hl = h_diff;
	al = a_diff;
	if(h_diff < 0) {
		al += (0 - h_diff);
	}
	if(a_diff < 0) {
		hl += (0 - a_diff);
	}

	away_score = 1.67 + 0.0781*al + 0.000146*al*al - 0.0000000002*al*al*al*al + (rand() % 15);
	home_score = 1.67 + 0.0781*hl + 0.000146*hl*hl - 0.0000000002*hl*hl*hl*hl + (rand() % 15);
	if(away_score < 0) away_score = 0;
	if(away_score > 255) away_score = 255;
	if(home_score < 0) home_score = 0;
	if(home_score > 255) home_score = 255;

	sc = ((away_score << 8) | (home_score));
	return sc;
}
		
void Game::Print_Data() const {

	std::string home = "";
	std::string away = "";
	std::string win = "";
	std::string loser = "";
	int wscore = 0;
	int lscore = 0;

	if(h = NULL) {
		printf("This game data has not been specified.\n");
		return;
	} else if(winner == '\0') {
		printf("Game #%d has not been played!\n", id_num);
		return;
	}
	
	home = (h->getLocation() + ' ' + h->getName());
	away = (a->getLocation() + ' ' + a->getName());

	if(winner == 'h') {
		win = home;
		loser = away;
		wscore = (score & 0xff);
		lscore = ((score >> 8) & 0xff);
	} else {
		win = away;
		loser = home;
		wscore = ((score >> 8) & 0xff);
		lscore = (score & 0xff);
	}

	printf("Game #%d:\n %s at %s\n Final Score: %s %d, %s %d\n", id_num, away, home, win, wscore, loser, lscore);

}
