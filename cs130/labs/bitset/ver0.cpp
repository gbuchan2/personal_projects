//lab.cpp
//BITSET LAB
//Graham Buchanan
//30 Jan 2021

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

std::string ToBinary(int val, int spa) {
	std::string c = "";
	int a = 1;
	do {
		if( a % (spa +1) == 0)
			c += ' ';
		else {
			if(val % 2 == 0)
				c += '0';
			else
				c += '1';
			val = val / 2;
		}
		++a;
	} while(val > 0);

	val = 0;

	std::string s = std::string(c.rbegin(), c.rend());
	return s;
}

int main(int argc, char ** argv) {
	std::cout << "Program Run." << std::endl;

	int b = 145;
	int p = 4;
	std::cout << ToBinary(b, p) << std::endl;
	return 0;
	
}

