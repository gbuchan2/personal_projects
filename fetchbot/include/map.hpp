//MAP.HPP
//Header file for map generator
//Graham Buchanan
//14 Sept 2023

/***FURTHER DESCRIPTION***
* Specify map generation algorithm
* Parameters taken on command line args
*/

/***CHANGELOG***
 * 14 Sept 2023 - Creation; ball and bot
 *
 *
 */

#include <time.h>
#include <vector>
#include <cmath>
#include <map>
#include <set>
#include <list>
#include <deque>
#include <stdint.h>
#include <cstdio>
#include <iostream>
#include <fstream>

class Map {
  public:
		Map();
		~Map();
		bool Read(std::string &filename);
		bool Save(std::string &filename);
		bool Capture(uint32_t direction);
		bool Move_Bot(uint32_t x);
		bool Place_Wall(uint32_t x, uint32_t y);
		void Generate_Image();
		bool Set_Map_Characters(std::string &s, char &c); //Allows user to specify symbols for each item
		bool Generate_Map(uint32_t w); //Generates a w*w map based upon input parameters
	  void Print();
		void Print_Capture();
		void Print_Picture();
		void Print_Info();

  protected:
		std::vector<std::string> camera_cone;
		std::vector<std::string> picture;
		std::vector<std::string> game_map;
		std::map<std::string, char> map_characters;

		std::map<std::string, char>::const_iterator mit;
		uint32_t bot_x, bot_y, ball_x, ball_y, bot_dir;
		int ball_in_image, ball_hor_image;
};
