//lab4.cpp
//Complicated Authoring Assistant BONUS POINTS
//Graham Buchanan
//20 October 2020

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

//var declare function to save output file
void saveOutput(vector<string>& v) 
{

	//open/create file
	ofstream ofs("output.txt");
	for(const auto &e : v) ofs << e << endl;
	ofs.close();

}

//var declare function to count words
int NumWords(vector<string>& v) 
{
	//var declare necessary variables
	int wordCount = 0;
	string str;
	
	//loop while there is text
	for(string& line : v)
	{
		stringstream ss(line);
		
		//loop input string into stream
		while(ss >> str) 
		{
			wordCount++;
		}
	}
	//return variable wordCount
	return wordCount;
}

//var declare function to count nonWS characters
int NumNonWSCharacters(vector<string>& v) 
{
	//var declare necessary variables
	int nonWS_char = 0;

	//loop for the length of the string 
	for(string& line : v)
	{
		for(unsigned int i = 0; i < line.size(); ++i) 
		{		
			//cond if there isn't a space
			if( !(isspace(line[i])))
			{
			
				nonWS_char++;
			}
		}
	}

	return nonWS_char;
}

//var declare function to replace characters in a string
void CharReplace(vector<string>& v, char old_char, char new_char) 
{
	//loop for the length of the string
	for(string& line : v)
	{
		for(unsigned int i = 0; i < line.size(); ++i)
		{
			if(line[i] == old_char)
				line[i] = new_char;
		}
	}
}

//var declare function to print menu
char PrintMenu() {

	//var necessary variable
	char userSelect;

	//txt print Menu
	cout << endl << "Options" << endl;
	cout << "w - Number of words" << endl;
	cout << "c - Number of non-whitespace characters" << endl;
	cout << "r - Replace a character" << endl;
	cout << "q - Quit" << endl << endl;

	//txt prompt user for input
	cout << "Choose an option: ";
	cin >> userSelect;		

	return userSelect;
}

int main() {

	//var declare necessary input variables
	string user_file;
	string line;
	vector<string> vecString;
	char userSelect;
	
	//prompt user for inputs
	cout << "Enter a text file to read: ";
	getline(cin, user_file);
	
	//opens file
	ifstream in(user_file);
	if(!in)
	{
		cout << "Unable to open " << user_file << "." << endl;
		return 0;
	}
	
	//loop while there is text
	while(getline(in, line))
	{
		//cond if there is text...add line to vector
		if(line.size() > 0)
			vecString.push_back(line);
	}
	
	//close file
	in.close();
	
	//txt repeat text file verbatim
	cout << endl << user_file << " contains:" << endl;
	for(string& line : vecString)
		cout << line << endl;

	//loop as long as no "Quit"
	while((userSelect = PrintMenu()) != 'q') {

		//cond switch case (menu)
		switch(userSelect) {
			
			//cond 'w' leads to Menu option...count words
			case 'w':

				//var declare variable
				int wordsInText;

				//run function
				wordsInText = NumWords(vecString);
				
				//txt print word count
				cout << endl << "Number of words: " << wordsInText  << endl;

				break;
			
			//cond 'c' leads to Menu option...count non-WS
			case 'c':

				//var declare variable
				int nonWS_char;
				
				//run function
				nonWS_char = NumNonWSCharacters(vecString);
				
				//txt print non-WS count
				cout << endl << "Number of non-whitespace characters: " << nonWS_char << endl;

				break;
			
			//cond 'r' leads to Menu option...replace character
			case 'r':

				//var declare variable
				char old_char, new_char;
				
				//prompt user for input
				cout << "Enter a character to find: ";
				cin >> old_char;		//var char to replace
				cout << "Enter a character to replace: ";
				cin >> new_char;		//var char to replace with
				
				//run function
				CharReplace(vecString, old_char, new_char);
				
				//txt print edited user_str
				cout << endl << "New contents:" << endl;
				for(string& line : vecString)
					cout << line << endl;

				break;
			
			//cond DEFAULT...empty line
			default:
				cout << endl;
		}
	}
	
	//run function saveOutput
	saveOutput(vecString);

	return 0;
}
