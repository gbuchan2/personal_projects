//Bigwhite.cpp
//Make a blank PGM file
//Graham Buchanan
//03 Sept 2021

#include <iostream>
#include <iomanip>
#include <sstream>

int main(int argc, char **argv) {
	int r = 0; //rows
	int c = 0; //cols
	std::istringstream ss; //sorting inputs
	int i = 0; //it
	
	//error checking, need 3 args, need rows & cols to be positive integers
	if(argc != 3) {
		std::cerr << "usage: bigwhite rows cols" << std::endl;
		return 1;
	} else {
		ss.str(argv[1]);
		if(!(ss >> r) || (r < 1)) {
			std::cerr << "usage: bigwhite rows cols" << std::endl;
			return 1;
		} else {
			ss.clear();
			ss.str(argv[2]);
			if(!(ss >> c) || (c < 1)) {
				std::cerr << "usage: bigwhite rows cols" << std::endl;
				return 1;
			} else {
				//Reprint all the data 
				std::cout << "P2" << std::endl << c << ' ' << r << std::endl << 255 << std::endl;
				for(i = 0; i < (r * c); ++i) {
					std::cout << 255 << ' ';
					std::cout << std::endl;
				}

			}
		}
	}
	return 0;
}
