//CODE_PROCESSOR.CPP
//Code Processor Class Definitions File
//Graham Buchanan
//20 October 2021

		/* This program creates a server to handle users redeeming codes for prizes, inspired by the "buy product, redeem code" marketing gimmick.
		 * There is one main Class "Code_Processor" which replicates two classes "Prize" and "User" to assign data to specific users and items available to purchase.
		 * This cpp file defines the Code_Processor Class, effectively the main server. It is driven by the "cp_tester" which creates our "UI". 
		 * The Class can create new users and prizes, handle user data management (phone numbers, points), and handle user-prize interactions (redeeming points, codes).*/


#include "code_processor.hpp"
#include <set>
#include <map>
#include <string>
#include <cstdio>

/***User and Prize management***/

//CP Class Function New Prize
//Adds a new prize and inserts into the prizes map
bool Code_Processor::New_Prize(const std::string &id, const std::string &description, int points, int quantity) {

	//Error Checking
	if((id == "") || (Prizes.find(id) != Prizes.end())) return false;
	if((points <= 0) || (quantity <= 0)) return false;

	//Create a new Prize and assign appropriate data
	Prize *p = new Prize;
	p->id = id;
	p->description = description;
	p->points = points;
	p->quantity = quantity;

	//Insert into Prizes map
	Prizes.insert(std::make_pair(id, p));
	return true;
}

//CP Class Function New User
//Adds a new user and inserts into the users map
bool Code_Processor::New_User(const std::string &username, const std::string &realname, int starting_points) {

	//Error Checking
	if((username == "") || (Names.find(username) != Names.end()) || (starting_points < 0)) return false;

	//Create a new user and assign appropriate data
	User *u = new User;
	u->username = username;
	u->realname = realname;
	u->points = starting_points;

	//Insert (username, User *) pair
	Names.insert(std::make_pair(username, u));
	return true;
}

//CP Class Function Delete User
//Removes a user from the users maps
bool Code_Processor::Delete_User(const std::string &username) {

	//Variable Declares
	std::set<std::string>::const_iterator pit;
	std::map<std::string, User *>::iterator mit;
	User *u;

	//Error Checking
	if((username == "") || (Names.find(username) == Names.end())) return false;

	//Find in the map
	mit = Names.find(username);
	u = mit->second;

	//Delete each phone number from the phone set
	if(!(u->phone_numbers.empty())) {
		for(pit = u->phone_numbers.begin(); pit != u->phone_numbers.end(); ++pit) {
			Phones.erase(Phones.find(*pit));
		}
		u->phone_numbers.clear();
	}

	//Erase the (username, User *) pair
	Names.erase(mit);
	return true;
}

/***User Data Management***/

//CP Class Function Add Phone
//Adds a phone number to the user
bool Code_Processor::Add_Phone(const std::string &username, const std::string &phone) {

	//Variable Declares 
	std::map<std::string, User *>::const_iterator mit; 

	//Error Checking
	if((username == "") || (phone == "")) return false;
	if((Phones.find(phone) != Phones.end()) || (Names.find(username) == Names.end())) return false;

	//Adding Phone number to user
	mit = Names.find(username);
	mit->second->phone_numbers.insert(phone);

	//Adding Phone number to list of numbers
	Phones.insert(std::make_pair(phone, mit->second));
	return true;
}


//CP Class Function Remove Phone
//Removes phone number to the user
bool Code_Processor::Remove_Phone(const std::string &username, const std::string &phone) {

	//Variable Declares
	std::map<std::string, User *>::const_iterator mit; 

	//Error Checking
	if((username == "") || (phone == "")) return false;
	if((Phones.find(phone) == Phones.end()) || (Names.find(username) == Names.end())) return false;

	//Removing Phone number to user
	mit = Names.find(username);
	if((mit->second->phone_numbers.erase(phone)) == 0) return false;

	//Remove Phone number from list of numbers
	Phones.erase(phone);
	return true;
}

//CP Class Function Show Phones
//Returns the phone number(s) associated with a user
std::string Code_Processor::Show_Phones(const std::string &username) const {

	//Variable Declares
	std::string s = "";
	std::set<std::string>::const_iterator sit;
	std::map<std::string, User *>::const_iterator mit; 

	//Error Checking
	if((username == "")) return "BAD USER";
	if(Names.find(username) == Names.end()) return "BAD USER";

	//Appending to string
	//Return the string that contains all user phone numbers
	mit = Names.find(username);
	if(mit->second->phone_numbers.size() == 0) return "";
	for(sit = mit->second->phone_numbers.begin(); sit != mit->second->phone_numbers.end(); ++sit) {
		s += *sit;
		s += "\n";
	}
	return s;
}

/***CODES***/

//CP Class Function Enter Code
//user enters a code...checks for validity
int Code_Processor::Enter_Code(const std::string &username, const std::string &code) {

	//Variable Declares
	std::map<std::string, User *>::iterator mit;
	int a = 0; 
	size_t i = 0; 
	unsigned int h = 5381; //Hash value

	//Error Checking
	if((username == "") || (Names.find(username) == Names.end())) return -1;
	if(Codes.find(code) != Codes.end()) return -1;

	//Running djbhash
	for(i = 0; i < code.size(); i++) {
		h = (h << 5) + h + code[i];
	}

	//Validating code
	if((h % 17) == 0) a = 10;
	else if ((h % 13) == 0) a = 3;

	//Assigning code values
	Codes.insert(code);
	mit = Names.find(username);
	mit->second->points += a;
	return a;
}

//CP Class Function Text Code
//user enters a code...checks  for validity
int Code_Processor::Text_Code(const std::string &phone, const std::string &code) {

	//Variable Declares
	std::map<std::string, User *>::iterator mit;
	int a = 0; 
	size_t i = 0;
	unsigned int h = 5381; //Hash Value

	//Error Checking
	if((phone == "") || (Phones.find(phone) == Phones.end())) return -1;
	if(Codes.find(code) != Codes.end()) return -1;

	//Running djbhash
	for(i = 0; i < code.size(); i++) {
		h = (h << 5) + h + code[i];
	}

	//Validating code
	if((h % 17) == 0) a = 10;
	else if ((h % 13) == 0) a = 3;

	//Assigning code values
	Codes.insert(code);
	mit = Phones.find(phone);
	mit->second->points += a;
	return a;
}

//CP Class Function Mark Code
//Mark a code used -- cannot be used again 
bool Code_Processor::Mark_Code_Used(const std::string &code) {

	//Variabel Declares
	size_t i = 0;
	unsigned int h = 5381; //Hash Value

	//Error Checking
	if((code == "") || Codes.find(code) != Codes.end()) return false;

	//Running djbhash
	for(i = 0; i < code.size(); i++) {
		h = (h << 5) + h + code[i];
	}

	//Validating code
	if(((h % 17) == 0) || ((h % 13) == 0)) Codes.insert(code);
	else 
		return false;
	
	return true;
}

/***POINTS MANAGEMENT***/

//CP Class Function Balance
//Returns a user's current balance
int Code_Processor::Balance(const std::string &username) const {

	//Error Checking
	if((username == "") || (Names.find(username) == Names.end())) return -1;

	//Return user points
	return (Names.find(username))->second->points;
}

//CP Class Redeem Prize
//Allows a user to redeem their points for a prize
//Decrements a prize's quantity or removes the prize
bool Code_Processor::Redeem_Prize(const std::string &username, const std::string &prize) {

	//Error Checking
	if((username == "") || (Names.find(username) == Names.end())) return false;
	if((prize == "") || (Prizes.find(prize) == Prizes.end())) return false;
	if((Names.find(username))->second->points < ((Prizes.find(prize))->second->points)) return false;

	//Decrement user point total
	(Names.find(username))->second->points -= (Prizes.find(prize))->second->points;

	//See if prize quantity is 0
	if(((Prizes.find(prize))->second->quantity -= 1) == 0) Prizes.erase(prize);
	
	return true;
}

//CP Class Destructor
//Iterate through all containers and delete pointers
Code_Processor::~Code_Processor() {

	//Variable Declares
	std::map<std::string, User *>::iterator mit;
	std::map<std::string, Prize *>::iterator pit;

	//Delete all User *
	if(!(Names.empty())) {
		for(mit = Names.begin(); mit != Names.end(); ++mit) {
			delete mit->second;
		}
	}
	//Code Below is unnecessary  
	//Caused a double "delete" error in C++
	//--> very interesting bug I have not encountered yet, 
	//--> so I have left it just for my own recollection
/*	if(!(Phones.empty())) {
		for(mit = Phones.begin(); mit != Phones.end(); ++mit) {
			delete mit->second;
		}
	}*/

	//Delete all Prize *
	if(!(Prizes.empty())) {
		for(pit = Prizes.begin(); pit != Prizes.end(); ++pit) {
			delete pit->second;
		}
	}
}

//CP Class Write
//Saves the current state of the "server"
//Generate file of "ADD_USER, PRIZE, ADD_PHONE, MARK_USED" calls
bool Code_Processor::Write(const std::string &filename) const {

	//Create all necessary iterators
	std::map<std::string, User *>::const_iterator nit; //Names iterator
	std::map<std::string, Prize *>::const_iterator pit; //Prizes iterator
	std::set<std::string>::const_iterator sit; //Set iterator

	//File Pointer
	FILE *file;

	//Error Checking 
	if(filename == "") return false;
	file = fopen(filename.c_str(), "w");
	if(file == NULL) return false;

	//Print all prizes
	if(!(Prizes.empty())) { //There are prizes
		for(pit = Prizes.begin(); pit != Prizes.end(); ++pit) { //Print Prizes
			fprintf(file, "PRIZE %s %d %d %s\n", pit->first.c_str(), pit->second->points, pit->second->quantity, pit->second->description.c_str());
		}
	}

	//Print all Users and associated phone numbers
	if(!(Names.empty())) {
		for(nit = Names.begin(); nit != Names.end(); ++nit) { //Print Users
			fprintf(file, "ADD_USER %s %d %s\n", nit->first.c_str(), nit->second->points, nit->second->realname.c_str());

			//Rim through all associated phone numbers
			if(!((nit->second->phone_numbers).empty())) { //Print Phone Nums
				for(sit = nit->second->phone_numbers.begin(); sit != nit->second->phone_numbers.end(); ++sit) {
					fprintf(file, "ADD_PHONE %s %s\n", (nit->first).c_str(), (*sit).c_str());
				}
			}
		}
	}

	//Print all used codes
	if(!(Codes.empty())) {
		for(sit = Codes.begin(); sit != Codes.end(); ++sit) {
			fprintf(file, "MARK_USED %s\n", (*sit).c_str());
		}
	}

	//Close the file and return
	fclose(file);
	return true;	
}
