//BSTREE_LAB.CPP
//IMPLEMENTING A BINARY SEARCH TREE
//Graham Buchanan
//14 November 2021

/***This Program implements specific methods of a Binary Search Tree.
 *  This program can return the height and depth of a given node,
 *  copy one tree into another, return a vector of all the values stored in a tree,
 *  return a vector of all keys stored in a tree,
 *  and balance said tree.*/


#include <vector>
#include <string>
#include <iostream>
#include <cstdio>
#include "bstree.hpp"
using namespace std;
using CS202::BSTree;
using CS202::BSTNode;

//Return the depth of the specified key
int BSTree::Depth(const string &key) const
{
	int d = 0;

	//iterate through tree until you find the right node
	//the "right" node should be findable if the tree is made correctly
	//counter d is counting how many nodes needed to step through
	if(sentinel->right == sentinel) return -1;
	BSTNode *n = sentinel->right;
	while(1) {
		if(n == sentinel) return -1;
		if((string)key == n->key) return d;
		if((string)key > n->key) n = n->right;
		else n = n->left;
		++d;
	}
	return -1;
}

//Return the height of the tree
int BSTree::Height() const
{
	int h = 0;

	//run the recursive height finder to return the height (see below)
	if(sentinel->right == sentinel) return 0;
	h = recursive_find_height(sentinel->right);

	return h;
}

//Create a vector of all the keys in the tree via in-order traversal
vector <string> BSTree::Ordered_Keys() const
{
	//call make_key_vector on the root
	vector <string> rv;
	make_key_vector(sentinel->right, rv);
	return rv;
}

//Copy Constructor
BSTree::BSTree(const BSTree &t)        
{
	//reset the size
	//set all sentinel values, and then use the assignment overload
	
	size = 0;
	BSTNode *n = new BSTNode;
	this->sentinel = n;
	this->sentinel->left = sentinel;
	this->sentinel->right = sentinel;
	this->sentinel->val = NULL;
	this->sentinel->key = "";

	*this = t;
}

//Assignment Overload
BSTree& BSTree::operator= (const BSTree &t) 
{

	//create two vectors holding the keys and values in order
	vector<string> kv = t.Ordered_Keys();
	vector<void *> vv = t.Ordered_Vals();

	//Create a node to act as the root
	BSTNode *node = new BSTNode;
	node = make_balanced_tree(kv, vv, 0, vv.size());

	//set the root of THIS tree to the root of the generated tree
	this->sentinel->right = node;
	this->size = t.size;
	return *this;
}

//Recursively find the height of a specific ndoe
int BSTree::recursive_find_height(const BSTNode *n) const
{
	int l = 0;
	int r = 0;

	//step through the tree (going down) until you find a leaf node
	//Find the height of the right and left child independently and compare
	//larger height = height -1 of the parent
	if((n->left == sentinel) && (n->right == sentinel)) return 1;
	if((n != sentinel) && (n->left != sentinel)) {
		l = 1 + recursive_find_height(n->left);
	} 
	if(n->right != sentinel) {
		r =	1 + recursive_find_height(n->right);
	}
	return (r > l) ? r : l;
}

//Create a vector of keys in order
void BSTree::make_key_vector(const BSTNode *n, vector<string> &v) const
{

	//run through the entire tree, storing all nodes and their respective children
	if(n == sentinel) return;
	make_key_vector(n->left, v);
	v.push_back(n->key);
	make_key_vector(n->right, v);
}

//Return the root of a balanced subtree
BSTNode *BSTree::make_balanced_tree(const vector<string> &sorted_keys, 
		const vector<void *> &vals,
		size_t first_index,
		size_t num_indices) const
{
	//find the middle of the given indices
	size_t middle = ((num_indices / 2) + first_index);

	//return the sentinel node if leaf
	if(num_indices == 0) return sentinel;
	BSTNode *n = new BSTNode;

	//store the appropriate value and key
	n->val = vals[middle];
	n->key = sorted_keys[middle];

	//assign the left and right via recursion involving the appropriate indices
	//Here, you effectively splice the tree into smaller subtrees until approaching a leaf
	n->left = make_balanced_tree(sorted_keys, vals, first_index, middle - first_index);
	n->left->parent = n;
	n->right = make_balanced_tree(sorted_keys, vals, middle + 1, (num_indices - 1) / 2);
	n->right->parent = n;

	return n;
}
