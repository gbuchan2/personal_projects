%% BUCK CONVERTER Models %%
Vg = 10;
R = 5;
Ron = 0.05;
RL = 0.5;
RD = 0.5;
VD = 0.6;

s = 1001;
M = zeros(1,s);
V = zeros(1,s);

for a = 1:s
    D = (a-1)/1000;
    Dp = 1-D;

    M(a) = D*(1-Dp*VD/(D*Vg))*(R/(R+D*Ron+RL+Dp*RD));
    V(a) = Vg*M(a);
    E(a) = M(a)/D;
end

%% DC Plots %%
fig1 = figure;
x = 0:.001:1;
subplot(3,1,1);
% hold on
plot(x, V)
title('Output vs Duty Cycle')
xlabel('Duty Cycle')
ylabel('Output')
% legend('K=10', 'K=0.1')
% hold off

subplot(3,1,2);
plot(x, M)
title('Conversion Ratio vs Duty Cycle')
xlabel('Duty Cycle')
ylabel('M(D)')

subplot(3,1,3);
plot(x, E)
title('Efficiency vs Duty Cycle')
xlabel('Duty Cycle')
ylabel('Efficiency')
ylim manual
ylim([0,1])

%% AC Plots %%
fig2 = figure;
