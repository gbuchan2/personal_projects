//MASTER.HPP
//Driver Program
//Graham Buchanan

/***CHANGELOG***
 * 09 SEPT 2024 - Creation; 
 */

#include "game.hpp"
/*
 * #include "league.hpp"
 */

struct master {

	master();
	~master();

	//SAVE STATE
	bool LoadState(std::string &filename);
	bool SaveState(std::string &filename);

	//LEAGUE MANAGEMENT
	bool CreateLeagues();

	//TEAM MANAGEMENT
	bool EnterTeams();
	
	//GAME MANAGEMENT
	bool Play_A_Game();
	
	//BASE
	bool DeleteData();
	void Print();

	//data
	std::map<std::string, Team *> TM;
	std::vector<Team *> TV;
	std::vector<Team *> dOne;
	std::vector<Team *> dTwo;
	std::vector<std::vector<Game *> > sch;
};
