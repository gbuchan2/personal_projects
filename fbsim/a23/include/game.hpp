//GAME.HPP
//Declarations of the Game module
//Graham Buchanan

/***CHANGELOG***
*
* 21 JULY 2022 - Creation and basic implementation
* 22 July 2022 - Print function
*
*/

#include "team.hpp"
#include <cmath>

struct Game {
	bool Game_Played();
	bool Play_Game();
	bool Sim_Multiple_Games(int n);
	bool Reset_Data();
	bool Calc_Game();
	void Print();

	Team *home;
	Team *away;
	uint8_t home_tag;
	uint8_t away_tag;

	uint8_t week;

	int8_t home_ptsc;
	int16_t home_pyds;
	int16_t home_ryds;

	int8_t away_ptsc;
	int16_t away_pyds;
	int16_t away_ryds;
	char winner;
	uint16_t gmID;
};
