The Agras collectively make up the agricultural economy onboard the Hub. They provide all sustenance, including grains and alcohol. Continual improvement of their product spells their goal. Any potential competition (which is almost impossible anyways) is met with strict discipline and violence. The Union maintains a strict alliance with the [[Arator]]s; between the two, both food and gun are secure from the rest of the Hub. The Union often strikes when the Treaty votes against their policies, and is generally seen as temperamental amongst the other guilds.
### Guild
Name: "The Agra Union"
Emblem: Scythe and Wheat
Members: 144
Leader: [[Leopold]]
[[Humanity#The Grand Treaty|Treaty]] Seats: 5

### Occupations:
- Botanist - Researches plant quality and maintains records
- Yeoman -  Manages each Hydroponics Lab
- Cook - Prepares Hydroponics Lab products for consumption
- Bin Man - Handles all recycling and waste
- Brewer - Produces mult from Hydroponics Lab products

### Stations:
- Hydroponics Labs: [[3]], [[5]], [[13]], [[16]] 
- Botany Labs: 3, [[9]], [[20]]
- The Recycler in Sector [[2]]
- Brewery in Sector 16

### Notables:
- [[Hanna]]
- [[Mimnet]]



