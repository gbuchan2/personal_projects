Implement a fully-autonomous neuromorphic bot that plays 'football'. The end goal features a digital neural network to a) identify a robot, ball, and goal; and b) instruct the robot to push the ball into the goal.

## Components
- Raspberry Pi 4B+ 
	- Runs the Camera
	- Implements the digital neural network
	- Communicates via pySOCKET with ESP8266
- ESP8266
	- Communicates via HTML requests with Pi4B+
	- Drives the robot locally
	- Controls the twin motor controllers (total of four motors on-body)
- Logitech 720p Webcam
- L298N
	- Simple motor controller

## Implementations
1. [[FETCH]] bot
	1. Identifies a ball and moves to intercept
	2. All components locally on-body
	3. Testing ground for wireless communication and image processing
2. 