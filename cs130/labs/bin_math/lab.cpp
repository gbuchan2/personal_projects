//lab.cpp
//Add/Subtract, Multiply/Divide manually
//Graham Buchanan
//09 February 2021
//Fellow coders: Christian Graham

//GIVEN BY INSTRUCTOR
#include <cstdio>

int Multiply(int leftop, int rightop);
int Add(int leftop, int rightop);
int Sub(int leftop, int rightop);
int Twos(int op);
int Div(int leftop, int rightop);
int Mod(int leftop, int rightop);
//GIVEN BY INSTRUCTOR
int main(int argc, char *argv[])
{
	int left, right, result;
	char op;

	if (4 != argc)
	{
		printf("Usage: %s <left> <op> <right>\n", argv[0]);
		return -1;
	}
	sscanf(argv[1], "%d", &left);
	sscanf(argv[2], "%c", &op);
	sscanf(argv[3], "%d", &right);

	switch (op)
	{
		case 'm':
		case 'x':
		case '*':
			result = Multiply(left, right);
			break;
		case 'a':
		case '+':
			result = Add(left, right);
			break;
		case 's':
		case '-':
			result = Sub(left, right);
			break;
		case 'd':
		case '/':
			result = Div(left, right);
			break;
		case '%':
			result = Mod(left, right);
			break;
		default:
			result = -11111111;
			break;
	}
	printf("%d\n", result);
	return 0;
}

//returns the product of two numbers
int Multiply(int leftop, int rightop) {
	//r & l & f cause its easier
	int l = leftop;
	int r = rightop;
	int f = 0;
	bool m = false;
	//checks to see if either number is neg, flips bool
	if(((leftop >> 31) & 1) == 1) {
		l = Twos(leftop);
		m = ~m;
	}
	if(((rightop >> 31) & 1) == 1) {
		r = Twos(rightop);
		m = ~m;
	}
	//loop
	while(r != 0) {
		if( r & 1)
			f = Add(l, f);
		r = (r >> 1);
		l = (l << 1);
	}
	//if bool is true, neg
	if(m == true)
		f = Twos(f);
	return f;
}

//returns sum of two nums
int Add(int leftop, int rightop) {
	//carry, sum, and a for reasons
	int c = 0;
	int s = 0;
	int a = 0;
	//iterate through all bits
	for(unsigned int i = 0; i < 32; ++i) {
		//right and left makes things simpel
		int r = ((rightop >> i) & 1);
		int l = ((leftop >> i) & 1);
		s = ((l^r)^c);
		//have you ever used a truth table to develop a logical statement...
		// \/ I spent two hours one night on this right here \/...AND IT WASNT THE PROBLEM!!!
		c = (((~l)&r&c)|(l&(~r)&c)|(l&r&(~c))|(l&r&c));
		//stolen from previous lab, literally just scoots the value of s over and ors it
		a |= (s << i);
	}
	return a;
}

//returns the addition of the opposite of one bit and the other
int Sub(int leftop, int rightop) {

	int a = Twos(rightop);
	int b = leftop;
	return Add(a, b);
}

//literally just flips the bits and adds one
int Twos(int op) {
	return (Add(~op, 1));
}

//returns the quotient as a whole number...pretty much just the "restoring division" algorithm given
int Div(int leftop, int rightop) {
	unsigned int q = 0;
	if(rightop == 0) {
		return 0;
	}
	//two longs to hold all of the remainder r and divisor d
	long r = leftop;
	long d = (static_cast<long>(rightop) << 32);
	for(int i = 31; i >= 0; --i) {
		//continual subtraction
		r = (r << 1) - d;
		if(r >= 0) 
			q |= (1 << i);
		else
			r += d;
	}
	return q;
}

//returns the remainder of a division equation...pretty much just the "resoring division" algorithm given
int Mod(int leftop, int rightop) {	
	unsigned int q = 0;
	if(rightop == 0) {
		return 0;
	}
	//two longs to hold all of the remainder r and the divisor d
	long r = leftop;
	long d = (static_cast<long>(rightop) << 32);
	//iterate through all 32 bits
	for(int i = 31; i >= 0; --i) {
		//continual subtraction
		r = (r << 1) - d;
		if(r = 0)
			q |= (1 << i);
		else
			r += d;
	}
	return (r >> 32);
}


