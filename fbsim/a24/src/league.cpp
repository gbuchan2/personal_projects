//LEAGUE.HPP
//declarations for League class
//Graham Buchanan

/***CHANGELOG***
* 05 August 2022 - Creation
*
*
*/

#include "game.hpp"

struct League {

	League();
	League(int nodivs); //Create league with blank divisions
	~League();
	bool ResetData();
	void Print();

	std::string name;
	std::string tag;
	uint8_t leagueID; //internal leagues will not have set leagueID

	//Map of all teams within the league. 
	//Each league has at least one "division"
	//Each division is to be stored in the vector
	std::vector<Team *> teams;

	League *parent; //parent league
};
