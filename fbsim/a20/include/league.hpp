//LEAGUE.HPP
//League Class declares
//Graham Buchanan


/***Update Log***
 * 02 November 2021 - Creation
 * 02 November 2021 - (WIP) Step One - Team data
 */

//#include "team.hpp"
//#include "game.hpp"
#include <stdint.h>
#include <set>
#include <iostream>
#include <vector>
#include <cstdio>
#include <sstream>
#include <map>
#include "division.hpp"

/* This program implements a league class to keep a catalogue of teams.*/

class League {
	public:
		League(); //create a league
		~League(); //delete a league

		bool Save_State(std::string f);
		bool Read_Teams(std::string f); //Enter Team Data
		bool Input_Teams(); //Enter Team Data
		bool Edit_Team(Team *t); //Edit Team Data
		
		Team *Find_Name(const std::string &name); //Find a team based on name
		Team *Find_Location(const std::string &location); //Find a team based on location
		Team *Find_Index(const uint32_t &id);

		void Print_By_Index() const; //Print teams by index
		void Print_By_Overall() const; //Print teams by overall rating
		void Print_By_Offense() const; //Print teams by offensive rating
		void Print_By_Defense() const; //Print teams by defensive rating
		void Print_By_Name() const; //Print teams by name
		void Print_By_Location() const; //Print teams by location
		void Print_By_Wins() const; //Print teams by wins
		void Print_By_Division() const;

	protected:
		std::vector <Team *> TV; //Team Vector
		std::multimap <std::string, Team *> NM; //Name Map
		std::multimap <std::string, Team *> LM; //Location Team Map

		std::map<std::string, Division *> DM; //Division Map
};
