//CELL.CPP
//Defines the Cell Class
//Graham Buchanan

#include "cell.hpp"

Cell::Cell() {
	cell = 0;
	good[0] = '\0';
	good[1] = '\0';
	locx = 0;
	locy = 0;
	rule = NULL;
	govn = NULL;
	type = 0;
	prod = 0;
	stor = 0;
	food = 0;
	pop = 0;
	grow = 0;
	happ = 0;
	infr = 0;
	garr = 0;
};

void Cell::Print() {

	printf("cell=%04x\n", cell);

};

bool Init(uint16_t &a, char &b[2], uint8_t c, uint8_t d) {
	cell = a;
	good = b;
	locx = c;
	locy = d;

	return true;
};

bool New_Ruler(Civ* a, Noble* b) {
	if(a != NULL) rule = a;
	if(b != NULL) govn = b;
	return true;
};

bool Update_Stats() {

	double a;
	/*update all base stats*/
	
	//first, update pop
	a = 1.0*grow + 1.0*pop;
	if(a > 250) pop = 250;
	else if(a < 0) pop = 0;
	else pop = (uint8_t)a;
	//then, infr
	a = 1.0*infr - 1.0*pop - 1.0*happ - 1.0*garr;
	if(a > 100) infr = 100;
	else if(a < 0) infr = 0;
	else infr = (uint8_t)a;
	//then, food
	a = 1.0*food - 1.0*pop - 1.0*garr;
	if(a > 100) food = 100;
	else if(a < 0) food = 0;
	else food = (uint8_t)a;
	//then, happ
	a = 1.0*happ - 1.0*pop + (1.0*garr-1.0*pop)/(1.0*pop) +1.0*food +1.0*infr;
	if(a > 100) happ = 100;
	else if(a < 0) happ = 0;
	else happ = (uint8_t)a;
	//then, grow
	a = 1.0*grow - 1.0*pop+1.0*infr+1.0*food+1.0*happ;
	if(a > 25) grow = 25;
	else if(a < -25) grow = -25;
	else grow = (uint8_t)a;
	//then, prod
	a = 1.0*prod + 1.0*pop +1.0*happ;
	if(a > 10) prod = 10;
	else if(a < 0) prod = 0;
	else prod = (uint8_t)a;
	  
	
	
	return true;
};
bool Clear_Stats();
