//ver1.cpp
//Review of C++
//Graham Buchanan
//24 January 2021

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

class Room {

	public:
		Room();
		~Room();

		std::string title;
		std::string description;
		int north, south, east, west;

		std::string north_exit() const {
			if(north != -1)
				return " n";
			else
				return "";
		}
		std::string south_exit() const {
			if(south != -1)
				return " s";
			else 
				return "";
		}
		std::string east_exit() const {
			if(east != -1)
				return " e";
			else
				return "";
		}
		std::string west_exit() const {
			if(west != -1)
				return " w";
			else
				return "";
		}

};

Room::Room() {
	north = -1;
	south = -1;
	east = -1;
	west = -1;
};

Room::~Room() {};

void extractInt(std::string line, int& loc_index) {
	std::stringstream ss;
	ss << line;
	std::string temp;
	int found = -1;
	while(!ss.eof()) {
		ss >> temp;
		if(std::stringstream(temp) >> found)
			loc_index = found;
	}
}

int main(int argc, char **argv) {

	//cond check arguments and file data
	if(argc < 2) {
		std::cout << "Incorrect usage, please supply file name." << std::endl;
		std::exit(1);
	}

	//open the file
	std::string fileName = argv[1];
	std::fstream fin(fileName);


	int num_tildes = 0;
	std::string line;


	if(!fin.is_open()) {
		std::cout << "GB - Error opening file." << std::endl;
		std::exit(1);
	}
	//report to dev TEST!!!!
	std::cout << "Game file read correctly, creating rooms now." << std::endl;
	while(getline(fin, line)) {

		for(unsigned int i = 0; i <line.size(); ++i) {
			if(line[i] == '~')
				++num_tildes;
		}
	}

	int room_index = (num_tildes / 3);
	Room *r = new Room[room_index];

	std::cout << "Rooms created, there are: " << room_index << std::endl;
	num_tildes = 0;

	fin.close();
	fin.open(fileName);
	if(!fin.is_open()) {
		std::cout << "GB - Error opening file." << std::endl;
		std::exit(1);
	}
	else
		std::cout << "Game file read correctly, copying files now." << std::endl;



	while(getline(fin, line)) {

		room_index = (num_tildes / 3);

		int loc_index = -1;
		if( (num_tildes % 3) == 0) {		
			if( !(line[0] == '~') )
				r[room_index].title = line;
		}
		if( (num_tildes % 3) == 1) {
			if( !(line[0] == '~') )
				r[room_index].description += line + '\n';
		}
		if( (num_tildes % 3) == 2) {
			 if( !(line[0] == '~') ) {
				extractInt(line, loc_index);

				for(unsigned int j = 0; j < line.size(); ++j) {
					if(line[j] == 'n')
						r[room_index].north = loc_index;
					if(line[j] == 's')
						r[room_index].south = loc_index;
					if(line[j] == 'e')
						r[room_index].east = loc_index;
					if(line[j] == 'w')
						r[room_index].west = loc_index;
				}
			}
		}

		for(unsigned int i = 0; i <line.size(); ++i) {
			if(line[i] == '~')
				++num_tildes;
		}
	}

	//GAME CODE
	fin.close();

	std::cout << "File succesfully copied, game beginning now." << std::endl;

	int userSelect;

	while( !(userSelect == -1) ) {
	std::cout << "Please input room number:" << std::endl << ">";
	std::cin >> userSelect;
	std::cout << r[userSelect].east << ' ' <<  r[userSelect].west << std::endl;
	std::cout << r[userSelect].title << ' ' << r[userSelect].description << std::endl;

	}
	delete[] r;

	return 0;
}



