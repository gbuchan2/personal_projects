Occupation: [[Arator]]
Age: 36 cycles
Height: 233.7 cm
Mass: 101 kg
Mate: Nobia+
Father: Odeif+
Mother: Nala+
Children: none
Master: [[Halsmer]]+
Apprentice(s): 
Birth Sector: [[21]]
Home Sector: [[Sectors/13]]

### Backstory
Odeicon comes from a generation prior, born in 766 A.W. His genetic testing and mother's influence put him up for an opening in the Aratorship, for which he began apprenticing at 8 cycles under Halsmer in Sector 4. At 15 cycles, he was mated with Nobia, but the two failed to produce a child during his 24th cycle. Nobia was summarily [[Humanity#Recycle|Recycle]]d, coinciding with Odeicon's assignment to Sector 13 as Head Arator. 

Odeicon ranks well in almost all testings performed on the Hub. His genetics place him highly amongst even the newer cyclings. His carefully maintained discipline has kept him in great physical shape compared to some of his Arator peers, but he still believes he can do better. Odeicon looks down upon his fellow man, particularly those of the 'backward' Sector 13. As far as Odeicon is concerned, none can beat him. He will not allow that. 

Despite his rich ego, much of Odeicon's successes can be attributed to the circumstances around him. He was born into a well-to-do Arator family, he lived during a particularly stable period of the Hub, and Sector 13 provides little disturbance for an Arator that follows his handbook. In truth, Odeicon may perform well in all metrics, but he has done nothing to prove himself. Some small portion of him understands this, thus the [[Humanity#The Breaking|Breaking]] has offered Odeicon a chance to do just that.

Sectors 13, [[16]], and [[9]] have landed upon the surface, surrounded by a myriad of internal and external threats. A strong Arator is exactly what humanity needs at this time. Can Odeicon provide salvation for his people? Or will he doom them all?

### Story
Odeicon lands amongst the [[Sicuno]]'s city of [[Hikalukapa]]. His journey mostly revolves around a budding rivalry with one of Haka's elders, "[[Gray]]" (Falagu o Hakalu). Gray initially treats Odeicon as an object of study, a human ([[Babalu]]) who exhibits some advanced intelligence and tenacity. When Odeicon stages an insurrection amongst some of Gray's guards, Gray takes Odeicon as a more formidable threat. Gray then allows Odeicon to "escape" in hopes he will lead them to the Babalu camp, but he leads them directly to Sector 13. A battle starts, in which Gray ruthlessly murders humans and takes several captive. 

A small contingent of Babalus and humans slip away and attempt to assault Gray's convoy. This is unsuccessful, but Gray and Odeicon come face-to-face and share a ferocious duel. Odeicon's story ends with the rest of humanity being scattered into the wilds, with Odeicon vowing revenge upon Gray.