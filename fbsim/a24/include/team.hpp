//TEAM.HPP
//Declarations for Team module
//Graham Buchanan


/*Changelog
 *
 * 19 July 2022 - Creating base code
 * 23 July 2022 - Added Yardage ticker
 *
 *
 */

#include <cstdlib>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <deque>
#include <stdint.h>
#include <cstdio>
#include <iostream>
#include <fstream>


struct Team {

	Team();
	Team(uint8_t num, std::string s, uint64_t rtg);

	std::string name;
	std::string location;
	std::string tag;
	uint8_t id;

	//Ratings
	uint8_t poff;
	uint8_t roff;
	uint8_t olin;

	uint8_t pdef;
	uint8_t rdef;
	uint8_t dlin;

	uint8_t rate;

	//stats
	uint16_t wins;
	uint16_t loss;
	uint16_t ties;
	uint16_t ptsc;
	uint16_t oppt;

	int16_t opyd;
	int16_t dpyd;
	int16_t oryd;
	int16_t dryd;
};
