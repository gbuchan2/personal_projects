// lab1.cpp
// Addressing the User and stealing their address, property value
// Graham Buchanan
// 25 August 2020

#include <iostream>
#include <string>

using namespace std;

int main()
{
	string name;		//var user name 
	string street_name;		//var user street name
	string street_type;		//var user street type

	int street_number;		//var user house#
	int last_val;		//var last month property value
	int this_val;		//var this month property value

	cout << "What is your name? ";		//txt prompts user for name
	cin >> name;
	
	cout << "Hi there, " << name << ". \n" ;		//txt address user

	cout << "Enter the property street number, name, and type: ";		//txt prompts user for address
	cin >> street_number >> street_name >> street_type;
	
	cout << "Last month's property value: $";		//txt prompts user for last property value 
	cin >> last_val;
	
	cout << "This month's property value: $";		//txt prompts user for current property value
	cin >> this_val;

	cout << "\n";

	cout << "This property is located at " << street_number << " " << street_name << " " << street_type << ". \n" ;		//txt user address
	cout << "Its value has changed by $" << this_val - last_val << " since last month. \n" ;		//txt diff in prop value
	cout << "The estimated monthly mortgage is $" << (this_val * 0.05) / 12 << ". \n" ;		//txt est mortgage payment; calc this month's mortgage payment
	cout << "Last month's estimated monthly mortgage was $" << (last_val * 0.05) / 12 << ". \n" ;			//txt last est. mortgage payment; calc last month's mortgage payment
	cout << "The average monthly mortgage is $" << (((last_val + this_val) / 2) * 0.05) / 12 << ". \n" ;		//txt avg est. mortgage; calc avg mortgage payment

	return 0;

}
