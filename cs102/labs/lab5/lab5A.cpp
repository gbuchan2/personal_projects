//lab5.cpp
//Shopping Cart Simulator by and for broke college students
//Graham Buchanan
//05 November 2020

#include <iostream>
#include <vector>
#include <iomanip>
#include <string>

using namespace std;

//class declare "Item" class
class Item {

	//class_var declare necessary member variables
	private:
		string itemName;
		float itemPrice;
		int itemNum;

	public:

		//class_funct declare default constructor--sets vars to "0"
		Item() {
			itemName = "none";
			itemPrice = 0;
			itemNum = 0;
		}

		//class_funct class mutator declaration
		void SetName(string);
		void SetPrice(float);
		void SetQuantity(int);

		//class_funct class accessor declaration
		string GetName() const;
		float GetPrice() const;
		int GetQuantity() const;
		void Print();

};

//class_funct Item mutator "set Name" define
void Item::SetName(string n) {
	itemName = n;
}

//class_funct Item accessor "get Name" define
string Item::GetName() const {
	return itemName;
}

//class_funct Item mutator "set Price" define
void Item::SetPrice(float p) {
	itemPrice = p;
}

//class_funct Item accessor "get Price" define
float Item::GetPrice() const {
	return itemPrice;
}

//class_funct Item mutator "set Num" define
void Item::SetQuantity(int q) {
	itemNum = q;
}

//class_funct Item accessor "get Num" define
int Item::GetQuantity() const {
	return itemNum;
}

//class_funct Item accessor "print" define
void Item::Print() {
	cout << GetName() << ": " << GetQuantity() << " at $" << setprecision(2) << fixed << GetPrice() << " = $" << GetPrice() * GetQuantity() << endl;
}

//wow I did not expect this one to appear :)
int main() {

	//var declare necessry vars
	int count = 0;
	string name;
	float price;
	int number;
	vector<Item> list;

	//class call default constructor
	Item apple;

	//cond program loop until EOF
	while(!cin.eof()) 
	{

		//txt for flavor
		cout << "Item " << (count + 1) << endl;

		//txt for input
		cout << "Enter the item name: ";
		getline(cin, name);

		//cond loop as long as no EOF
		if(!cin.eof()) {

			//txt prompt for input
			cout << "Enter the item price: $";
			cin >> price;

			//cond if no EOF...so I kept running into issues until I spammed the code with if statements...hope you enjoy
			if(!cin.eof()) {

				//txt prompt for input
				cout << "Enter the item quantity: ";
				cin >> number;
			}

			//cond if no EOF...welcome to the jungle of actual operations
			if(!cin.eof()) {

				//operations
				apple.SetName(name);
				apple.SetPrice(price);
				apple.SetQuantity(number);

				//var store item data
				list.push_back(apple);
			}

			//var count one
			++count;
		}
		
		//txt space and clearing cin
		cout << endl;
		cin.ignore();
	}

	//cond if no EOF...this feels repetitive
	if(count > 0) {
		//txt print summary page
		cout << "\nTotals" << endl;
		double sum = 0;
		
		for(unsigned int j = 0; j < list.size(); j++) {

			//funct find sum total of list
			list[j].Print();
			sum += list[j].GetPrice() * list[j].GetQuantity();
		}
		
		//txt final tally
		cout << "\nTotal: $" << sum << endl;
	}

	return 0;
}









