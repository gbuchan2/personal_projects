// lab3.cpp
// Developing a slot machine i.e. developing a gambling addiction
// Graham Buchanan
// 30 September 2020

#include <iostream>
#include <random>

using namespace std;

int main() {

	int user_pts;		//var user score
	int user_bet;		//var user wager
	int user_win;		//var user reward
	unsigned int seed;		//var seed
	int i;		//var i --> for loop
	int gamble[3];		//var gamble array 3 int
	int matches;		//var counts how many matches within the randos
	int plays;		//var counts how many iterations of the main while loop
	int tracker[2][20];		//var array to carry all wagers(row0) and winnings(row1)
	int min_max[2][2] = { {999999, 0}, {999999, 0} };		//var array to carry max (col1) /min (col0) values


	default_random_engine rng(seed);		//Create RNG	
	uniform_int_distribution<int> r(2,7);		// Range of ints 2-7

	cout << "Input a seed: ";		//txt Prompts user for seed
	cin >> seed;		//stores user input to seed
	cout << '\n';		//endline

	user_pts = 100;		//starts user with 100 "coins"
	user_bet = 1;
	user_win = 1;
	plays = 0;


	while(user_pts > 0) {		//loop while user does not enter 0

		cout << "You currently possess " << user_pts << " coins. \n";
		cout << "Bet how many coins? ";		//txt prompts user for wager

		while( !( cin >> user_bet) || !(user_bet <= user_pts) || (user_bet < 0) ) {

			cin.clear();		//cin clear error flag
			cin.ignore(numeric_limits<streamsize>::max(), '\n');

			cout << "Bet how many coins? ";		//txt Prompts user for wager
		}		

		if(user_bet == 0)		//if user enters 0...
			break;		//end program
		
		cout << '\n';		//endline

		tracker[0][plays] = user_bet;		//var stores wager into row0 of tracker
		
		if(user_bet > min_max[0][1])		//var stores min/max wagers
			min_max[0][1] = user_bet;
		if(user_bet < min_max[0][0])
			min_max[0][0] = user_bet;

		user_pts -= user_bet;		//subtract the wagered amount
		matches = 0;		// set matches to zero

		for(i=0; i<3; ++i){		//loop 3 times

			gamble[i] = r(rng);		//grabs rng value and stores in array
			if(gamble[i] == gamble[i-1]) {		//if this gamble value matches the previous...
				matches += 1;		//count one match
			}
		}

		for (i=0; i<3; ++i){		//loop 3 times
			cout << gamble[i] << ' ';		//prints array
		}
		cout << '\n';		// endline

		if(matches == 2) {		//if there are two matches (i.e. 3 same ##)
			if(gamble[0] == 7)		//if one of the values = 7
				user_win = 10;		//reward = 10*
			else
				user_win = 5;
		}
		else if(matches == 1) {
			user_win = 2;
		}
		else 
			user_win = 0;		//no matches equals no win

		tracker[1][plays] = (user_win*user_bet);		//var stores winning into row1 of tracker
		
		if(user_win*user_bet > min_max[1][1])		//var stores min/max winnings
			min_max[1][1] = user_win*user_bet;
		if(user_win*user_bet < min_max[1][0])
			min_max[1][0] = user_win*user_bet;

		if(user_win > 0)
			cout << "You won " << user_bet * user_win << " coins!" << '\n';		//txt Winning Congratulations
		else 
			cout << "You did not win." << '\n';		//txt LOSER!

		cout << '\n';		//endline

		user_pts += user_bet * user_win;		//add coins based on win
		plays += 1;		//play count

	}

	if(plays > 0) {		//cond if user played...
		
		cout << "Play Summary:" << '\n';		//txt Begins play summary
		for( i = 0; i < plays; ++i) {		//cond for statement
			cout << "You wagered " << tracker[0][i] << " coins and won " << tracker[1][i] << " coins. \n";		//displays each wager and win		
		}
		cout << '\n';		//endline
			cout << "Your minimum wager was " << min_max[0][0] << " coins. \n";		//ends Play Summary.
			cout << "Your maximum wager was " << min_max[0][1] << " coins. \n";
			cout << "Your minimum prize was " << min_max[1][0] << " coins. \n";
			cout << "Your maximum prize was " << min_max[1][1] << " coins. \n";
	}


	return 0;
}
