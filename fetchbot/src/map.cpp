//MAP.CPP
//Source file for map generator
//Graham Buchanan
//14 Sept 2023

/***FURTHER DESCRIPTION***
 * Specify map generation algorithm
 * Parameters taken on command line args
 */

/***CHANGELOG***
 * 14 Sept 2023 - Creation; ball and bot
 *
 *
 */

#include "map.hpp"

Map::Map() {
	//Initialize  map generator

	size_t i;

	game_map.clear();
	map_characters.clear();

	map_characters["bot"] = 'O';
	map_characters["wall"] = 'X';
	map_characters["ball"] = '*';
	mit = map_characters.begin();
	ball_in_image = 10000;
}

Map::~Map() {

	game_map.clear();
	map_characters.clear();

}

bool Map::Read(std::string &filename) {

	std::FILE* f;
	int a;
	uint32_t r_ballx, r_bally, r_botx, r_boty;
	char item;
	char *e;
	char line[256];
	std::string s = "";
	std::string temp = "";	
	size_t i, j, r_width;

	try {
		f = fopen(filename.c_str(), "r");
		if(f == NULL) return false;
		else {
			//Read in Map info
			a = fscanf(f, "%zu %u,%u %u,%u\n", 
					&r_width, &r_ballx, &r_bally, &r_botx, &r_boty);
			if(a != 5 || a == EOF) throw;

			//Store Map Info
			ball_x = r_ballx;
			ball_y = r_bally;
			bot_x = r_botx;
			bot_y = r_boty;
			
			//begin generating map

			if(!game_map.empty()) game_map.clear();
			temp.resize(r_width, '.');
			game_map.resize(r_width, temp);
			

			//read each line of the map
			//shove into each row
			printf("\n");
			for(i = 0; i < game_map.size(); ++i) {
				e = fgets(line, 256, f);
				if(e == NULL) throw;
				else {
					s = line;
					for(j = 0; j < s.size(); ++j) {
						//identify item
					  printf("%c ", s[j]);
					  if(s[j] == 'O') s[j] = map_characters["bot"];
						else if(s[j] == 'X') s[j] = map_characters["wall"];
						else if(s[j] == '*') s[j] = map_characters["ball"];
						//store char into map
					  game_map[i][j] = s[j];
					}
				}
				printf("\nRow %2zd %s\n",i , game_map[i].c_str());
			}
			fclose(f);
		}
	} catch(...) { 
		perror("ERROR: File is incorrect format.\n");
		return false;
	}
	return true;
}

bool Map::Save(std::string &filename) {
	std::FILE* f;
	int a;
	uint32_t r_width, r_ballx, r_bally, r_botx, r_boty;
	char item;
	std::string s = "";
	std::string temp = "";	
	size_t i, j;
		  printf("Saving the map\n");

	try {
		f = fopen(filename.c_str(), "w");
		if(f == NULL) throw(1);
		else {
		  //store header first
			fprintf(f, "%zu %u,%u %u,%u\n", 
					game_map.size(), ball_x, ball_y, bot_x, bot_y); 
			for(i = 0; i < game_map.size(); ++i) {
			  s = game_map[i];
				for(j = 0; j < s.size(); ++j) {
					if(s[j] == map_characters["bot"]) s[j] = 'O';
					else if(s[j] == map_characters["wall"]) s[j] = 'X';
					else if(s[j] == map_characters["ball"]) s[j] = '*';
					else s[j] = '.';
				}
				fprintf(f, "%s\n", s.c_str());
			}
			fclose(f);
		}
	} catch(...) {
	  perror("ERROR: Cannot open file.\n");
		return false;
	}
	return true;
}

bool Map::Capture(uint32_t direction) {
	//fill the camera cone vector
	//0 1
	//2 3 4 5
	//6 7 8 9 10 11
	int i, d, w, max_w, lim_w, lim_d;
	int x, y, dir; //x,y,dir of map
	std::string s, t;
	x = bot_x;
	y = bot_y; 
	dir = direction; //0 - North, 1 - East, 2 - South, 3 - West
	d = 0;
	lim_d = (y < 23) ? y : 23;
	bool capture_flag;

	if(game_map.empty()) {
		perror("ERROR: There is no map!\n");
		return false;
	}
	camera_cone.clear();
	ball_in_image = 10000;
	ball_hor_image = 0;
	capture_flag = true;
	printf("Starting 1.\n");
	while(capture_flag) {

		if(dir == 0) {
		  //facing up (north)
			printf("N Finding row %d\n", d);
			t = "";
			//calculate width based on depth (symmetric)
			//w = d/sqrt(3)
			s = game_map[y-d]; //grab string for row
			if(d == 1) {
				t.push_back(s[x]); //catch d=1 case
				if(s[x] == map_characters["ball"]) {
					ball_in_image = 1;
					ball_hor_image = 0;
				}
			}
			else {
				w = (x < d/sqrt(3)) ? x : d/sqrt(3); //check left
				for(i = x - w; i < x; ++i) {
					t.push_back(s[i]);
					if(s[i] == map_characters["ball"]) {
						ball_in_image = d;
						ball_hor_image = x - i;
					}
				}
				w = ((x + d/sqrt(3)) > s.length()) ? (s.length() - x) : d/sqrt(3); 
				for(i = x; i < x + w; ++i) {
					t.push_back(s[i]);
					if(s[i] == map_characters["ball"]) {
						ball_in_image = d;
						ball_hor_image = x - i;
					}
				}

			}
			//iterate d
			camera_cone.push_back(t);
			if(ball_in_image != 10000) printf("ball found! %d\n", ball_in_image);
			++d;
			printf("iterating\ty=%d d=%d\n", y, d);
			if((d > 22) || ((y - d) < 0)) capture_flag = false;
		} else if(dir == 1) {
		  //facing right (east)
			t = "";
			s = "";
			printf("E Finding row %d\n", d);

			//d is distance (in x direction) from bot_x
			//do not have the ability to just grab ONE string (which makes this more challenging)
		  if(d == 1) {
				s.push_back(game_map[x+1][y]);
				if(game_map[y][x+1] == map_characters["ball"]) {
					ball_in_image = 1;
					ball_hor_image = 0;
				}
			}
			else {
				w = (y < d/sqrt(3)) ? y : d/sqrt(3); //check left
				for(i = y-w; i < y; ++i) {
					if(i >= 0) {
						s.push_back(game_map[i][x+d]);
						if(game_map[i][x+d] == map_characters["ball"]) {
							ball_in_image = d;
							ball_hor_image = y-i;
						}
					}
				}
				w = ((y + d/sqrt(3)) > game_map.size()) ? (game_map.size() - y) : d/sqrt(3); 
				for(i = y; i < y+w; ++i) {
					if(i < game_map.size()) {
						s.push_back(game_map[i][x+d]);
						if(game_map[i][x+d] == map_characters["ball"]) {
							ball_in_image = d;
							ball_hor_image = y-i;
						}
					}
				}
			}
			//check end conditions
			camera_cone.push_back(s);
			++d;
			if((d > 22) || ((x + d) >= game_map.size())) capture_flag = false;
		} else if(dir == 2) {
			//facing down (south)
			printf("S Finding row %d\n", d);
			t = "";
			//calculate width based on depth (symmetric)
			//w = d/sqrt(3)
			s = game_map[y+d]; //grab string for row
			if(d == 1) {
				t.push_back(s[x]); //catch d=1 case
				if(s[x] == map_characters["ball"]) ball_in_image = 1;
			}
			else {
				w = (x < d/sqrt(3)) ? x : d/sqrt(3); //check left
				for(i = x - w; i < x; ++i) {
					t.push_back(s[i]);
					if(s[i] == map_characters["ball"]) {
						ball_in_image = d;
						ball_hor_image = x - i;
					}
				}
				w = ((x + d/sqrt(3)) > s.length()) ? (s.length() - x) : d/sqrt(3); 
				for(i = x; i < x + w; ++i) {
					t.push_back(s[i]);
					if(s[i] == map_characters["ball"]) {
						ball_in_image = d;
						ball_hor_image = x -i;
					}
				}
			}
			//iterate d
			camera_cone.push_back(t);
			++d;
			printf("iterating\ty=%d d=%d\n", y, d);
			if((d > 22) || ((y + d) >= game_map.size())) capture_flag = false;
		} else {
		  //facing left (west)
			t = "";
			s = "";
			printf("W Finding row %d\n", d);

			//d is distance (in x direction) from bot_x
			//do not have the ability to just grab ONE string (which makes this more challenging)
		  if(d == 1) {
				s.push_back(game_map[x-1][y]);
				if(game_map[y][x-1] == map_characters["ball"]) {
					ball_in_image = 1;
					ball_hor_image = 0;
				}
			}
			else {
				w = (y < d/sqrt(3)) ? y : d/sqrt(3); //check left
				for(i = y-w; i < y; ++i) {
					if(i >= 0) {
						s.push_back(game_map[i][x-d]);
						if(game_map[i][x-d] == map_characters["ball"]) {
							ball_in_image = d;
							ball_hor_image = y-i;
						}
					}
				}
				w = ((y + d/sqrt(3)) > game_map.size()) ? (game_map.size() - y) : d/sqrt(3); 
				for(i = y; i < y+w; ++i) {
					if(i < game_map.size()) {
						s.push_back(game_map[i][x-d]);
						if(game_map[i][x-d] == map_characters["ball"]) {
							ball_in_image = d;
							ball_hor_image = y-i;
						}
					}
				}
			}
			//check end conditions
			camera_cone.push_back(s);
			++d;
			if((d > 22) || ((x - d) < 0)) capture_flag = false;
		}
	}

	/* What Capture "captures" (flipped upside down)
	 * .... (d = 5, w = 2)
	 * .... (d = 4, w = 2)
	 *  ..  (d = 3, w = 1)
	 *  ..  (d = 2, w = 1)
	 *   .  (d = 1, w = 0)
	 *   X  (d = 0, w = 0) -- 'X' is stand in for bot, not captured
	 */
	return true;
}

bool Map::Move_Bot(uint32_t x) {

	if(game_map.empty()) return false;
  if(x == 0) {
		//moving north
		if(bot_y - 1 >= game_map.size()) {
			printf("ERROR: that is outside of the map!\n");
			return false;
		} else if(game_map[bot_x][bot_y - 1] != '.') {
			printf("ERROR: something is in that space\n");
			return false;
		} else {
			game_map[bot_x][bot_y] = '.';
			bot_y--;
			game_map[bot_y][bot_x] = map_characters["bot"];
			return true;
		}
	} else if(x == 1) {
		//moving east
		if(bot_x + 1 >= game_map.size()) {
			printf("ERROR: that is outside of the map!\n");
			return false;
		} else if(game_map[bot_y][bot_x + 1] != '.') {
			printf("ERROR: something is in that space\n");
			return false;
		} else {
			game_map[bot_y][bot_x] = '.';
			bot_x++;
			game_map[bot_y][bot_x] = map_characters["bot"];
			return true;
		}
	} else if(x == 2) {
		//moving north
		if(bot_y + 1 >= game_map.size()) {
			printf("ERROR: that is outside of the map!\n");
			return false;
		} else if(game_map[bot_x][bot_y + 1] != '.') {
			printf("ERROR: something is in that space\n");
			return false;
		} else {
			game_map[bot_x][bot_y] = '.';
			bot_y++;
			game_map[bot_x][bot_y] = map_characters["bot"];
			return true;
		}
	} else {
		//moving north
		if(bot_x - 1 >= game_map.size()) {
			printf("ERROR: that is outside of the map!\n");
			return false;
		} else if(game_map[bot_y][bot_x - 1] != '.') {
			printf("ERROR: something is in that space\n");
			return false;
		} else {
			game_map[bot_y][bot_x] = '.';
			bot_x--;
			game_map[bot_y][bot_x] = map_characters["bot"];
			return true;
		}
	}
	return true;
}

bool Map::Place_Wall(uint32_t x, uint32_t y) {

	if(game_map.empty()) return false;
	if((x) > game_map.size()) return false;
	if((y) > game_map.size()) return false;

	game_map[y][x] = map_characters["wall"];

	return true;
}

void Map::Generate_Image() { //Generates a 1280x720 (720p) image
	//Find the cube first
	std::string s;
	int width = 36;
	int height = 24;
	size_t i, j, k, z;
	uint32_t a, ow, oh, od, oc; //object width, object depth

	//generate base picture
	picture.clear();
	s.resize(width, '.');
	picture.resize(height, s);
	//p (pixels) = 1280*(w/(2*d/sqrt(3)))

	//start with the cube (if seen)
	printf("Ball in Image: %d\nBall Hor Image: %d\n", ball_in_image, ball_hor_image);
	if(ball_in_image != 10000) {
		od = ball_in_image;
		a = od/sqrt(3);
		ow = (a == 0) ? width : width/(2*a);
		oh = (ow < height) ? ow : height;
		//find location of ball within row of capture
		//map that location to image x
		oc = (ow != width) ? width/2 - ow*ball_hor_image - ow/2: width / 2;
		printf("Image:\nOD = %u\tOW = %u\tOC=%u\n",od,ow,oc);
		printf("iterating from rows %d to %d\n", picture.size()/2-oh/2, picture.size()/2+oh/2);
		for(j = picture.size()/2-oh/2; j < picture.size()/2+oh/2; ++j) {
			printf("Printing row j=%u\n", j);
			for(i = (oc - ow/2); i < (oc + ow/2); ++i) {
			  picture[j][i] = '*';
			}
		}
		//iterate through each row BEFORE cube
		//identify any WALLS, set entire column back to '.'
		for(i = 0; i < od; ++i) {
			//iterate through cone rows to find wall
			for(j = 0; j < camera_cone[od - i].size(); ++j) {
				a = (od - i)/sqrt(3);
				printf("j = %d, a = %u\n",j, a);
				ow = (a == 0) ? width : width/(2*a) + 1;
				printf("OD-I=%d J=%d OW=%d", od-i, j, ow);
				//found wall
				if(camera_cone[od - i][j] == map_characters["wall"]) {
					oc = j*(width/(2*a))+width/(4*a); //x-axis center
					printf("found wall OD-I=%d J=%d OW=%d OC=%d\n", od-i, j, ow, oc);
					for(k = 0; k < height; ++k) {
						//iterate through each column
						for(z = oc-ow/2; z < (oc +ow/2); ++z) {
							printf("z=%u, width=%u\n", z, width);
							if((z < 0) || (z >= width));
							else picture[k][z] = '.';
						}
					}
				}
			}
		}
	}
	//if no cube, no reason to do anything else

	Print_Picture();
}

void Map::Print_Capture() {
	size_t i, j;
	for(i = 0; i < camera_cone.size(); ++i) printf("%zd: %s\n", i, camera_cone[i].c_str());
}

void Map::Print_Picture() {
	size_t i;

	if(picture.empty()) printf("ERROR: Nothing to print\n");
	for(i = 0; i < picture.size(); ++i) {
		printf("%s\n", picture[i].c_str());
	}
}

bool Map::Set_Map_Characters(std::string &s, char &c) {

	try {
		if(map_characters.find(s) == map_characters.end()) throw((std::string)"ERROR: Incorrect object name");
		else map_characters[s] = c;
	} catch(...) { 
		return false;
	}
	return true;
}

bool Map::Generate_Map(uint32_t w) {
	int wall_percentage = 15;
	int dice;
	srand (time(NULL));
	size_t i,j;
	std::string temp;

	if(!game_map.empty()) game_map.clear();

	temp.resize(w, '.');
	game_map.resize(w, temp);

	game_map[w/4][w/2] = map_characters["ball"];
	ball_x = w/2;
	ball_y = w/4;

	game_map[3*w/4][w/2] = map_characters["bot"];
	bot_x = w/2;
	bot_y = 3*w/4;

	for(i = 0; i < game_map.size(); ++i) {
		for(j = 0; j < game_map.size(); ++j) {
			dice = 100;
			if((game_map[i][j] != map_characters["ball"]) && (game_map[i][j] != map_characters["bot"])) {
				dice = rand() % 100 + 1;
				if(dice < wall_percentage) game_map[i][j] = map_characters["wall"];
			}
		}
	}

	return true;
}

void Map::Print() {
	size_t i, j;

	if(game_map.empty()) perror("ERROR: There is no map!\n");
	else 
		for(i = 0; i < game_map.size(); ++i) printf("%s\n", game_map[i].c_str());
}

void Map::Print_Info() {
	printf("Map Info:\n\tSize: %zdx%zd\n\tBot X,Y: %u,%u\n\tBall X,Y: %u,%u\n", 
			game_map.size(), game_map.size(), bot_x, bot_y, ball_x, ball_y);
	if(!camera_cone.empty()) printf("\tBiI X: %d\n", ball_in_image);
}

char PrintMenu() {
	char d;
	printf("Please select an option:\n\tc - Change Map Characters\n\tw - place wall\n\t"
			"s - save the current state\n\tr - read a new state\n\tg - generate new map\n\t"
			"m - move bot\n\ti - camera capture\n\ty - show info\n\t"
			"p - print existing map\n> ");
	std::cin >> d;
	return d;
}

int main(int argc, char** argv) {
	Map* m = new Map();
	char c, a;
	uint32_t x, y;
	uint32_t t;
	std::string s;

	c = PrintMenu();
	while(c != 'q') {
		switch(c) {
			case 'c':
				printf("Please name an object. Your options are:\n\tball, bot, and wall\n> ");
				std::cin >> s;
				printf("Please specify a new symbol for object '%s'. Use any character in the ASCII table.\n> ", s.c_str());
				std::cin >> a;
				m->Set_Map_Characters(s, a);
				break;
			case 'g':
				printf("Please specify a map width. The acceptable range is [5, 200].\n> ");
				std::cin >> t;
				if((t < 5) || (t > 200)) perror("ERROR: You have specified a width outside of the range [5, 200].");
				else m->Generate_Map(t);
				break;
			case 'w':
				printf("Please specify a x coordinate.\n> ");
				std::cin >> x;
				printf("Please specify a y coordinate.\n> ");
				std::cin >> y;
				if(!(m->Place_Wall(x,y))) perror("ERROR: place failed.\n");
				break;
			case 'm':
				printf("Please specify a direction (0-north, 1-east).\n> ");
				std::cin >> x;
				if(!(m->Move_Bot(x))) perror("ERROR: move failed.\n");
				break;
			case 's': 
				//saving
				printf("Please specify a file name.\n> ");
				std::cin >> s;
				m->Save(s);
				break;
			case 'r':
				printf("Please specify a file name.\n> ");
				std::cin >> s;
				m->Read(s);
				break;
			case 'i':
				printf("Please specify a direction. N=0; S=2.\n> ");
				std::cin >> x;
				m->Capture(x);
				m->Print_Capture();
				m->Generate_Image();
				break;
			case 'y':
				m->Print_Info();
				break;
			case 'p':
				printf("Map:\n");
				m->Print();
				break;
			case 'q':
				printf("Thanks for playing!\n");
				return 0;
			default:
				perror("ERROR: Not an acceptable input! Please try again!\n");
				break;
		}
		c = PrintMenu();
	}

	return 0;
}

