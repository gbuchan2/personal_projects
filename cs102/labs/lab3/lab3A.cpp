// lab3.cpp
// Developing a slot machine i.e. developing a gambling addiction
// Graham Buchanan
// 30 September 2020

#include <iostream>
#include <random>

using namespace std;

int main() {

	unsigned int seed;		//var seed
	int i;		//var i --> for loop
	int gamble[3];		//var gamble array 3 int

	cout << "Input a seed: ";		 //txt Prompts User for seed
	cin >> seed;		//stores user input to seed

	default_random_engine rng(seed);		//Create RNG	
	uniform_int_distribution<int> r(2,7);		// Range of ints 2-7

	for(i=0; i<3; ++i){		//loop 3 times

		gamble[i] = r(rng);		//grabs rng value and stores in array

	}

	for (i=0; i<3; ++i){		//loop 3 times
		cout << gamble[i] << ' ';		//prints array
	}
	cout << '\n';		// endline

	return 0;
}
