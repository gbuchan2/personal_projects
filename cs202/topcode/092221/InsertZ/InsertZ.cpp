#include <string>
#include <vector>
#include <list>
#include <cmath>
#include <algorithm>
#include <map>
#include <set>
#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

class InsertZ {
  public:
    string canTransform(string init, string goal);
};

string InsertZ::canTransform(string init, string goal)
{
	size_t i = 0;
	size_t j = 0;
	string s = "";
	for(i = 0; i < goal.length(); ++i) {
		if(goal[i] != 'z')
			s.push_back(goal[i]);
	}
	if(s == init)
		return "Yes";
	else
		return "No";
}
