//MAIN.CPP
//Simulator Driver Program
//Graham Buchanan

/***Update Log***
 * 10 November 2021 - Creation
 * 17 November 2021 - First Tests, MakeFile functionality
 * 22 December 2021 - Added Print Menu and Input Menu
 *
 *
 * */

#include "league.hpp"

/*League.hpp
 * public:
 bool Read_Teams(); //Enter Team Data
 bool Edit_Teams(); //Edit Team Data

 Team *Find(const std::string &name); //Find a team based on name
 Team *Find(const std::string &location); //Find a team based on location

 void Print_By_Overall() const; //Print teams by overall rating
 void Print_By_Offense() const; //Print teams by offensive rating
 void Print_By_Defense() const; //Print teams by defensive rating
 void Print_By_Name() const; //Print teams by name
 void Print_By_Location() const; //Print teams by location
 void Print_By_Wins() const; //Print teams by wins

 protected:
 std::vector <Team *> TV; //Team Vector
 std::map <std::string, Team *> NM; //Name Team Map
 std::map <std::string, Team *> LM; //Location Team Map
 */

/*Team.hpp
 * public:
 Team(std::string l, std::string n, uint16_t r); //Class Constructor
 bool EditName(std::string l, std::string n); //0 - edit Name and Location, 1 - Rating
 bool UpdateStats(bool flag, int pf, int po); //0 - new game, 1- edited game
 void PrintData(); //Prints all Data of the Team Class

 protected:
 std::string Name; //Stores the name of the Team (i.e. "Tigers")
 std::string Location; //Stores the Location of the Team (i.e. "Auburn")
 uint16_t Rating; //0-7 Offense, 8-15 Defense
 double PPG; //ppg
 double PAPG;//opponent ppg
 uint32_t record; //0-7 ties, 8-15 losses, 16-23 wins
 */



char MainMenu();
bool Edit_Teams(League &l);
void Print_Menu(League &l);
void Input_Menu(League &l);

int main() {

	char in = '\0';
	League l;
	std::string s = "";

	printf("Welcome to my Football Simulator version ALPHA_20_0_1.\nThe current simulator only handles a few options at the moment. Check out the Update Log to see all available functionalities!. Enjoy!\n");

	while((in = MainMenu()) != 'q') {
		if(in == 't') {
			//Input Team Data
			Input_Menu(l);
		}
		if(in == 'y') {
			//Edit Teams
			if(!Edit_Teams(l))
				std::cerr << "There was an error processing that request. Please try again.\n";
		}
		if(in == 'p') {
			//Print League
			Print_Menu(l);
		}
		if(in == 's') {
			//Save current data
			std::cout << "Please input valid save path. Please type 'Quit' to stop this function. > ";
			std::cin >> s;
			while(!(std::cin) || (s == "") || (s != "Quit") || !(l.Save_State(s))) {
				std::cin.clear();
				std::cerr << "ERROR: There was something wrong with that string. Please try again. > ";
				std::cin >> s;
			}
		}
	}

	printf("Thanks for playing!\n");

	return 0;

}


char MainMenu() {
	char in = '\0';
	printf("\nCommands:\n\tt - Input Team Data\n\ty - Edit Team Data\n\tp - Print League\n\ts - Save State\n\tq - Quit Program\n\nInput > ");
	std::cin >> in;
	while((!std::cin) || ((in != 't') && (in != 'y') && (in != 'p') && (in != 's') && (in != 'q'))) {
		std::cin.clear();
		printf("Please select one of the above options. > ");
		std::cin >> in;
	}

	return in;
}
bool Edit_Teams(League &l) {

	uint32_t id = 0;

	l.Print_By_Index();
	std::cout << "Which Team would you like to edit? Please enter their id (tmID).\nEnter a wrong number to end this process. > ";
	std::cin >> id;
		if((l.Find_Index(id) == NULL)) {
			std::cerr << "That Team cannot be found in the registry. Exiting process.\n";
			return false;
		}
		if(!l.Edit_Team(l.Find_Index(id))) {
			std::cerr << "There was an error processing that request.\n";
			return false;
		}
	return true;

}

void Print_Menu(League &l) {

	char c = '\0';

	printf("This is the Print Menu!\n\tn - Print By Team Names (all teams in the league)\n\tl - Print by Team Locations (all teams in the league)\n\td - Print by Division\n\ti - Print by Index (order of insertion)\n\tq - Exit Print Menu\n Input > ");
	std::cin >> c;
	while(c != 'q') {
		if(c == 'n') l.Print_By_Name(); 
		else if(c == 'l') l.Print_By_Location();
		else if(c == 'd') l.Print_By_Division();
		else if(c == 'i') l.Print_By_Index();
		else {
			std::cout << "ERROR: Input was not recorded properly. Please try again. > ";
		}
		std::cin >> c;
	}
	std::cout << "Returning to the Main Menu.\n";
}

void Input_Menu(League &l) {

	char c = '\0';
	std::string f;
	printf("This is the Input Menu! Here you will input all league data!\n\tm - Enter team and division data manually (one at a time)\n\tf - Input data from a file (need exact file path from the base file!)\n\tq - Exit Input Menu\n Input > ");
	std::cin >> c;
	while(c != 'q') {
		if(c == 'm') l.Input_Teams();
		else if(c == 'f') {
			std::cout << "Please input a file path. > ";
			std::cin >> f;
			if(!(l.Read_Teams(f))) {
				std::cerr << "ERROR: file could not be read. Please try again.\n";
			} else std::cout << "Read successful.\n";
		}
		std::cin >> c;
	}
	std::cout << "Returning to Main Menu.\n";
}
