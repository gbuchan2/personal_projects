//ALPHA 1.14 
//Preparing "Error Checking"
//College Football League Simulator

#include <iostream>
#include <vector>
#include <string>
#include <cstdio>
#include <limits>
#include <map>

char PrintMenu() {
	char in = '\0';
	printf("\nCommands:\n"
			"     t - Input team data\n"
			"     s - Input schedule\n"
			"     d - Print schedule\n"
			"     p - Print standings\n"
			"     g - Play the next game in the schedule\n"
			"     f - Play the full schedule\n"
			"     q - Quit\n"
			"What would you like to do? > ");
	std::cin >> in;
	return in;

}
struct Game {
	std::string Away;
	std::string Home;
	int away_score;
	int home_score;
};

class Team {
  public:
		std::string Name;
    int off;
    int def;
		int wins = 0;
		int losses = 0;
		float ppg = 0;
		float papg = 0;
    void Print() const;
		void Avg_SC(int pts_for, int pts_ag);
};

class TMS {
	public:
		bool Read();
		bool Edit_Sch();
		bool Check_In(char op);
		Team *Find(const std::string &name);
		
		void Print_By_Offense() const;
		void Print_By_Defense() const;
		void Print_By_Name() const;
		void Print_By_Wins() const;
		void Print_Sch();
		
		void Play_Game(const std::string &Away, const std::string &Home);
		void Play_Schedule(bool flag);
	protected:
		std::vector <Team *> TV;
		std::map <std::string, Team *> TM;
		std::vector <Game> schedule;
		int games_played = 0;
};

/* THE MAIN FUNCTION/.....
 *
 * STOP LOSING IT!!!!! */

int main() {
	TMS tms;
	char userSelect = '\0';
	bool flag = false;
	printf("Welcome to my Football Simulator version alpha_0105.\nThe current simulator will only progress through an 'as-run' schedule, meaning you will specify the teams, ratings, and then the number of games.\nBefore each game, you will be asked who is playing.\nPlease keep in mind there are no tie games in College Football!\nPlease follow all instructions as they come.\n");
	while( (userSelect = PrintMenu()) != 'q') {
		if(userSelect == 't') {
			tms.Read();
			if(tms.Check_In(userSelect) == false) {
				while(tms.Check_In(userSelect) == false) {
					printf("You must input more than one team.\n");
					tms.Read();
				}
			}
			printf("All teams accounted for. Continuing operation.\n");

		}
		else if(userSelect == 's') {
			if(tms.Check_In(userSelect) == true) {
				tms.Edit_Sch();
				tms.Print_Sch();
			}
			else
				printf("There must be teams to create a schedule.\n");
		}
		else if(userSelect == 'd') {
			if(tms.Check_In(userSelect) == true) {
				tms.Print_Sch();
			}
			else
				printf("There must be a schedule to print a schedule.\n");
		}		
		else if(userSelect == 'p') {
			if(tms.Check_In(userSelect) == true) {
				tms.Print_By_Wins();
			}
			else
				printf("There must be teams to print the standings\n.");
		}
		else if(userSelect == 'g') {
			if(tms.Check_In(userSelect) == true) {
				flag = false;
				tms.Play_Schedule(flag);
			}
			else
				printf("There must a schedule to play a game.\n");
		}
		else if(userSelect == 'f') {
			if(tms.Check_In(userSelect) == true) {
				flag = true;
				tms.Play_Schedule(flag);
			}
			else
				printf("There must be a schedule to play a game.\n");
		}
		else
			printf("Process did not gather your command. Please make sure to use one of the listed commands.\n");
	}
	printf("Thank you for playing!\n");
  return 0;
}


/* TEAM class function:::::
 *
 */
void Team::Print() const {
  printf("%s (%d-%d); %d offense, %d defense; average score (%.2f-%.2f).\n", Name.c_str(), wins, losses, off, def, ppg, papg);
}

void Team::Avg_SC(int pts_for, int pts_ag) {
	float curr_avg = ppg;
	int num = wins + losses;
	curr_avg *= num;
	curr_avg += pts_for;
	curr_avg /= (num + 1);
	ppg = curr_avg;
	curr_avg = papg;
	curr_avg *= num;
	curr_avg += pts_ag;
	curr_avg /= (num + 1);
	papg = curr_avg;
}

/* TMS class functions::::::
 *
 */
bool TMS::Read() { //read in team data
	Team *t;
	int notms = 0;
	printf("\nHow many teams are there? > ");
	std::cin >> notms;
	if(notms < 2) {
		while( (!std::cin) || (notms < 2) ) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			printf("There must be more than one team. > ");
			std::cin >> notms;
		}
	}
	for(int i = 0; i < notms; ++i) {
		t = new Team;
		printf("\nTeam %d:\nPlease input the Name (string) Offensive Rating (int) and Defensive Rating (int).\n", i + 1);
		std::cin >> t->Name >> t->off >> t->def;
		while(!std::cin) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			printf("ERROR: Make there are ONLY SPACES in between each input. > ");
			std::cin >> t->Name >> t->off >> t->def;
		}
		TV.push_back(t);
	}
	printf("\n");
	for(int i = 0; i < notms; ++i) TV[i]->Print();
	for(unsigned long i = 0; i < TV.size(); ++i) {
		t = TV[i];
		TM[t->Name] = t;
	}
	return true;
}

bool TMS::Edit_Sch() {
	//CURRENTLY ONLY LOADS THE SCHEDULE AT PROGRAM START
	//WILL ADD FUNCTIONALITY TO EDIT DURING RUN
	Team *a;
	int nogms = 0;
	std::string home_team;
	std::string away_team;
	Game game;
	printf("\nHow many games are we playing? > ");
	std::cin >> nogms;
	while( (!std::cin) || (nogms < 1) ) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			printf("ERROR: Make sure to input a positive number. > ");
			std::cin >> nogms;
		}
	for(int i = 0; i < nogms; ++i) {
		printf("Game %d:\n", i + 1);
		printf("Who is Away? > ");
		std::cin >> away_team;
		a = Find(away_team);
		while(a == NULL) {
			printf("That team cannot be found. Please try again. > ");
			std::cin >> away_team;
			a = Find(away_team);
		}
		printf("Who is Home? > ");
		std::cin >> home_team;
		a = Find(home_team);
		while(a == NULL) {
			printf("That team cannot be found. Please try again. > ");
			std::cin >> home_team;
			a = Find(home_team);
		}
		game.Away = away_team;
		game.Home = home_team;
		schedule.push_back(game);
	}
	return true;
}

bool TMS::Check_In(char op) {
	if( (op == 't') || (op == 's') || (op == 'p') ) {
		if(TV.size() > 0) 
			return true;
		else
			return false;
	}
	else {
		if(schedule.size() > 0) 
			return true;
		else
			return false;
	}
}
Team *TMS::Find(const std::string &name) { //Find correct team object based on name
	std::map<std::string, Team *>::iterator tit;
	tit = TM.find(name);
	if(tit == TM.end()) return NULL;
	return tit->second;
}

/* TMS print functions::::
 */

void TMS::Print_Sch() {
	printf("This is our full season schedule!\n");
	for(size_t i = 0; i < schedule.size(); ++i) {
		printf("     Game %zd: %s at %s\n", i + 1, schedule[i].Away.c_str(), schedule[i].Home.c_str());
	}
}

void TMS::Print_By_Offense() const { //Prints team data by Offensive Rating
	Team *t;
	size_t i;
	std::multimap <double, Team *> m;
	std::multimap <double, Team *>::const_iterator mit;
	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->off, t));
	}
	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Defense() const { //Prints team data by Defensive Rating
	Team *t;
	size_t i;
	std::multimap <double, Team *> m;
	std::multimap <double, Team *>::const_iterator mit;
	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->def, t));
	}

	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Name() const { //Prints team data by Name
	std::map <std::string, Team *> m;
	std::map <std::string, Team *>::const_iterator mit;
	size_t i;
	Team *t;
	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m[t->Name] = t;
	}
	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

void TMS::Print_By_Wins() const { //Prints team data by Wins
	std::multimap <int, Team *> m;
	std::multimap <int, Team *>::const_iterator mit;
	size_t i;
	Team *t;
	for(i = 0; i < TV.size(); ++i) {
		t = TV[i];
		m.insert(std::make_pair(-t->wins, t));
	}
	for(mit = m.begin(); mit != m.end(); ++mit) {
		mit->second->Print();
	}
}

/* TMS class functions to play games
 */

void TMS::Play_Game(const std::string &Away, const std::string &Home) { //play a game dialogue 
	int home_score;
	int away_score;
	Team *a;
	Team *h;
	a = Find(Away);
	h = Find(Home);
	if(a == NULL)
		printf("The Away Team cannot be found.\n");
	else if(h == NULL) 
		printf("The Home Team cannot be found.\n");
	else {
		printf("%s vs. %s\n", Away.c_str(), Home.c_str());
		a->Print();
		h->Print();
		printf("What is the final score (Format: Away-Home)? > ");
		std::cin >> away_score;
		std::cin.ignore(50, '-');
		std::cin >> home_score;
		if (away_score > home_score) {
			printf("Final Score: %s %d, %s %d. \nCongratulations %s!\n\n", Away.c_str(), away_score, Home.c_str(), home_score, Away.c_str());
			a->Avg_SC(away_score, home_score);
			h->Avg_SC(home_score, away_score);
			a->wins++;
			h->losses++;
		}
		else if (home_score > away_score) {	
			printf("Final Score: %s %d, %s %d. \nCongratulations %s!\n\n", Home.c_str(), home_score, Away.c_str(), away_score, Home.c_str());
			a->Avg_SC(away_score, home_score);
			h->Avg_SC(home_score, away_score);
			h->wins++;
			a->losses++;
		}
		else {
			printf("There can be no ties! The Game has no conclusion!\n");
		}
	}
}

void TMS::Play_Schedule(bool flag) { //run through the schedule
	if(flag == true) {
		for(size_t i = games_played; i < schedule.size(); ++i) {
			printf("GAME %zd: ", i + 1);
			Play_Game(schedule[i].Away, schedule[i].Home);
			games_played++;
		}
		Print_By_Wins();
	}
	if(flag == false) {
		printf("GAME %d: ", games_played + 1);
		Play_Game(schedule[games_played].Away, schedule[games_played].Home);
		games_played++;
	}
}

